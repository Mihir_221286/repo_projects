﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StockCharcha.Startup))]
namespace StockCharcha
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
