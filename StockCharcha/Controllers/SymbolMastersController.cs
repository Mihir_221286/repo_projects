﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using StockCharcha.DataAccess;
using StockCharcha.Models;
using StockCharcha.Filters;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using StockCharcha.Helpers;

namespace StockCharcha.Controllers
{
    public class SymbolMastersController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;
        [SessionExistFilter]
        public ActionResult Index()
        {
            List<SymbolMaster> symbolList = new List<SymbolMaster>();
            var symbol = new PagedData<SymbolMaster>();
            try
            {
                symbolList = db.SymbolMasters.OrderByDescending(x => x.CreatedOn).ToList();
                symbol.Data = symbolList.Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                symbol.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)symbolList.Count() / PageSize));
                symbol.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(symbol);
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<SymbolMaster> symbolList = new List<SymbolMaster>();
            var symbol = new PagedData<SymbolMaster>();
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    symbol = GetList(searchText, page);
                    return PartialView("_SymbolList", symbol);
                }

                symbolList = db.SymbolMasters.OrderByDescending(x => x.CreatedOn).ToList();
                symbol.Data = symbolList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                symbol.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)symbolList.Count() / PageSize));
                symbol.CurrentPage = page;
                return PartialView("_SymbolList", symbol);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_SymbolList", new PagedData<SymbolMaster>());
        }

        public ActionResult Create()
        {
            try
            {
                return View(new SymbolModel());
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new SymbolModel());
        }

        public ActionResult AddOrEditSymbol(SymbolModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Id == 0)
                    {
                        SymbolMaster symbol = new SymbolMaster
                        {
                            NSESymbol = model.NSESymbol,
                            BSESymbol = model.BSESymbol,
                            SecurityId = model.SecurityId,
                            ISINNumber = model.ISINNumber,
                            CompanyName = model.CompanyName,
                            DateOfListing = model.DateOfListing,
                            PaidUpValue = model.PaidUpValue,
                            FaceValue = model.FaceValue,
                            Status = model.Status,
                            Series = model.Series,
                            MarketLot = model.MarketLot,
                            SymbolGroup = model.SymbolGroup,
                            Industry = model.Industry,
                            CreatedBy = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId),
                            CreatedOn = DateTime.Now
                        };
                        db.SymbolMasters.Add(symbol);
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditSymbol", symbol.Id.ToString(), "New symbol");
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelInsertStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.AddStatus;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        SymbolMaster symbol = db.SymbolMasters.Find(model.Id);

                        symbol.NSESymbol = model.NSESymbol;
                        symbol.BSESymbol = model.BSESymbol;
                        symbol.SecurityId = model.SecurityId;
                        symbol.ISINNumber = model.ISINNumber;
                        symbol.CompanyName = model.CompanyName;
                        symbol.DateOfListing = model.DateOfListing;
                        symbol.PaidUpValue = model.PaidUpValue;
                        symbol.FaceValue = model.FaceValue;
                        symbol.Status = model.Status;
                        symbol.Series = model.Series;
                        symbol.MarketLot = model.MarketLot;
                        symbol.SymbolGroup = model.SymbolGroup;
                        symbol.Industry = model.Industry;
                        symbol.ModifiedBy = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId);
                        symbol.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditSymbol", symbol.Id.ToString(), "Update symbol");
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelUpdateStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.UpdateStatus;
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            TempData[CommonConstants.ModelStatus] = CommonConstants.ModelInvalidStatus;
            TempData[CommonConstants.MessageStatus] = CommonConstants.InvalidStatus;
            return View("Create", model);
        }

        public ActionResult Edit(long? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                SymbolMaster symbolMaster = db.SymbolMasters.Find(id);
                if (symbolMaster == null)
                {
                    return HttpNotFound();
                }
                SymbolModel model = new SymbolModel
                {
                    Id = symbolMaster.Id,
                    NSESymbol = symbolMaster.NSESymbol,
                    BSESymbol = symbolMaster.BSESymbol,
                    SecurityId = symbolMaster.SecurityId,
                    ISINNumber = symbolMaster.ISINNumber,
                    CompanyName = symbolMaster.CompanyName,
                    DateOfListing = symbolMaster.DateOfListing,
                    PaidUpValue = symbolMaster.PaidUpValue,
                    FaceValue = symbolMaster.FaceValue,
                    Status = symbolMaster.Status,
                    Series = symbolMaster.Series,
                    MarketLot = symbolMaster.MarketLot,
                    SymbolGroup = symbolMaster.SymbolGroup,
                    Industry = symbolMaster.Industry,
                };
                return View("Create", model);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View("Create", new SymbolModel());
        }

        //public ActionResult Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SymbolMaster symbolMaster = db.SymbolMasters.Find(id);
        //    if (symbolMaster == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(symbolMaster);
        //}

        //// POST: SymbolMasters/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    SymbolMaster symbolMaster = db.SymbolMasters.Find(id);
        //    db.SymbolMasters.Remove(symbolMaster);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        public ActionResult Delete(bool confirm, long? id)
        {
            try
            {

                if (confirm)
                {
                    SymbolMaster symbolMaster = db.SymbolMasters.Find(id);
                    //symbolMaster.IsDeleted = true;
                    db.SymbolMasters.Remove(symbolMaster);
                    db.SaveChanges();
                    SiteActivityErrorsUtil.SiteActivity("Delete Symbol", id.ToString(), "Delete symbol");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public PagedData<SymbolMaster> GetList(string searchText, int page)
        {
            try
            {
                var symbol = new PagedData<SymbolMaster>();
                var symbolMasters = db.SymbolMasters.Where(x => x.BSESymbol.ToLower().Contains(searchText) || x.NSESymbol.ToLower().Contains(searchText)).ToList();

                List<SymbolMaster> symbolMasterList = new List<SymbolMaster>();

                symbolMasterList.AddRange(symbolMasters.Select(x => new SymbolMaster
                {
                    Id = x.Id,
                    NSESymbol = x.NSESymbol,
                    BSESymbol = x.BSESymbol,
                    CreatedOn = x.CreatedOn
                }));
                symbol.Data = symbolMasterList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                symbol.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)symbolMasterList.Count() / PageSize));
                symbol.CurrentPage = page;
                return symbol;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<SymbolMaster>();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
