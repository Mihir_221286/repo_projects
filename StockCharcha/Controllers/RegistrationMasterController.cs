﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StockCharcha.DataAccess;
using StockCharcha.Models;
using System.Web.Security;
using System.IO;
using StockCharcha.Filters;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using StockCharcha.Helpers;

namespace StockCharcha.Controllers
{
    public class RegistrationMasterController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;
        [SessionExistFilter]
        public ActionResult Index()
        {
            List<RegistrationModel> registrationList = new List<RegistrationModel>();
            var registration = new PagedData<RegistrationModel>();
            try
            {
                var registrationMasters = db.RegistrationMasters.Include(r => r.aspnet_Users).Where(x => x.IsDeleted == false).ToList();

                registrationList.AddRange(registrationMasters.Select(x => new RegistrationModel
                {
                    Id = x.Id,
                    UserName = x.aspnet_Users.UserName,
                    RoleId = GetRolesForUser(x.aspnet_Users.UserName),
                    Mobile = x.Mobile,
                    //CreatedByName = Convert.ToString(Membership.GetUser(x.CreatedBy).UserName),
                    Email = Convert.ToString(Membership.GetUser(x.aspnet_Users.UserName).Email),
                    IsDeleted = Convert.ToBoolean(x.IsDeleted)
                }));
                registration.Data = registrationList.Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                registration.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)registrationList.Count() / PageSize));
                registration.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(registration);
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<RegistrationModel> registrationList = new List<RegistrationModel>();
            var registration = new PagedData<RegistrationModel>();

            if (!string.IsNullOrEmpty(searchText))
            {
                registration = GetList(searchText, page);
                return PartialView("_RegistrationList", registration);
            }
            var registrationMasters = db.RegistrationMasters.Include(r => r.aspnet_Users).Where(x => x.IsDeleted == false).ToList();

            registrationList.AddRange(registrationMasters.Select(x => new RegistrationModel
            {
                Id = x.Id,
                UserName = x.aspnet_Users.UserName,
                RoleId = GetRolesForUser(x.aspnet_Users.UserName),
                Mobile = x.Mobile,
                //CreatedByName = Convert.ToString(Membership.GetUser(x.CreatedBy).UserName),
                Email = Convert.ToString(Membership.GetUser(x.aspnet_Users.UserName).Email),
                IsDeleted = Convert.ToBoolean(x.IsDeleted)
            }));
            registration.Data = registrationList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
            registration.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)registrationList.Count() / PageSize));
            registration.CurrentPage = page;

            return PartialView("_RegistrationList", registration);
        }

        public string GetRolesForUser(string userName)
        {
            try
            {
                string roleName = Roles.GetRolesForUser(userName)[0];
                return roleName;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return string.Empty;
        }
        public ActionResult Create()
        {
            try
            {
                ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName");
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new RegistrationModel());
        }

        public ActionResult AddOrEditRegistration(RegistrationModel model, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                    model.Photo = UploadImage(file);

                if (ModelState.IsValid)
                {
                    MembershipUser checkUser = Membership.GetUser(model.UserName);

                    if (model.Id == 0)
                    {
                        if (checkUser != null)
                        {
                            ModelState.AddModelError("checkUser", "User Name already exist.");
                            ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", model.RoleId);
                            return View("Create", model);
                        }
                        else
                        {
                            string userName = Membership.GetUserNameByEmail(model.Email);
                            if (!string.IsNullOrEmpty(userName))
                            {
                                ModelState.AddModelError("checkEmail", "Email already exist.");
                                ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", model.RoleId);
                                return View("Create", model);
                            }
                        }

                        Membership.CreateUser(model.UserName, model.Password, model.Email);
                        string roleName = db.aspnet_Roles.Where(x => x.RoleId == new Guid(model.RoleId)).FirstOrDefault().RoleName;
                        Roles.AddUserToRole(model.UserName, roleName);

                        RegistrationMaster registrationModel = new RegistrationMaster
                        {
                            UserId = new Guid(Membership.GetUser(model.UserName).ProviderUserKey.ToString()),
                            Mobile = model.Mobile,
                            Photo = model.Photo,
                            Description = model.Description,
                            UserTag = model.UserTag,
                            Website = model.Website,
                            CreatedBy = new Guid(Membership.GetUser(model.UserName).ProviderUserKey.ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };

                        db.RegistrationMasters.Add(registrationModel);
                        db.SaveChanges();
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelInsertStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.AddStatus;
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditRegistration", registrationModel.Id.ToString(), "New registration");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        RegistrationMaster registrationModel = db.RegistrationMasters.Find(model.Id);

                        var user = from s in db.RegistrationMasters
                                   where s.UserId == (Guid)checkUser.ProviderUserKey && s.Id != model.Id
                                   select s;
                        if (user.Count() > 0)
                        {
                            ModelState.AddModelError("checkUser", "User Name already exist.");
                            ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", model.RoleId);
                            return View("Create", model);
                        }
                        else
                        {
                            string userName = Membership.GetUserNameByEmail(model.Email);
                            MembershipUser checkUserEmail = Membership.GetUser(userName);
                            var userEmail = from s in db.aspnet_Membership
                                            where s.Email == model.Email && s.UserId != (Guid)checkUser.ProviderUserKey
                                            select s;

                            if (userEmail.Count() > 0)
                            {
                                ModelState.AddModelError("checkEmail", "Email already exist.");
                                ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", model.RoleId);
                                return View("Create", model);
                            }
                        }

                        if (registrationModel != null)
                        {
                            registrationModel.UserId = new Guid(Membership.GetUser(model.UserName).ProviderUserKey.ToString());
                            registrationModel.Mobile = model.Mobile;
                            registrationModel.Photo = model.Photo != null ? model.Photo : registrationModel.Photo;
                            registrationModel.Description = model.Description;
                            registrationModel.UserTag = model.UserTag;
                            registrationModel.Website = model.Website;
                            registrationModel.IsDeleted = model.IsDeleted;
                            db.SaveChanges();
                        }
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelUpdateStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.UpdateStatus;
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditRegistration", registrationModel.Id.ToString(), "update registration");
                        return RedirectToAction("Index");
                    }
                }
                if (model.Id == 0)
                {
                    ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName");
                }
                else
                {
                    List<string> roles = Roles.GetRolesForUser(model.UserName).ToList();
                    Guid roleId = new Guid();
                    foreach (var role in roles)
                    {
                        roleId = db.aspnet_Roles.Where(x => x.RoleName == role).FirstOrDefault().RoleId;
                    }
                    ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", roleId);
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            TempData[CommonConstants.ModelStatus] = CommonConstants.ModelInvalidStatus;
            TempData[CommonConstants.MessageStatus] = CommonConstants.InvalidStatus;
            return View("Create", model);
        }

        public ActionResult Edit(long? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                RegistrationMaster registrationMaster = db.RegistrationMasters.Find(id);
                if (registrationMaster == null)
                {
                    return HttpNotFound();
                }

                RegistrationModel model = new RegistrationModel
                {
                    Id = registrationMaster.Id,
                    UserName = registrationMaster.aspnet_Users.UserName,
                    Password = "111111",
                    ConfirmPassword = "111111",
                    Email = Membership.GetUser(registrationMaster.aspnet_Users.UserName).Email,
                    Mobile = registrationMaster.Mobile,
                    Description = registrationMaster.Description,
                    Website = registrationMaster.Website,
                    UserTag = registrationMaster.UserTag,
                };

                List<string> roles = Roles.GetRolesForUser(registrationMaster.aspnet_Users.UserName).ToList();
                Guid roleId = new Guid();
                foreach (var role in roles)
                {
                    roleId = db.aspnet_Roles.Where(x => x.RoleName == role).FirstOrDefault().RoleId;
                }

                ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", roleId);
                return View("Create", model);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View("Create", new RegistrationModel());
        }

        public byte[] UploadImage(HttpPostedFileBase file)
        {
            string filename = string.Empty;
            byte[] bytes;
            int BytestoRead;
            int numBytesRead;
            filename = Path.GetFileName(file.FileName);
            bytes = new byte[file.ContentLength];
            BytestoRead = (int)file.ContentLength;
            numBytesRead = 0;
            while (BytestoRead > 0)
            {
                int n = file.InputStream.Read(bytes, numBytesRead, BytestoRead);
                if (n == 0) break;
                numBytesRead += n;
                BytestoRead -= n;
            }
            return bytes;
        }

        public void showImage(int id = 0)
        {
            try
            {
                byte[] image = db.RegistrationMasters.Where(x => x.Id == id).FirstOrDefault().Photo;
                Response.Buffer = true;
                Response.Clear();
                if (image != null)
                {
                    Response.BinaryWrite(image);
                }
                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public ActionResult Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    RegistrationMaster registrationMaster = db.RegistrationMasters.Find(id);
        //    if (registrationMaster == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(registrationMaster);
        //}

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    RegistrationMaster registrationMaster = db.RegistrationMasters.Find(id);
        //    registrationMaster.IsDeleted = true;
        //    //db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        public ActionResult Delete(bool confirm, long? id)
        {
            try
            {
                if (confirm)
                {
                    RegistrationMaster registrationMaster = db.RegistrationMasters.Find(id);
                    registrationMaster.IsDeleted = true;
                    db.SaveChanges();
                    SiteActivityErrorsUtil.SiteActivity("Registration Delete", id.ToString(), "Delete registration");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public PagedData<RegistrationModel> GetList(string searchText, int page)
        {
            try
            {
                var registration = new PagedData<RegistrationModel>();
                var registrationMasters = db.RegistrationMasters.Include(r => r.aspnet_Users).Where(x => x.IsDeleted == false).ToList();

                List<RegistrationModel> registrationList = new List<RegistrationModel>();
                List<RegistrationModel> registrationSearchList = new List<RegistrationModel>();

                registrationList.AddRange(registrationMasters.Select(x => new RegistrationModel
                {
                    Id = x.Id,
                    UserName = x.aspnet_Users.UserName,
                    RoleId = GetRolesForUser(x.aspnet_Users.UserName),
                    Mobile = x.Mobile,
                    Email = Convert.ToString(Membership.GetUser(x.aspnet_Users.UserName).Email),
                    IsDeleted = Convert.ToBoolean(x.IsDeleted)
                }));
                var registrationsearch = (from p in registrationList
                                          where (
                                                 !string.IsNullOrEmpty(p.UserName) ? p.UserName.ToLower().Contains(searchText.ToLower()) : string.Empty.Contains(searchText)
                                              || !string.IsNullOrEmpty(p.Mobile) ? p.Mobile.ToLower().Contains(searchText.ToLower()) : string.Empty.Contains(searchText)
                                              || !string.IsNullOrEmpty(p.Email) ? p.Email.ToLower().Contains(searchText.ToLower()) : string.Empty.Contains(searchText)
                                              || !string.IsNullOrEmpty(p.RoleId) ? p.RoleId.ToLower().Contains(searchText.ToLower()) : string.Empty.Contains(searchText)
                                              )
                                          && p.IsDeleted == false
                                          select p).ToList();

                registrationSearchList.AddRange(registrationsearch.Select(x => new RegistrationModel
                {
                    Id = x.Id,
                    UserName = x.UserName,
                    RoleId = x.RoleId,
                    Mobile = x.Mobile,
                    Email = x.Email,
                    IsDeleted = x.IsDeleted,
                    CreatedOn = x.CreatedOn
                }));

                registration.Data = registrationSearchList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                registration.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)registrationSearchList.Count() / PageSize));
                registration.CurrentPage = page;

                return registration;

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<RegistrationModel>();
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
