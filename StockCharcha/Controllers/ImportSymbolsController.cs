﻿using StockCharcha.Filters;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace StockCharcha.Controllers
{
    public class ImportSymbolsController : Controller
    {
        [SessionExistFilter]
        public ActionResult Index()
        { return View("Index"); }
        public ActionResult NseSymbol(HttpPostedFileBase file)
        {
            try
            {
                if (file == null)
                {
                    return RedirectToAction("Index");
                }
                if (Path.GetExtension(file.FileName) != ".csv")
                {
                    TempData[CommonConstants.ModelStatus] = "ImportFileCheck";
                    TempData[CommonConstants.MessageStatus] = "Please check file format.";
                    return RedirectToAction("Index");
                }
                string newFileName = UploadFile(CommonConstants.NSE, file);
                if (!string.IsNullOrEmpty(newFileName))
                {
                    string message = NseImport(newFileName);
                    TempData[CommonConstants.ModelStatus] = "Import";
                    TempData[CommonConstants.MessageStatus] = message;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public ActionResult BseSymbol(HttpPostedFileBase file)
        {
            try
            {
                if (file == null)
                {
                    return RedirectToAction("Index");
                }
                if (Path.GetExtension(file.FileName) != ".csv")
                {
                    TempData[CommonConstants.ModelStatus] = "ImportFileCheck";
                    TempData[CommonConstants.MessageStatus] = "Please check file format.";
                    return RedirectToAction("Index");
                }
                string newFileName = UploadFile(CommonConstants.BSE, file);
                if (!string.IsNullOrEmpty(newFileName))
                {
                    string message = BseImport(newFileName);
                    TempData[CommonConstants.ModelStatus] = "Import";
                    TempData[CommonConstants.MessageStatus] = message;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public string UploadFile(string stockType, HttpPostedFileBase file)
        {
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    string newFileName = String.Format("{0:ddMMyyyyhhmmssfff}", DateTime.Now);
                    string originalFileName = Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(originalFileName);

                    //string imagesFilePath = Path.Combine(HttpContext.Server.MapPath("/Import/" + stockType));
                    string fileImagePath = Path.Combine("/Import/" + stockType);
                    var directoryPath = HttpContext.Server.MapPath(fileImagePath);
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }
                    string filePath = Path.Combine(directoryPath, newFileName + extension);
                    file.SaveAs(filePath);
                    return newFileName + extension;
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return null;
        }

        private string[,] LoadCsv(string filename)
        {
            // Get the file's text.
            string whole_file = System.IO.File.ReadAllText(filename);

            // Split into lines.

            //whole_file = whole_file.Replace('\n', '\r');
            //string[] lines = whole_file.Split(new char[] { '\r' },StringSplitOptions.RemoveEmptyEntries);

            string[] lines = whole_file.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            // See how many rows and columns there are.
            int num_rows = lines.Length;
            int num_cols = lines[0].Split(',').Length;

            // Allocate the data array.
            string[,] values = new string[num_rows, num_cols];

            // Load the array.
            for (int r = 0; r < num_rows; r++)
            {
                string[] line_r = lines[r].Split(',');
                for (int c = 0; c < num_cols; c++)
                {
                    values[r, c] = line_r[c];
                }
            }

            // Return the values.
            return values;
        }

        private string NseImport(string newFileName)
        {
            string message = string.Empty;
            // Get the data.
            string[,] values = LoadCsv(Server.MapPath("/Import/" + CommonConstants.NSE + "/" + newFileName));
            int num_rows = values.GetUpperBound(0) + 1;
            int num_cols = values.GetUpperBound(1) + 1;

            // Display the data to show we have it.

            // Make column headers.
            // For this example, we assume the first row
            // contains the column names.

            DataTable dt = new DataTable();
            for (int c = 0; c < num_cols; c++)
                dt.Columns.Add(values[0, c].ToString().Trim());

            // Add the data.
            for (int r = 1; r < num_rows; r++)
            {
                DataRow dr = dt.NewRow();
                for (int c = 0; c < num_cols; c++)
                {
                    dr[c] = values[r, c].ToString().Trim();
                }
                dt.Rows.Add(dr);
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                int count = 0;


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["SYMBOL"].ToString().Trim().Length > 0 && dt.Rows[i]["SERIES"].ToString().Trim().ToLower() == "eq")
                    {
                        string nameOfCompany = string.Empty;
                        if (dt.Rows[i]["NAME OF COMPANY"].ToString().Trim().Length > 0)
                        {
                            nameOfCompany = TitleCase(dt.Rows[i]["NAME OF COMPANY"].ToString().Trim());
                        }

                        string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                        SqlConnection con = new SqlConnection(builder.ConnectionString);
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SymbolMasterInsertNSE", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@NSESymbol", dt.Rows[i]["SYMBOL"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@CompanyName", nameOfCompany);
                        cmd.Parameters.AddWithValue("@Series", dt.Rows[i]["SERIES"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@DateOfListing", dt.Rows[i]["DATE OF LISTING"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@PaidUpValue", dt.Rows[i]["PAID UP VALUE"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@MarketLot", dt.Rows[i]["MARKET LOT"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@ISINNumber", dt.Rows[i]["ISIN NUMBER"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@FaceValue", dt.Rows[i]["FACE VALUE"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@CreatedBy", WebUtil.GetSessionValue<Guid>(CommonConstants.UserId));

                        int a = cmd.ExecuteNonQuery();
                        con.Close();
                        if (a <= 0) { }
                        else
                        {
                            count++;
                        }
                    }
                }
                message = count.ToString() + " Record(s) Imported Successfully from " + dt.Rows.Count + " Record(s)"; ;
            }
            return message;
        }

        private string BseImport(string newFileName)
        {
            string message = string.Empty;
            // Get the data.
            string[,] values = LoadCsv(Server.MapPath("/Import/" + CommonConstants.BSE + "/" + newFileName));
            int num_rows = values.GetUpperBound(0) + 1;
            int num_cols = values.GetUpperBound(1) + 1;

            // Display the data to show we have it.

            // Make column headers.
            // For this example, we assume the first row
            // contains the column names.

            DataTable dt = new DataTable();
            for (int c = 0; c < num_cols; c++)
                dt.Columns.Add(values[0, c].ToString().Trim());

            // Add the data.
            for (int r = 1; r < num_rows; r++)
            {
                DataRow dr = dt.NewRow();
                for (int c = 0; c < num_cols; c++)
                {
                    dr[c] = values[r, c].ToString().Trim();
                }
                dt.Rows.Add(dr);
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                int count = 0;


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Security Code"].ToString().Trim().Length > 0 && dt.Rows[i]["Status"].ToString().Trim().ToLower() == "active")
                    {

                        string nameOfCompany = string.Empty;
                        if (dt.Rows[i]["Security Name"].ToString().Trim().Length > 0)
                        {
                            nameOfCompany = TitleCase(dt.Rows[i]["Security Name"].ToString().Trim());
                        }

                        string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                        SqlConnection con = new SqlConnection(builder.ConnectionString);
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SymbolMasterInsertBSE", con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@BSESymbol", dt.Rows[i]["Security Code"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@SecurityId", dt.Rows[i]["Security Id"].ToString().Trim());

                        cmd.Parameters.AddWithValue("@CompanyName", nameOfCompany);
                        cmd.Parameters.AddWithValue("@Status", dt.Rows[i]["Status"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@SymbolGroup", dt.Rows[i]["Group"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@Series", dt.Rows[i]["Instrument"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@Industry", dt.Rows[i]["Industry"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@ISINNumber", dt.Rows[i]["ISIN No"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@FaceValue", dt.Rows[i]["Face Value"].ToString().Trim());
                        cmd.Parameters.AddWithValue("@CreatedBy", WebUtil.GetSessionValue<Guid>(CommonConstants.UserId));
                        
                        int a = cmd.ExecuteNonQuery();
                        con.Close();

                        if (a <= 0)
                        {

                        }
                        else
                        {
                            count++;
                        }
                    }
                }
                message = count.ToString() + " Record(s) Imported Successfully from " + dt.Rows.Count + " Record(s)"; ;
            }
            return message;
        }

        public static string TitleCase(String str)
        {
            string titleCase = str.ToLower();
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            titleCase = textInfo.ToTitleCase(titleCase);
            return titleCase;
        }

    }
}