﻿using StockCharcha.DataAccess;
using StockCharcha.Filters;
using StockCharcha.Helpers;
using StockCharcha.Models;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace StockCharcha.Controllers
{
    public class HelpController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;
        // GET: Blogs
        [SessionExistFilter]
        public ActionResult Index()
        {
            List<HelpModel> helpList = new List<HelpModel>();
            var help = new PagedData<HelpModel>();
            try
            {
                var questionList = db.HelpQuestions.ToList();
                helpList.AddRange(questionList.Select(x => new HelpModel
                {
                    Id = x.Id,
                    FromUser = new Guid(x.FromUser.ToString()),
                    Question = x.Description,
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    IsResponded = Convert.ToBoolean(x.IsResponded),
                    IsActive = Convert.ToBoolean(x.IsActive)
                }));

                helpList = helpList.OrderByDescending(x => x.CreatedOn).ToList();
                help.Data = helpList.Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                help.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)helpList.Count() / PageSize));
                help.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(help);
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<HelpModel> helpList = new List<HelpModel>();
            var help = new PagedData<HelpModel>();
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    help = GetList(searchText, page);
                    return PartialView("_HelpList", help);
                }

                var questionList = db.HelpQuestions.ToList();
                helpList.AddRange(questionList.Select(x => new HelpModel
                {
                    Id = x.Id,
                    FromUser = new Guid(x.FromUser.ToString()),
                    Question = x.Description,
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    IsResponded = Convert.ToBoolean(x.IsResponded),
                    IsActive = Convert.ToBoolean(x.IsActive)
                }));

                helpList = helpList.OrderByDescending(x => x.CreatedOn).ToList();
                help.Data = helpList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                help.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)helpList.Count() / PageSize));
                help.CurrentPage = page;
                return PartialView("_HelpList", help);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HelpList", new PagedData<Blog>());
        }

        public PagedData<HelpModel> GetList(string searchText, int page)
        {
            try
            {
                List<HelpModel> helpList = new List<HelpModel>();
                var help = new PagedData<HelpModel>();

                var questionList = db.HelpQuestions.Where(x => x.Description.ToLower().Contains(searchText)).ToList();
                helpList.AddRange(questionList.Select(x => new HelpModel
                {
                    Id = x.Id,
                    FromUser = new Guid(x.FromUser.ToString()),
                    Question = x.Description,
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    IsResponded = Convert.ToBoolean(x.IsResponded),
                    IsActive = Convert.ToBoolean(x.IsActive)
                }));

                help.Data = helpList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                help.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)helpList.Count() / PageSize));
                help.CurrentPage = page;
                return help;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<HelpModel>();
        }

        public ActionResult View(long id)
        {
            try
            {
                Guid userId = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId); ;
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }

                //answerList
                List<HelpAnswerModel> helpAnswerListModel = new List<HelpAnswerModel>();
                var helpAnswerList = db.HelpAnswers.Where(x => x.HelpQuestionId == id).ToList();

                if (helpAnswerList.Count() > 0)
                {
                    helpAnswerListModel.AddRange(helpAnswerList.Select(x => new HelpAnswerModel
                    {
                        Id = x.Id,
                        HelpQuestionId = id,
                        FromUser = new Guid(x.FromUser.ToString()),
                        Description = x.Description,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        IsRead = Convert.ToBoolean(x.IsRead),
                        IsActive = Convert.ToBoolean(x.IsActive),
                        Username = Membership.GetUser(new Guid(x.FromUser.ToString())).UserName,
                        UserImage = GetUserImage(new Guid(x.FromUser.ToString()), CommonConstants.userImage32.ToString()),
                        LoggedInUserId = userId
                    }));
                }

                var que = db.HelpQuestions.Where(x => x.Id == id).FirstOrDefault();

                HelpModel helpModel = new HelpModel
                {
                    HelpAnswerModel = helpAnswerListModel,
                    UserQuestion = string.Format("{0} [{1}]", que.Description, Membership.GetUser(new Guid(que.FromUser.ToString())))
                };
                ViewBag.questionId = id;
                return View(helpModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new HelpModel());
        }

        public ActionResult AnswerListByQuestion(long questionId)
        {
            try
            {
                Guid userId = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId);
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }

                //answerList
                List<HelpAnswerModel> helpAnswerListModel = new List<HelpAnswerModel>();
                var helpAnswerList = db.HelpAnswers.Where(x => x.FromUser == userId).ToList();

                if (helpAnswerList.Count() > 0)
                {
                    helpAnswerListModel.AddRange(helpAnswerList.Select(x => new HelpAnswerModel
                    {
                        Id = x.Id,
                        HelpQuestionId = questionId,
                        FromUser = new Guid(x.FromUser.ToString()),
                        Description = x.Description,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        IsRead = Convert.ToBoolean(x.IsRead),
                        IsActive = Convert.ToBoolean(x.IsActive),
                        Username = Membership.GetUser(new Guid(x.FromUser.ToString())).UserName,
                        UserImage = GetUserImage(new Guid(x.FromUser.ToString()), CommonConstants.userImage32.ToString()),
                        LoggedInUserId = userId
                    }));
                }

                HelpModel helpModel = new HelpModel
                {
                    HelpAnswerModel = helpAnswerListModel
                };
                ViewBag.questionId = questionId;
                return PartialView("_HelpAnswerListByQuestion", helpModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HelpAnswerListByQuestion", new HelpModel());
        }

        public ActionResult SaveAnswer(long questionId, string answer)
        {
            try
            {
                Guid userId = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId);
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }

                //Save Answer
                HelpAnswer helpAnswer = new HelpAnswer
                {
                    HelpQuestionId = questionId,
                    FromUser = userId,
                    Description = answer,
                    CreatedOn = DateTime.Now,
                    IsRead = false,
                    IsActive = true
                };
                db.HelpAnswers.Add(helpAnswer);
                db.SaveChanges();

                //answerList
                List<HelpAnswerModel> helpAnswerListModel = new List<HelpAnswerModel>();
                var helpAnswerList = db.HelpAnswers.Where(x => x.HelpQuestionId == questionId).ToList();

                if (helpAnswerList.Count() > 0)
                {
                    helpAnswerListModel.AddRange(helpAnswerList.Select(x => new HelpAnswerModel
                    {
                        Id = x.Id,
                        FromUser = new Guid(x.FromUser.ToString()),
                        Description = x.Description,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        IsRead = Convert.ToBoolean(x.IsRead),
                        IsActive = Convert.ToBoolean(x.IsActive),
                        Username = Membership.GetUser(new Guid(x.FromUser.ToString())).UserName,
                        UserImage = GetUserImage(new Guid(x.FromUser.ToString()), CommonConstants.userImage32.ToString()),
                        LoggedInUserId = userId
                    }));
                }

                HelpModel helpModel = new HelpModel
                {
                    HelpAnswerModel = helpAnswerListModel
                };
                ViewBag.questionId = questionId;
                return PartialView("_HelpAnswerListByQuestionAdmin", helpModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HelpAnswerListByQuestionAdmin", new HelpModel());
        }

        public string GetUserImage(Guid userId, string size)
        {
            string userImage = string.Empty;
            try
            {
                if (userId != Guid.Empty)
                {
                    string[] userImages = new string[0];
                    userImages = Directory.GetFiles(Path.Combine(HttpContext.Server.MapPath("/Media/UserImages/" + userId + "")), size + "*.*");
                    if (userImages.Count() > 0)
                    {
                        userImage = ConfigurationManager.AppSettings["ImagePath"] + "/Media/UserImages/" + userId + "/" + Path.GetFileName(userImages[0]);
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return userImage;
        }

        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

    }
}