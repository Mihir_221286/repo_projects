﻿using StockCharcha.DataAccess;
using StockCharcha.Filters;
using StockCharcha.Helpers;
using StockCharcha.Models;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace StockCharcha.Controllers
{
    public class ReportUsersController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;

        [SessionExistFilter]
        public ActionResult Index()
        {
            List<ReportUserModel> reportUserModelList = new List<ReportUserModel>();
            var reportUserModel = new PagedData<ReportUserModel>();
            try
            {
                var reportUserList = db.ReportUsers.OrderByDescending(x => x.CreatedOn).ToList();

                reportUserModelList.AddRange(reportUserList.Select(x => new ReportUserModel
                {
                    Id = x.Id,
                    FromUser = Membership.GetUser(x.FromUser).UserName,
                    ToUser = Membership.GetUser(x.ToUser).UserName,
                    Comments = x.Comments,
                    CreatedOn = x.CreatedOn,
                    IsActive = x.IsActive
                }));

                reportUserModel.Data = reportUserModelList.Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                reportUserModel.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)reportUserModelList.Count() / PageSize));
                reportUserModel.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(reportUserModel);
        }

        public PagedData<ReportUserModel> GetList(string searchText, int page)
        {
            try
            {
                List<ReportUserModel> reportUserModelList = new List<ReportUserModel>();
                var reportUser = new PagedData<ReportUserModel>();

                var reportUserList = db.ReportUsers.OrderByDescending(x => x.CreatedOn).ToList();
                reportUserModelList.AddRange(reportUserList.Select(x => new ReportUserModel
                {
                    Id = x.Id,
                    FromUser = Membership.GetUser(x.FromUser).UserName,
                    ToUser = Membership.GetUser(x.ToUser).UserName,
                    Comments = x.Comments,
                    CreatedOn = x.CreatedOn,
                    IsActive = x.IsActive
                }));

                var reportUsersSearchList = reportUserModelList.Where(x => x.ToUser.ToLower().Contains(searchText) || x.FromUser.ToLower().Contains(searchText)).ToList();

                reportUser.Data = reportUsersSearchList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                reportUser.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)reportUsersSearchList.Count() / PageSize));
                reportUser.CurrentPage = page;
                return reportUser;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<ReportUserModel>();
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<ReportUserModel> reportUserModelList = new List<ReportUserModel>();
            var reportUserModel = new PagedData<ReportUserModel>();
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    reportUserModel = GetList(searchText, page);
                    return PartialView("_ReportUserList", reportUserModel);
                }

                var reportUserList = db.ReportUsers.OrderByDescending(x => x.CreatedOn).ToList();
                reportUserModelList.AddRange(reportUserList.Select(x => new ReportUserModel
                {
                    Id = x.Id,
                    FromUser = Membership.GetUser(x.FromUser).UserName,
                    ToUser = Membership.GetUser(x.ToUser).UserName,
                    Comments = x.Comments,
                    CreatedOn = x.CreatedOn,
                    IsActive = x.IsActive
                }));

                reportUserModel.Data = reportUserModelList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                reportUserModel.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)reportUserModelList.Count() / PageSize));
                reportUserModel.CurrentPage = page;
                return PartialView("_ReportUserList", reportUserModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_ReportUserList", new PagedData<ReportUserModel>());
        }

        public JsonResult updateIsActive(long reportUserId, int isActive)
        {
            try
            {
                ReportUser reportUser = db.ReportUsers.Find(reportUserId);
                if (isActive == 0)
                {
                    reportUser.IsActive = false;
                    db.SaveChanges();

                    //Lock User from login.
                    MembershipUser mu = Membership.GetUser(reportUser.ToUser);
                    mu.IsApproved = false;
                    Membership.UpdateUser(mu);

                    SiteActivityErrorsUtil.SiteActivity("updateIsActive", "", "Report User locked");
                    return Json("User Locked.");
                }
                else
                {
                    reportUser.IsActive = true;
                    db.SaveChanges();

                    //Lock User from login.
                    MembershipUser mu = Membership.GetUser(reportUser.ToUser);
                    mu.IsApproved = true;
                    Membership.UpdateUser(mu);

                    SiteActivityErrorsUtil.SiteActivity("updateIsActive", "", "Report User Unlocked");
                    return Json("User UnLocked.");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json("Something went wrong.");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}