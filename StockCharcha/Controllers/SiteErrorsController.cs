﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StockCharcha.DataAccess;
using StockCharcha.Filters;
using StockCharcha.Models;
using System.Web.Security;
using StockCharcha.Utils.Utilities;
using StockCharcha.Utils;
using StockCharcha.Helpers;

namespace StockCharcha.Controllers
{
    public class SiteErrorsController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;

        [SessionExistFilter]
        public ActionResult Index()
        {
            List<SiteErrorModel> siteErrorList = new List<SiteErrorModel>();
            var siteError = new PagedData<SiteErrorModel>();
            try
            {
                var siteErrorr = db.SiteErrors.OrderByDescending(x => x.ModifiedON).ToList();

                siteErrorList.AddRange(siteErrorr.Select(x => new SiteErrorModel
                {
                    ErrorID = x.ErrorID,
                    ErrorCode = x.ErrorCode,
                    ExceptionMessage = x.ExceptionMessage,
                    WebURL = x.WebURL,
                    ModifiedName = x.ModifiedBY != Guid.Empty && x.ModifiedBY != null ? Membership.GetUser((Guid)x.ModifiedBY).UserName.ToString() : string.Empty,
                    ModifiedONDate = String.Format("{0:dd/MM/yyyy}", x.ModifiedON)
                }));
                siteError.Data = siteErrorList.Take(PageSize).OrderByDescending(x => x.ModifiedON).ToList();
                siteError.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)siteErrorList.Count() / PageSize));
                siteError.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(siteError);
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<SiteErrorModel> siteErrorList = new List<SiteErrorModel>();
            var siteError = new PagedData<SiteErrorModel>();
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    siteError = GetList(searchText, page);
                    return PartialView("_SiteErrorList", siteError);
                }

                var siteErrorr = db.SiteErrors.OrderByDescending(x => x.ModifiedON).ToList();

                siteErrorList.AddRange(siteErrorr.Select(x => new SiteErrorModel
                {
                    ErrorID = x.ErrorID,
                    ErrorCode = x.ErrorCode,
                    ExceptionMessage = x.ExceptionMessage,
                    WebURL = x.WebURL,
                    ModifiedName = x.ModifiedBY != Guid.Empty && x.ModifiedBY != null ? Membership.GetUser((Guid)x.ModifiedBY).UserName.ToString() : string.Empty,
                    ModifiedONDate = String.Format("{0:dd/MM/yyyy}", x.ModifiedON)
                }));
                siteError.Data = siteErrorList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.ModifiedON).ToList();
                siteError.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)siteErrorList.Count() / PageSize));
                siteError.CurrentPage = page;
                return PartialView("_SiteErrorList", siteError);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_SiteErrorList", new PagedData<SiteErrorModel>());
        }

        public PagedData<SiteErrorModel> GetList(string searchText, int page)
        {
            List<SiteErrorModel> siteErrorList = new List<SiteErrorModel>();
            var siteError = new PagedData<SiteErrorModel>();
            try
            {
                var siteErrors = db.SiteErrors.Where(x => x.ErrorCode.ToLower().Contains(searchText) || x.ExceptionMessage.ToLower().Contains(searchText)).OrderByDescending(x => x.ModifiedON).ToList();

                siteErrorList.AddRange(siteErrors.Select(x => new SiteErrorModel
                  {
                      ErrorID = x.ErrorID,
                      ErrorCode = x.ErrorCode,
                      ExceptionMessage = x.ExceptionMessage,
                      WebURL = x.WebURL,
                      ModifiedName = x.ModifiedBY != Guid.Empty && x.ModifiedBY != null ? Membership.GetUser((Guid)x.ModifiedBY).UserName.ToString() : string.Empty,
                      ModifiedONDate = String.Format("{0:dd/MM/yyyy}", x.ModifiedON)
                  }));
                siteError.Data = siteErrorList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.ModifiedON).ToList();
                siteError.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)siteErrorList.Count() / PageSize));
                siteError.CurrentPage = page;
                return siteError;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<SiteErrorModel>();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
