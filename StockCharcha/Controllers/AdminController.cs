﻿using StockCharcha.Models;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace StockCharcha.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            ViewData[CommonConstants.LoginStatus] = null;
            return View();
        }
        public ActionResult Login(LoginModel login)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewData[CommonConstants.ModelStatus] = CommonConstants.ModelInvalidStatus;
                    return View("Index", login);
                }
                else
                {
                    string userName = Membership.GetUserNameByEmail(login.Email);
                    string roleName = Roles.GetRolesForUser(userName)[0];
                    if (!string.IsNullOrEmpty(roleName) && (roleName.ToLower() == ("Admin").ToLower() || roleName.ToLower() == ("SuperAdmin").ToLower()))
                    {
                        bool userValidate = Membership.ValidateUser(userName, login.Password);
                        if (userValidate)
                        {
                            //set session
                            string userId = Membership.GetUser(userName).ProviderUserKey.ToString();
                            WebUtil.SetSessionValue<Guid>(CommonConstants.UserId, new Guid(userId));
                            WebUtil.SetSessionValue<string>(CommonConstants.UserName, userName);
                            WebUtil.SetSessionValue<string>(CommonConstants.UserEmail, login.Email);
                            SiteActivityErrorsUtil.SiteActivity("LoginIn", "", "User Login");
                            return RedirectToAction("Index", "RegistrationMaster");
                        }
                        else
                        {
                            ViewData[CommonConstants.LoginStatus] = CommonConstants.LoginFailedStatus;
                            ViewData[CommonConstants.MessageStatus] = CommonConstants.LoginFailed;
                            return View("Index", login);
                        }
                    }
                    else
                    {
                        ViewData[CommonConstants.LoginStatus] = CommonConstants.InvalidLoginStatus;
                        ViewData[CommonConstants.MessageStatus] = CommonConstants.LoginInvalid;
                        return View("Index", login);
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index", "Admin");
        }
        public ActionResult LogOut()
        {
            try
            {
                WebUtil.ClearSession();
                SiteActivityErrorsUtil.SiteActivity("Logout", "", "User Logout");
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index", "Admin");
        }

    }
}