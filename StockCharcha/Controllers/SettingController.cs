﻿using StockCharcha.DataAccess;
using System;
using System.Linq;
using System.Web.Mvc;
using StockCharcha.Filters;
using StockCharcha.Utils.Utilities;
using StockCharcha.Utils;
using StockCharcha.Models;
using StockCharcha.Utils.Enums;
using System.Collections.Generic;
using StockCharcha.Helpers;
using System.Net;

namespace StockCharcha.Controllers
{

    public class SettingController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;
        [SessionExistFilter]
        public ActionResult Index()
        {
            Setting setting = db.Settings.OrderByDescending(x => x.Id).FirstOrDefault();
            if (setting != null)
            {
                SettingModel settingsModel = new SettingModel
                {
                    Id = setting.Id,
                    StreamPrefrence = (StreamPrefrence)setting.StreamPrefrence,
                    ShareIdeaText = setting.ShareIdeaText,
                    CommentTextLimit = setting.CommentTextLimit,
                    ConsensusDays = setting.ConsensusDays,
                    ViewsOnPage = setting.ViewsOnPage,
                    Ideas = setting.Ideas,
                    Likes = setting.Likes,
                    MenuByDays = setting.MenuByDays,
                    StockUpdate = Convert.ToInt32(setting.StockUpdate)
                };
                return View(settingsModel);
            }
            return View(new SettingModel());
        }

        public ActionResult AddOrEditSettings(SettingModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Id == 0)
                    {

                        Setting setting = new Setting
                        {
                            StreamPrefrence = Convert.ToInt32(model.StreamPrefrence),
                            ShareIdeaText = model.ShareIdeaText,
                            CommentTextLimit = model.CommentTextLimit,
                            ConsensusDays = model.ConsensusDays,
                            StockUpdate = model.StockUpdate,
                            ViewsOnPage = model.ViewsOnPage,
                            Ideas = model.Ideas,
                            Likes = model.Likes,
                            MenuByDays = model.MenuByDays,
                            CreatedBy = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId),
                            CreatedOn = DateTime.Now
                        };
                        db.Settings.Add(setting);
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditSettings", setting.Id.ToString(), "New setting saved");
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelInsertStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.AddStatus;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        Setting setting = db.Settings.Find(model.Id);

                        setting.StreamPrefrence = Convert.ToInt32(model.StreamPrefrence);
                        setting.ShareIdeaText = model.ShareIdeaText;
                        setting.CommentTextLimit = model.CommentTextLimit;
                        setting.ConsensusDays = model.ConsensusDays;
                        setting.StockUpdate = model.StockUpdate;
                        setting.ViewsOnPage = model.ViewsOnPage;
                        setting.Ideas = model.Ideas;
                        setting.Likes = model.Likes;
                        setting.MenuByDays = model.MenuByDays;
                        setting.ModifiedBy = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId);
                        setting.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditSettings", setting.Id.ToString(), "Update settings");
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelUpdateStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.UpdateStatus;
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View("Index", model);
        }

        #region "Template"

        public ActionResult TemplateList()
        {
            List<TemplateMaster> templateList = new List<TemplateMaster>();
            var template = new PagedData<TemplateMaster>();
            try
            {
                templateList = db.TemplateMasters.OrderByDescending(x => x.CreatedOn).ToList();
                template.Data = templateList.Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                template.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)templateList.Count() / PageSize));
                template.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(template);
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<TemplateMaster> templateList = new List<TemplateMaster>();
            var template = new PagedData<TemplateMaster>();
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    template = GetList(searchText, page);
                    return PartialView("_TemplateList", template);
                }

                templateList = db.TemplateMasters.OrderByDescending(x => x.CreatedOn).ToList();
                template.Data = templateList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                template.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)templateList.Count() / PageSize));
                template.CurrentPage = page;
                return PartialView("_TemplateList", template);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_TemplateList", new PagedData<SymbolMaster>());
        }

        public PagedData<TemplateMaster> GetList(string searchText, int page)
        {
            try
            {
                var template = new PagedData<TemplateMaster>();
                var templateMasters = db.TemplateMasters.Where(x => x.TemplateName.ToLower().Contains(searchText) || x.TemplateSubject.ToLower().Contains(searchText)).ToList();

                List<TemplateMaster> templateList = new List<TemplateMaster>();

                templateList.AddRange(templateMasters.Select(x => new TemplateMaster
                {
                    TemplateID = x.TemplateID,
                    TemplateName = x.TemplateName,
                    TemplateSubject = x.TemplateSubject,
                    IsActive = x.IsActive
                }));
                template.Data = templateList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                template.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)templateList.Count() / PageSize));
                template.CurrentPage = page;
                return template;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<TemplateMaster>();
        }

        public ActionResult Template()
        {
            try
            {
                return View(new TemplateModel());
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new TemplateModel());
        }

        public ActionResult Edit(long? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                TemplateMaster templateMaster = db.TemplateMasters.Find(id);
                if (templateMaster == null)
                {
                    return HttpNotFound();
                }
                TemplateModel model = new TemplateModel
                {
                    TemplateID = templateMaster.TemplateID,
                    TemplateName = templateMaster.TemplateName,
                    TemplateSubject = templateMaster.TemplateSubject,
                    TemplateDescription = templateMaster.TemplateDescription,
                    IsActive = templateMaster.IsActive,
                    IsDelete = templateMaster.IsDelete
                };
                return View("Template", model);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View("Template", new TemplateMaster());
        }

        public ActionResult AddOrEditTemplate(TemplateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.TemplateID == 0)
                    {
                        TemplateMaster template = new TemplateMaster
                        {
                            TemplateName = model.TemplateName,
                            TemplateSubject = model.TemplateSubject,
                            TemplateDescription = model.TemplateDescription,
                            IsActive = model.IsActive,
                            IsDelete = model.IsDelete,
                            CreatedBy = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId),
                            CreatedOn = DateTime.Now
                        };
                        db.TemplateMasters.Add(template);
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditTemplate", template.TemplateID.ToString(), "New Template");
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelInsertStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.AddStatus;
                        return RedirectToAction("TemplateList");
                    }
                    else
                    {
                        TemplateMaster template = db.TemplateMasters.Find(model.TemplateID);

                        template.TemplateName = model.TemplateName;
                        template.TemplateSubject = model.TemplateSubject;
                        template.TemplateDescription = model.TemplateDescription;
                        template.IsActive = model.IsActive;
                        template.IsDelete = model.IsDelete;
                        template.ModifiedBy = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId);
                        template.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("AddOrEditTemplate", template.TemplateID.ToString(), "Update template");
                        TempData[CommonConstants.ModelStatus] = CommonConstants.ModelUpdateStatus;
                        TempData[CommonConstants.MessageStatus] = CommonConstants.UpdateStatus;
                        return RedirectToAction("TemplateList");
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            TempData[CommonConstants.ModelStatus] = CommonConstants.ModelInvalidStatus;
            TempData[CommonConstants.MessageStatus] = CommonConstants.InvalidStatus;
            return View("Template", model);
        }

        public ActionResult Delete(bool confirm, long? id)
        {
            try
            {
                if (confirm)
                {
                    TemplateMaster templateMaster = db.TemplateMasters.Find(id);
                    templateMaster.IsDelete = true;
                    db.SaveChanges();
                    SiteActivityErrorsUtil.SiteActivity("Delete Template", id.ToString(), "Delete Template");
                    return RedirectToAction("TemplateList");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("TemplateList");
        }

        #endregion
    }
}