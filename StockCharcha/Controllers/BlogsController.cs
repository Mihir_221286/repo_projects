﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using StockCharcha.DataAccess;
using StockCharcha.Filters;
using StockCharcha.Utils.Utilities;
using StockCharcha.Utils;
using StockCharcha.Helpers;

namespace StockCharcha.Controllers
{
    public class BlogsController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;
        // GET: Blogs
        [SessionExistFilter]
        public ActionResult Index()
        {
            List<Blog> blogList = new List<Blog>();
            var blog = new PagedData<Blog>();
            try
            {
                blogList = db.Blogs.Include(b => b.aspnet_Users).Include(b => b.aspnet_Users1).OrderByDescending(x => x.CreatedOn).ToList();
                blog.Data = blogList.Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                blog.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)blogList.Count() / PageSize));
                blog.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(blog);
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<Blog> blogList = new List<Blog>();
            var blog = new PagedData<Blog>();
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    blog = GetList(searchText, page);
                    return PartialView("_BlogList", blog);
                }

                blogList = db.Blogs.Include(b => b.aspnet_Users).Include(b => b.aspnet_Users1).OrderByDescending(x => x.CreatedOn).ToList();
                blog.Data = blogList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                blog.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)blogList.Count() / PageSize));
                blog.CurrentPage = page;
                return PartialView("_BlogList", blog);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_BlogList", new PagedData<Blog>());
        }

        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        public PagedData<Blog> GetList(string searchText, int page)
        {
            try
            {
                List<Blog> blogList = new List<Blog>();
                var blog = new PagedData<Blog>();
                var blogs = db.Blogs.Where(x => x.Symbol.ToLower().Contains(searchText)).ToList();

                blogList.AddRange(blogs.Select(x => new Blog
                {
                    Id = x.Id,
                    Symbol = x.Symbol,
                    Everyone = x.Everyone,
                    OnlyFollowers = x.OnlyFollowers,
                    TrendInSights = x.TrendInSights,
                    EmailOnReply = x.EmailOnReply,
                    IsPublished = x.IsPublished,
                    CreatedOn = x.CreatedOn
                }));
                blog.Data = blogList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.CreatedOn).ToList();
                blog.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)blogList.Count() / PageSize));
                blog.CurrentPage = page;
                return blog;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<Blog>();
        }
        public JsonResult updateIsPublish(long blogId)
        {
            try
            {
                Blog blog = db.Blogs.Find(blogId);
                blog.IsPublished = false;
                db.SaveChanges();
                SiteActivityErrorsUtil.SiteActivity("UpdateIsPublish", "", "Blog unpublished");
                return Json("Updated.");
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json("Something went wrong.");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
