﻿using StockCharcha.DataAccess;
using StockCharcha.Filters;
using StockCharcha.Helpers;
using StockCharcha.Models;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace StockCharcha.Controllers
{
    public class SiteActivityController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        public const int PageSize = CommonConstants.PageSize;
        [SessionExistFilter]
        public ActionResult Index()
        {
            List<SiteActivityModel> siteActivityList = new List<SiteActivityModel>();
            var siteActivity = new PagedData<SiteActivityModel>();
            try
            {
                var siteActivities = db.SiteActivities.OrderByDescending(x => x.ModifiedON).ToList();

                siteActivityList.AddRange(siteActivities.Select(x => new SiteActivityModel
                {
                    SiteActivityID = x.SiteActivityID,
                    ModuleName = x.ModuleName,
                    WebURL = x.WebURL,
                    ModifiedName = x.ModifiedBY != Guid.Empty && x.ModifiedBY != null ? Membership.GetUser((Guid)x.ModifiedBY).UserName.ToString() : string.Empty,
                    ModifiedONDate = String.Format("{0:dd/MM/yyyy}", x.ModifiedON)
                }));
                siteActivity.Data = siteActivityList.Take(PageSize).OrderByDescending(x => x.ModifiedON).ToList();
                siteActivity.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)siteActivityList.Count() / PageSize));
                siteActivity.CurrentPage = 1;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(siteActivity);
        }

        public ActionResult GetListByPage(string searchText, int page)
        {
            List<SiteActivityModel> siteActivityList = new List<SiteActivityModel>();
            var siteActivity = new PagedData<SiteActivityModel>();
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    siteActivity = GetList(searchText, page);
                    return PartialView("_SiteActivityList", siteActivity);
                }

                var siteActivities = db.SiteActivities.OrderByDescending(x => x.ModifiedON).ToList();

                siteActivityList.AddRange(siteActivities.Select(x => new SiteActivityModel
                {
                    SiteActivityID = x.SiteActivityID,
                    ModuleName = x.ModuleName,
                    WebURL = x.WebURL,
                    ModifiedName = x.ModifiedBY != Guid.Empty && x.ModifiedBY != null ? Membership.GetUser((Guid)x.ModifiedBY).UserName.ToString() : string.Empty,
                    ModifiedONDate = String.Format("{0:dd/MM/yyyy}", x.ModifiedON)
                }));
                siteActivity.Data = siteActivityList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.ModifiedON).ToList();
                siteActivity.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)siteActivityList.Count() / PageSize));
                siteActivity.CurrentPage = page;
                return PartialView("_SiteActivityList", siteActivity);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_SiteActivityList", new PagedData<Blog>());
        }

        //[HttpGet]
        public PagedData<SiteActivityModel> GetList(string searchText, int page)
        {
            List<SiteActivityModel> siteActivityList = new List<SiteActivityModel>();
            var siteActivity = new PagedData<SiteActivityModel>();
            try
            {
                var siteActivities = db.SiteActivities.Where(x => x.ModuleName.ToLower().Contains(searchText) || x.WebURL.ToLower().Contains(searchText)).OrderByDescending(x => x.ModifiedON).ToList();

                siteActivityList.AddRange(siteActivities.Select(x => new SiteActivityModel
                {
                    SiteActivityID = x.SiteActivityID,
                    ModuleName = x.ModuleName,
                    WebURL = x.WebURL,
                    ModifiedName = x.ModifiedBY != Guid.Empty && x.ModifiedBY != null ? Membership.GetUser((Guid)x.ModifiedBY).UserName.ToString() : string.Empty,
                    ModifiedONDate = String.Format("{0:dd/MM/yyyy}", x.ModifiedON)
                }));
                siteActivity.Data = siteActivityList.Skip(PageSize * (page - 1)).Take(PageSize).OrderByDescending(x => x.ModifiedON).ToList();
                siteActivity.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)siteActivityList.Count() / PageSize));
                siteActivity.CurrentPage = page;
                return siteActivity;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new PagedData<SiteActivityModel>();
        }
    }
}