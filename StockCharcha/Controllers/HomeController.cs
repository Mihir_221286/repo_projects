﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web.Mvc;
using StockCharcha.Models.Web;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using StockCharcha.DataAccess;
using System.Web.Security;
using StockCharcha.Utils;
using StockCharcha.Utils.Utilities;
using StockCharcha.Utils.Enums;
using TemplateParser;
using System.Data.SqlClient;
using System.Globalization;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Text;

namespace StockCharcha.Controllers
{
    public class HomeController : Controller
    {
        private StockCharchaEntities db = new StockCharchaEntities();
        static bool currentRequest = false;
        static int newIdeaCount = 0;//used for idealist count
        bool IsStockMarketOpenClose = false;

        #region "Stock Landing Page"
        public ActionResult Index()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                    IsStockMarketOpenClose = IsStockMarketOpenClose
                };
                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }

        #endregion

        #region Dashboard

        public ActionResult Dashboard()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    StockModel stockModel = new StockModel
                    {
                        StockMenuList = Menu(),
                        LoggedInUser = SetLoggedInUser(),
                        NseStockDetail = NseStockDetail(),
                        BseStockDetail = BseStockDetail(),
                        IsStockMarketOpenClose = IsStockMarketOpenClose
                    };
                    return View(stockModel);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }

        public JsonResult DashboardBubbles(Int32 treandInSight)
        {
            string bubbleLink = "";
            try
            {
                using (StockCharchaEntities context = new StockCharchaEntities())
                {
                    string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                    builder.ConnectTimeout = 2500;
                    SqlConnection con = new SqlConnection(builder.ConnectionString);
                    con.Open();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "GetDashboardBubbles";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TrendInsights", treandInSight);
                        cmd.Parameters.AddWithValue("@UserId", treandInSight == -2 ? GetCurrentLoggedinUserId() : Guid.Empty);
                        bubbleLink = Convert.ToString(cmd.ExecuteScalar());
                    }
                    con.Close();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = true, html = bubbleLink });
        }

        #endregion

        #region "Timeline Page"
        public ActionResult Timeline()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    int BlockSize = CommonConstants.BlockSize;
                    int streamPrefrence = (int)db.Settings.FirstOrDefault().StreamPrefrence;
                    StockModel stockModel = new StockModel
                    {
                        StockMenuList = Menu(),
                        LoggedInUser = SetLoggedInUser(),
                        NseStockDetail = NseStockDetail(),
                        BseStockDetail = BseStockDetail(),
                        ShareIdeaBlogModelList = GetTimelineBlogItems(1, BlockSize, "allIdea", streamPrefrence),
                        //StockUpdate = TimeSpan.FromSeconds(Convert.ToInt64(db.Settings.Select(x => x.StockUpdate).FirstOrDefault())).TotalMilliseconds,
                        UserRecentlyViewedList = RecentlyViewedList(string.Empty),
                        RecentlyViewedUserList = RecentlyViewedUserList(string.Empty),
                        WatchListModel = ProfileWatchListBind(),
                        StreamPrefrence = streamPrefrence,
                        //LastFetchIdeaDate = DateTime.Now,
                        NewIdeaCount = -1,//newIdeaCount
                        IsStockMarketOpenClose = IsStockMarketOpenClose
                    };
                    ViewBag.userId = userId;
                    return View(stockModel);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }

        public List<ShareIdeaBlogListModel> GetTopTimelineList(int option = 0)
        {
            Guid userId = GetCurrentLoggedinUserId();
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogList = GetAllTimelineList();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                if ((StreamPrefrence)option == StreamPrefrence.Comments)
                {
                    var blogTopListComments = (from b in shareIdeaBlogList
                                               join bc in db.BlogComments on b.BlogId equals bc.BlogId
                                               group new { b, bc } by new { b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogId, b.ReshareBlock }
                                                   into x
                                                   select new
                                                   {
                                                       BlogId = x.Key.BlogId,
                                                       Description = x.Key.Description,
                                                       TrendInSights = x.Key.TrendInSights,
                                                       ReShareId = x.Key.ReShareId,
                                                       CreatedOn = x.Key.CreatedOn,
                                                       CreatedBy = x.Key.CreatedBy,
                                                       Total = x.Count(),
                                                       ReshareBlock = x.Key.ReshareBlock
                                                   }).OrderByDescending(a => a.Total).ToList();
                    shareIdeaBlogListModel.AddRange(blogTopListComments.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId ?? 0,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId ?? 0),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                        ReshareBlock = x.ReshareBlock
                    }));
                }
                else if ((StreamPrefrence)option == StreamPrefrence.Reshare)
                {
                    var blogTopListReshare = (from b in shareIdeaBlogList
                                              join bc in db.Blogs on b.BlogId equals bc.ReShareId
                                              group new { b, bc } by new { b.BlogId, b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, b.ReshareBlock }
                                                  into x
                                                  select new
                                                  {
                                                      BlogId = x.Key.BlogId,
                                                      Description = x.Key.Description,
                                                      TrendInSights = x.Key.TrendInSights,
                                                      ReShareId = x.Key.ReShareId,
                                                      CreatedOn = x.Key.CreatedOn,
                                                      CreatedBy = x.Key.CreatedBy,
                                                      Total = x.Count(),
                                                      ReshareBlock = x.Key.ReshareBlock
                                                  }).OrderByDescending(a => a.Total).ToList();

                    shareIdeaBlogListModel.AddRange(blogTopListReshare.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                        ReshareBlock = x.ReshareBlock
                    }));
                }
                else if ((StreamPrefrence)option == StreamPrefrence.Likes)
                {
                    var blogTopListLike = (from b in shareIdeaBlogList
                                           join bc in db.BlogCommentsLikes on b.BlogId equals bc.BlogCommentsId
                                           where bc.Isdeleted == false
                                           group new { b, bc } by new { b.BlogId, b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogCommentsId, bc.Isdeleted, b.ReshareBlock }
                                               into x
                                               select new
                                               {
                                                   BlogId = x.Key.BlogId,
                                                   Description = x.Key.Description,
                                                   TrendInSights = x.Key.TrendInSights,
                                                   ReShareId = x.Key.ReShareId,
                                                   CreatedOn = x.Key.CreatedOn,
                                                   CreatedBy = x.Key.CreatedBy,
                                                   IsDeleted = x.Key.Isdeleted,
                                                   Total = x.Count(),
                                                   ReshareBlock = x.Key.ReshareBlock
                                               }).OrderByDescending(a => a.Total).ToList();
                    shareIdeaBlogListModel.AddRange(blogTopListLike.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                        ReshareBlock = x.ReshareBlock
                    }));
                }
                else
                {

                }
                return shareIdeaBlogListModel.ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }
        public JsonResult GetTopTimelineIdeas(int option)
        {
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                int BlockSize = CommonConstants.BlockSize;
                shareIdeaBlogListModel = GetTimelineBlogItems(1, BlockSize, "topIdea", option);

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetAllTimelineList()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                List<Blog> blogListFollower = new List<Blog>();
                var userFollowers = string.Join(",", db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).Select(x => x.FollowTo));
                var symbolWatchlist = string.Join(",", db.UserWatchLists.Where(x => x.UserId == userId && x.IsDeleted == false).Select(x => x.Symbol));
                blogListFollower = db.Blogs
                                  .Where(x => (x.IsPublished == true)
                                      &&
                                      (userFollowers.Contains(x.CreatedBy.ToString())
                                        ||
                                        (symbolWatchlist.Contains(x.Symbol) && x.Everyone == true)
                                      )
                                      && x.CreatedBy != userId).ToList();

                shareIdeaBlogListModel.AddRange(blogListFollower.Select(x => new ShareIdeaBlogListModel
                {
                    BlogId = x.Id,
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                    TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                         Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                         Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                         Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                         Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                         string.Empty,
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    CreatedBy = x.CreatedBy,
                    ReShareId = x.ReShareId > 0 ? true : false,
                    ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                    Like = IsLiked(x.Id),
                    CommentsCount = db.BlogComments.Where(s => s.BlogId == x.Id).ToList().Count,
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                    IsLink = Convert.ToBoolean(x.IsLink)
                }));
                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }
        public JsonResult GetAllTimelineIdeas()
        {
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                int BlockSize = CommonConstants.BlockSize;
                shareIdeaBlogListModel = GetTimelineBlogItems(1, BlockSize, "allIdea");

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetChartTimelineList()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();
                List<ShareIdeaBlogListModel> shareIdeaBlogList = GetAllTimelineList().Where(x => x.Description.ToString().Contains("<img")).ToList();

                shareIdeaBlogListModel.AddRange(shareIdeaBlogList.Select(x => new ShareIdeaBlogListModel
                {
                    BlogId = x.BlogId,
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    TrendInSights = x.TrendInSights,
                    TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 0 ? CommonConstants.StrongBuy :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 1 ? CommonConstants.Buy :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 2 ? CommonConstants.Hold :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 3 ? CommonConstants.Sell :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 4 ? CommonConstants.StrongSell :
                                         string.Empty,
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    CreatedBy = x.CreatedBy,
                    ReShareId = x.ReShareId,
                    Like = IsLiked(x.BlogId),
                    CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                    ReshareBlock = x.ReshareBlock
                }));

                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }
        public JsonResult GetChartTimelineIdeas()
        {
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                int BlockSize = CommonConstants.BlockSize;
                shareIdeaBlogListModel = GetTimelineBlogItems(1, BlockSize, "chartIdea");

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetLinksTimelineList()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();
                //List<ShareIdeaBlogListModel> shareIdeaBlogList = GetAllTimelineList().Where(x => x.Description.ToString().Contains("<a") && !x.Description.ToString().Contains("prettyPhoto")).ToList();
                List<ShareIdeaBlogListModel> shareIdeaBlogList = GetAllTimelineList().Where(x => x.IsLink == true).ToList();

                shareIdeaBlogListModel.AddRange(shareIdeaBlogList.Select(x => new ShareIdeaBlogListModel
                {
                    BlogId = x.BlogId,
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    TrendInSights = x.TrendInSights,
                    TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 0 ? CommonConstants.StrongBuy :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 1 ? CommonConstants.Buy :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 2 ? CommonConstants.Hold :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 3 ? CommonConstants.Sell :
                                         Convert.ToInt32(Enum.Parse(typeof(TrendInsights), x.TrendInSights)) == 4 ? CommonConstants.StrongSell :
                                         string.Empty,
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    CreatedBy = x.CreatedBy,
                    ReShareId = x.ReShareId,
                    Like = IsLiked(x.BlogId),
                    CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                    ReshareBlock = x.ReshareBlock
                }));

                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }
        public JsonResult GetLinksTimelineIdeas()
        {
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                int BlockSize = CommonConstants.BlockSize;
                shareIdeaBlogListModel = GetTimelineBlogItems(1, BlockSize, "linksIdea");

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        [HttpPost]
        public JsonResult InfinateTimelineScroll(int BlockNumber, string selectedClass, int option = 0)
        {
            try
            {
                int BlockSize = CommonConstants.BlockSize;
                List<ShareIdeaBlogListModel> ideaBlogs = GetTimelineBlogItems(BlockNumber, BlockSize, selectedClass, option);

                if (ideaBlogs.Count > 0)
                {
                    JsonModel jsonModel = new JsonModel();
                    jsonModel.NoMoreData = ideaBlogs.Count < BlockSize;
                    StockModel stockModel = new StockModel
                    {
                        ShareIdeaBlogModelList = ideaBlogs//ShareIdeaBlogList(setSymbol)
                    };
                    jsonModel.HTMLString = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                    return Json(new { IsSuccess = true, html = jsonModel });
                }
                else
                {
                    return Json(new { IsSuccess = false });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false });
        }
        public List<ShareIdeaBlogListModel> GetTimelineBlogItems(int BlockNumber, int BlockSize, string selectedClass, int option = 0)
        {
            try
            {
                int startIndex = (BlockNumber - 1) * BlockSize;
                List<ShareIdeaBlogListModel> ShareIdeaBlogLists = new List<ShareIdeaBlogListModel>();
                if (selectedClass == "topIdea")
                {
                    ShareIdeaBlogLists = GetTopTimelineList(option);
                }
                else if (selectedClass == "allIdea")
                {
                    ShareIdeaBlogLists = GetAllTimelineList();
                }
                else if (selectedClass == "chartIdea")
                {
                    ShareIdeaBlogLists = GetChartTimelineList();
                }
                else if (selectedClass == "linksIdea")
                {
                    ShareIdeaBlogLists = GetLinksTimelineList();
                }
                else
                {
                    ShareIdeaBlogLists = new List<ShareIdeaBlogListModel>();
                }
                return ShareIdeaBlogLists.Skip(startIndex).Take(BlockSize).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        #endregion

        #region "Search Auto Complete"

        public List<SymbolModel> SymbolSearchList(string term)
        {
            try
            {
                List<SymbolModel> SymbolModelList = new List<SymbolModel>();

                using (StockCharchaEntities context = new StockCharchaEntities())
                {
                    string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                    builder.ConnectTimeout = 2500;
                    SqlConnection con = new SqlConnection(builder.ConnectionString);
                    System.Data.Common.DbDataReader sqlReader;
                    con.Open();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "SymbolSearchList";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@term", SqlDbType.NVarChar).Value = term;
                        sqlReader = (System.Data.Common.DbDataReader)cmd.ExecuteReader();

                        while (sqlReader.Read())
                        {
                            SymbolModel symbolModel = new SymbolModel();
                            symbolModel.Id = sqlReader.GetInt64(0);
                            symbolModel.NseSymbol = !sqlReader.IsDBNull(sqlReader.GetOrdinal("NSESymbol")) ? sqlReader.GetString(1) : string.Empty;
                            symbolModel.BseSymbol = !sqlReader.IsDBNull(sqlReader.GetOrdinal("BSESymbol")) ? sqlReader.GetString(2) : string.Empty;
                            symbolModel.SecurityId = !sqlReader.IsDBNull(sqlReader.GetOrdinal("SecurityId")) ? sqlReader.GetString(3) : string.Empty;
                            symbolModel.IsinNumber = !sqlReader.IsDBNull(sqlReader.GetOrdinal("ISINNumber")) ? sqlReader.GetString(4) : string.Empty;
                            symbolModel.CompanyName = !sqlReader.IsDBNull(sqlReader.GetOrdinal("CompanyName")) ? sqlReader.GetString(5) : string.Empty;
                            SymbolModelList.Add(symbolModel);
                        }
                    }
                    con.Close();
                }
                return SymbolModelList;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<SymbolModel>();
        }
        public List<UserListModel> UserSearchList(string term)
        {
            try
            {
                List<UserListModel> userModelList = new List<UserListModel>();

                using (StockCharchaEntities context = new StockCharchaEntities())
                {
                    string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                    builder.ConnectTimeout = 2500;
                    SqlConnection con = new SqlConnection(builder.ConnectionString);
                    System.Data.Common.DbDataReader sqlReader;
                    con.Open();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "UserSearchList";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@term", SqlDbType.NVarChar).Value = term;
                        sqlReader = (System.Data.Common.DbDataReader)cmd.ExecuteReader();

                        while (sqlReader.Read())
                        {
                            UserListModel userModel = new UserListModel();
                            userModel.UserId = sqlReader.GetGuid(0);
                            userModel.Username = !sqlReader.IsDBNull(sqlReader.GetOrdinal("UserName")) ? sqlReader.GetString(1) : string.Empty;
                            userModel.Firstname = !sqlReader.IsDBNull(sqlReader.GetOrdinal("FirstName")) ? sqlReader.GetString(2) : string.Empty;
                            userModel.Lastname = !sqlReader.IsDBNull(sqlReader.GetOrdinal("LastName")) ? sqlReader.GetString(3) : string.Empty;
                            userModelList.Add(userModel);
                        }
                    }
                    con.Close();
                }
                return userModelList;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<UserListModel>();
        }

        public List<SymbolModel> SearchList(string searchString, int searchOption)
        {
            try
            {
                string term = searchString.ToLower();
                List<SymbolModel> symbolModelList = new List<SymbolModel>();

                if (HeaderSearchOptions.Stock == (HeaderSearchOptions)searchOption)
                {
                    var symbols = SymbolSearchList(term);

                    symbolModelList.AddRange(symbols.AsEnumerable().Select(x => new SymbolModel
                    {
                        SearchType = 1,
                        NseSymbol = x.NseSymbol,
                        BseSymbol = x.BseSymbol,
                        CompanyName = x.CompanyName,
                        IsinNumber = x.IsinNumber
                    }));
                }
                else if (HeaderSearchOptions.User == (HeaderSearchOptions)searchOption)
                {
                    var users = UserSearchList(term);
                    //users
                    Guid userId = GetCurrentLoggedinUserId();
                    if (userId != Guid.Empty)
                    {
                        symbolModelList.AddRange(users.AsEnumerable().Select(x => new SymbolModel
                        {
                            SearchType = 2,
                            UserId = x.UserId,
                            UserName = x.Username
                        }));
                    }
                }
                else//All
                {
                    var symbols = SymbolSearchList(term);
                    var users = UserSearchList(term);

                    symbolModelList.AddRange(symbols.AsEnumerable().Select(x => new SymbolModel
                    {
                        SearchType = 1,
                        NseSymbol = x.NseSymbol,
                        BseSymbol = x.BseSymbol,
                        CompanyName = x.CompanyName,
                        IsinNumber = x.IsinNumber
                    }));


                    //users
                    Guid userId = GetCurrentLoggedinUserId();

                    if (userId != Guid.Empty)
                    {
                        symbolModelList.AddRange(users.AsEnumerable().Select(x => new SymbolModel
                        {
                            SearchType = 2,
                            UserId = x.UserId,
                            UserName = x.Username
                        }));
                    }
                }

                if (symbolModelList.Count() > 0)
                {
                    return symbolModelList = symbolModelList.ToList();
                }
                else
                {
                    return new List<SymbolModel>();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<SymbolModel>();
        }

        public JsonResult SymbolList(string term, int searchOption)
        {
            try
            {
                List<SymbolModel> symbolModelList = new List<SymbolModel>();
                symbolModelList = SearchList(term, searchOption);
                symbolModelList = symbolModelList.Take(10).ToList();
                //All
                if (symbolModelList.Count() > 0 && symbolModelList.Count() == 10)
                {
                    symbolModelList.Add(new SymbolModel
                    {
                        SearchType = 3,//All
                        SearchedValue = term
                    });
                }

                return Json(symbolModelList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new List<SymbolModel>(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(string searchString)
        {
            try
            {
                List<SymbolModel> symbolModelList = SearchList(searchString, 2);

                const int PageSize = CommonConstants.PageSize;

                if (symbolModelList.Count() > 0)
                {
                    ViewBag.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)symbolModelList.Count() / PageSize));
                    ViewBag.CurrentPage = 1;
                    ViewBag.searchString = searchString;

                    StockModel stockModel = new StockModel
                    {
                        StockMenuList = Menu(),
                        LoggedInUser = SetLoggedInUser(),
                        NseStockDetail = NseStockDetail(),
                        BseStockDetail = BseStockDetail(),
                        symbolModelList = symbolModelList.Take(PageSize).OrderBy(x => x.CompanyName).ToList()
                    };
                    return View(stockModel);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }

        public ActionResult GetSearchListByPage(string searchString, int page)
        {
            List<SymbolModel> symbolModelList = new List<SymbolModel>();
            List<SymbolModel> symbolModelListData = new List<SymbolModel>();
            //var symbol = new PagedData<SymbolMaster>();
            try
            {
                const int PageSize = CommonConstants.PageSize;
                symbolModelList = SearchList(searchString, 2);
                symbolModelListData = symbolModelList.Skip(PageSize * (page - 1)).Take(PageSize).ToList();
                StockModel stockModel = new StockModel
                {
                    symbolModelList = symbolModelListData
                };

                ViewBag.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)symbolModelList.Count() / PageSize));
                ViewBag.CurrentPage = page;
                ViewBag.searchString = searchString;
                return PartialView("_SearchList", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_SearchList", new StockModel());
        }

        #endregion

        #region "Header Share Idea"

        [HttpGet]
        public ActionResult GetHeadershareIdea()
        {
            try
            {
                HeaderShareIdea stockModel = new HeaderShareIdea
                {
                    ShareIdeaTextLimit = (int)db.Settings.FirstOrDefault().ShareIdeaText
                };
                return PartialView("_HeaderShareIdea", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HeaderShareIdea", new StockModel());
        }

        public List<string> headerShareIdeahashTags = new List<string>();
        public string HeaderShareIdeaDescription(string description)
        {
            string input = description;
            string input1 = description;
            //List<string> hashTags = new List<string>();

            foreach (System.Text.RegularExpressions.Match match in System.Text.RegularExpressions.Regex.Matches(input, "(\\#\\w+) "))
            {
                if (!headerShareIdeahashTags.Contains(match.Groups[1].Value))
                {
                    headerShareIdeahashTags.Add(match.Groups[1].Value);
                    string nseBse = NSE_BSEType(match.Groups[1].Value.Replace("#", "").Trim());
                    input1 = input1.Replace(match.Groups[1].Value, "<a href='javascript:void(0);' onmouseout='Common.CommonMenuHoverComplete()' onmouseover='Common.CommonMenuHover(this,event)' onclick='Common.CommonMenu(this)' id='" + match.Groups[1].Value.Replace("#", "").Trim() + "," + nseBse + "'><b>" + match.Groups[1].Value + "</b></a>");
                }

            }
            string input2 = HeaderShareIdeaDescriptionAt(input1);
            return input2.Replace("\n", "<br/>");
        }
        public string HeaderShareIdeaDescriptionAt(string description)
        {
            string input = description;
            string input1 = description;
            List<string> hashTags1 = new List<string>();

            foreach (System.Text.RegularExpressions.Match match in System.Text.RegularExpressions.Regex.Matches(input, "(\\@\\w+) "))
            {
                if (!hashTags1.Contains(match.Groups[1].Value))
                {
                    hashTags1.Add(match.Groups[1].Value);
                    string nseBse = NSE_BSEType(match.Groups[1].Value.Replace("@", "").Trim());
                    input1 = input1.Replace(match.Groups[1].Value, "<a href='/Profile/" + match.Groups[1].Value.Replace("@", "").Trim() + "'><b>" + match.Groups[1].Value + "</b></a>");
                }
            }
            return input1.Replace("\n", "<br/>");
        }
        [ValidateInput(false)]
        public JsonResult HeaderShareIdea(ShareIdeaBlogModel model)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please Login First." });
                }

                string setImage = string.Empty;
                if (!string.IsNullOrEmpty(model.NewFileName))
                {
                    setImage = "<br/> <a href='" + model.NewFileName + "' rel='prettyPhoto'><img src='" + model.NewFileName + "' alt='' style='height: 150px;'/></a>";
                }
                string shareIdeaDescriptionString = HeaderShareIdeaDescription(model.Description) + setImage;
                if (headerShareIdeahashTags.Count() > 0)
                {

                    foreach (var hashTag in headerShareIdeahashTags)
                    {
                        if (!hashTag.Contains('@'))
                        {
                            string refTag = hashTag.Replace('#', ' ').Trim();
                            Blog blog1 = new Blog
                            {
                                Description = shareIdeaDescriptionString,
                                EmailOnReply = model.EmailOnReply,
                                Everyone = model.Everyone,
                                OnlyFollowers = model.OnlyFollowers,
                                TrendInSights = model.TrendInSights != null ? Convert.ToInt32(model.TrendInSights) : -1,
                                CreatedOn = DateTime.Now,
                                CreatedBy = userId,
                                IsPublished = true,
                                Symbol = refTag,
                                NewFileName = model.NewFileName,
                                OriginalFileName = model.OriginalFileName
                            };
                            db.Blogs.Add(blog1);
                            db.SaveChanges();
                            SiteActivityErrorsUtil.SiteActivity("ShareIdea", blog1.Id.ToString(), "Header Share Idea");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.Description))
                    {
                        Blog blog1 = new Blog
                        {
                            Description = shareIdeaDescriptionString,
                            EmailOnReply = model.EmailOnReply,
                            Everyone = model.Everyone,
                            OnlyFollowers = model.OnlyFollowers,
                            TrendInSights = model.TrendInSights != null ? Convert.ToInt32(model.TrendInSights) : -1,
                            CreatedOn = DateTime.Now,
                            CreatedBy = userId,
                            IsPublished = true,
                            //Symbol = refTag,
                            NewFileName = model.NewFileName,
                            OriginalFileName = model.OriginalFileName
                        };
                        db.Blogs.Add(blog1);
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("ShareIdea", blog1.Id.ToString(), "Header Share Idea");
                    }
                }
                return Json(new { IsSuccess = true });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        #endregion

        #region "TextArea Share Idea Auto Complete"
        [HttpGet]
        public JsonResult SymbolsList(string searchText)
        {
            try
            {
                List<KeyValuePair<string, string>> symbolList = new List<KeyValuePair<string, string>>();
                List<SymbolModel> symbolModelList = new List<SymbolModel>();
                var symbols = SymbolSearchList(searchText);
                var nseList = symbols.AsEnumerable().ToList().Where(x => !string.IsNullOrEmpty(x.NseSymbol)).ToList();  //.Where(x => !string.IsNullOrEmpty(x.NseSymbol)).ToList();
                var bseList = symbols.AsEnumerable().ToList().Where(x => string.IsNullOrEmpty(x.NseSymbol)).ToList();

                if (nseList.Count() > 0)
                {
                    //NSE
                    foreach (var item in nseList)
                    {
                        symbolList.Add(new KeyValuePair<string, string>(item.NseSymbol, item.CompanyName)); //"<b>&nbsp;&nbsp;" + item.CompanyName + "</b><span class='pull-right'>&nbsp;&nbsp;NSE</span>"
                    }
                }
                if (bseList.Count() > 0)
                {
                    //BSE
                    foreach (var item in bseList)
                    {
                        symbolList.Add(new KeyValuePair<string, string>(item.BseSymbol, item.CompanyName));
                    }
                }
                return Json(symbolList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new List<SymbolModel>(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult UserList(string searchText)
        {
            try
            {
                var users = from p in db.RegistrationMasters
                            where p.UserTag.Contains(searchText)
                            select p.UserTag;
                return Json(users.ToList().ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new List<SymbolModel>(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Registration And Login"

        public JsonResult Registration(string firstName, string lastName, string userName, string email, string password)
        {
            try
            {
                MembershipUser checkUser = Membership.GetUser(userName);
                if (checkUser != null)
                {
                    return Json(new { IsSuccess = false, html = "User Name already exist." });
                }
                else
                {
                    string userNamee = Membership.GetUserNameByEmail(email);
                    if (!string.IsNullOrEmpty(userNamee))
                    {
                        return Json(new { IsSuccess = false, html = "Email already exist." });
                    }
                }

                if (string.IsNullOrEmpty(userName) && string.IsNullOrEmpty(email) && string.IsNullOrEmpty(password))
                {
                    return Json(new { IsSuccess = false, html = "Invalid Form." });
                }
                else
                {
                    Membership.CreateUser(userName, password, email);
                    Roles.AddUserToRole(userName, "User");

                    string randomCode = GlobalUtil.GetRandomNumber(8);
                    RegistrationMaster registrationModel = new RegistrationMaster
                    {
                        FirstName = firstName,
                        LastName = lastName,
                        UserId = new Guid(Membership.GetUser(userName).ProviderUserKey.ToString()),
                        CreatedOn = DateTime.Now,
                        IsActive = false,
                        ActivationCode = randomCode,
                        UserTag = userName.Trim().Replace(" ", "_")
                    };
                    db.RegistrationMasters.Add(registrationModel);
                    db.SaveChanges();
                    //email = "jaimin.shah@diinsy.com";
                    string body = sendActivationLink(userName, email, password, randomCode);
                    MailUtil.SendMail(email, "", "", "Stock Charcha Registration", body, false, "", "Registration", registrationModel.Id.ToString(), "User registration");
                    SiteActivityErrorsUtil.SiteActivity("Web-Registration", registrationModel.Id.ToString(), "New user registration");

                    return Json(new { IsSuccess = true, html = email });
                }

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public string sendActivationLink(string userName, string email, string password, string activationCode)
        {
            string tempPath = Request.PhysicalApplicationPath + "/EmailTemplates/registration.html";

            Hashtable tableVariables = new Hashtable();
            tableVariables.Add("UserName", userName.Trim());
            tableVariables.Add("Email", email.Trim());
            tableVariables.Add("Password", password.Trim());
            tableVariables.Add("ActivationCode", activationCode);

            string ActivationLink = "<a title='Here' href='" + ConfigurationManager.AppSettings["ImagePath"] + "/Activate' target='_blank'>Here</a>";
            //string DirectActivationLink = "<a title='Here' href='" + ConfigurationManager.AppSettings["ImagePath"] + "/Activate?email=" + EncryptDecrpt.Encrypt(email.Trim(), true) + "&activationCode=" + activationCode.Trim() + "' target='_blank'>Here</a>"; ;
            string DirectActivationLink = "<a title='Here' href='" + ConfigurationManager.AppSettings["ImagePath"] + "/DirectActivate/" + email.Trim() + "/" + activationCode.Trim() + "' target='_blank'>Here</a>"; ;

            tableVariables.Add("ActivationLink", ActivationLink);
            tableVariables.Add("DirectActivationLink", DirectActivationLink);

            Parser parser = new Parser(tempPath, tableVariables);
            return parser.Parse();
        }

        public ActionResult Activatation()
        {
            try
            {

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View();
        }

        public ActionResult Activate(string email, string activationCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(activationCode))
                {
                    string userName = Membership.GetUserNameByEmail(email).ToString();
                    Guid userId = (Guid)Membership.GetUser(userName).ProviderUserKey;
                    var checkUser = db.RegistrationMasters.FirstOrDefault(x => x.UserId == userId && x.ActivationCode == activationCode);
                    if (checkUser != null)
                    {
                        RegistrationMaster updateUser = db.RegistrationMasters.FirstOrDefault(x => x.UserId == userId);
                        updateUser.IsActive = true;
                        db.SaveChanges();

                        WebUtil.SetSessionValue<Guid>(CommonConstants.UserIdWeb, userId);
                        WebUtil.SetSessionValue<string>(CommonConstants.UserNameWeb, userName);
                        WebUtil.SetSessionValue<string>(CommonConstants.UserEmailWeb, email);
                    }
                    return Json(new { IsSuccess = true, html = "Activation done successfully." });
                }
                else
                {
                    return Json(new { IsSuccess = false, html = "Email or activation code not valid." });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "There is something wrong please contact support team." });
        }

        public ActionResult DirectActivate(string email, string activationCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(activationCode))
                {
                    string userName = Membership.GetUserNameByEmail(email).ToString();
                    Guid userId = (Guid)Membership.GetUser(userName).ProviderUserKey;
                    var checkUser = db.RegistrationMasters.FirstOrDefault(x => x.UserId == userId && x.ActivationCode == activationCode);
                    if (checkUser != null)
                    {
                        RegistrationMaster updateUser = db.RegistrationMasters.FirstOrDefault(x => x.UserId == userId);
                        updateUser.IsActive = true;
                        db.SaveChanges();

                        WebUtil.SetSessionValue<Guid>(CommonConstants.UserIdWeb, userId);
                        WebUtil.SetSessionValue<string>(CommonConstants.UserNameWeb, userName);
                        WebUtil.SetSessionValue<string>(CommonConstants.UserEmailWeb, email);
                        return RedirectPermanent("/Profile/" + userName);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public JsonResult Login(string email, string password)
        {
            try
            {
                string username = Membership.GetUserNameByEmail(email);
                if (string.IsNullOrEmpty(username))
                {
                    return Json(new { IsSuccess = false, html = "You are not registered with us. Please do registration first." });
                }
                MembershipUser user = Membership.GetUser(Membership.GetUserNameByEmail(email));
                Guid getuserId = (Guid)user.ProviderUserKey;
                var checkUser = db.RegistrationMasters.FirstOrDefault(x => x.UserId == getuserId && x.IsActive == true);
                if (checkUser != null)
                {
                    string userName = Membership.GetUserNameByEmail(email);
                    bool userValidate = Membership.ValidateUser(userName, password);
                    if (userValidate)
                    {
                        //set session
                        string userId = Membership.GetUser(userName).ProviderUserKey.ToString();
                        WebUtil.SetSessionValue<Guid>(CommonConstants.UserIdWeb, new Guid(userId));
                        WebUtil.SetSessionValue<string>(CommonConstants.UserNameWeb, userName);
                        WebUtil.SetSessionValue<string>(CommonConstants.UserEmailWeb, email);
                        SiteActivityErrorsUtil.SiteActivity("Web-Login", userId.ToString(), "user login");
                        bool syspass = user.Comment == "syspass" ? true : false;
                        return Json(new { IsSuccess = true, validUserName = userName, html = "Login Successful", syspass = syspass });
                    }
                    else
                    {
                        return Json(new { IsSuccess = false, html = "The email or password you entered didn't match." });
                    }
                }
                else
                {
                    return Json(new { IsSuccess = false, html = "Please do account activation first." });
                }

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public JsonResult LogOut()
        {
            try
            {
                WebUtil.ClearSession();
                SiteActivityErrorsUtil.SiteActivity("Web-Logout", "", "user logout");
                return Json(new { IsSuccess = true });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = true });
        }

        public LoggedInUser SetLoggedInUser()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                string[] userImages = new string[0];
                string userImage = string.Empty;
                RegistrationMaster registrationMaster = new RegistrationMaster();
                if (userId != Guid.Empty)
                {
                    registrationMaster = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                    userImage = GetUserImage(userId, "32");
                }

                LoggedInUser loggedInUser = new LoggedInUser
                {
                    FirstName = registrationMaster.FirstName,
                    UserId = userId,
                    UserName = GetCurrentLoggedinUserName(),
                    RegistrationId = registrationMaster.Id,
                    userImage = userImage
                };
                return loggedInUser;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new LoggedInUser();
        }

        public JsonResult LoggedInUser()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser()
                };
                var stringView = RenderRazorViewToString("_JoinSignIn", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public JsonResult ResetPassword(string email)
        {
            try
            {
                string userName = Membership.GetUserNameByEmail(email);
                if (!string.IsNullOrEmpty(userName))
                {
                    MembershipUser user = Membership.GetUser(userName);
                    if (user != null)
                    {
                        string password = user.ResetPassword();
                        //user.ChangePassword(password, "111111");
                        user.Comment = "syspass";
                        Membership.UpdateUser(user);
                        var emailModel = ResetPasswordEmailBody(userName, email, password);
                        MailUtil.SendMail(email, "", "", emailModel.subject, emailModel.body, false, "", emailModel.emailType, string.Empty, emailModel.emailfor);
                        SiteActivityErrorsUtil.SiteActivity("Web-ResetPassword", user.ProviderUserKey.ToString(), "user reset password");
                        return Json(new { IsSuccess = true, html = "Password reset successful" });
                    }
                }
                else
                {
                    return Json(new { IsSuccess = false, html = "Email address not registered." });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Password reset unsuccessful" });
        }

        public EmailModel ResetPasswordEmailBody(string userName, string email, string password)
        {
            var forgotPasswordTemplate = db.TemplateMasters.Where(x => x.TemplateName.ToLower().Trim() == ("Forgot Password").ToLower().Trim()).FirstOrDefault();
            if (forgotPasswordTemplate == null)
            {
                return new EmailModel();
            }
            EmailModel emailModel = new EmailModel
            {
                subject = forgotPasswordTemplate.TemplateSubject,
                body = forgotPasswordTemplate.TemplateDescription.Replace("{{UserName}}", userName.Trim()).Replace("{{Email}}", email).Replace("{{Password}}", password),
                emailType = "Reset Password",
                emailfor = "User reset password"
            };
            return emailModel;
        }

        public TemplateMaster GetTemplateMailDetails(string TemplateName)
        {
            try
            {
                return db.TemplateMasters.Where(x => x.TemplateName.ToLower().Trim() == TemplateName.ToLower().Trim()).FirstOrDefault();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return null;
        }

        public ActionResult ChangePassword()
        {
            try
            { }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View();
        }

        public ActionResult ChangePasswordConfirm(string email, string currentPassword, string newPassword)
        {
            try
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(currentPassword) && !string.IsNullOrEmpty(newPassword))
                {
                    string userName = Membership.GetUserNameByEmail(email).ToString();
                    MembershipUser user = Membership.GetUser(userName);
                    var checkUser = db.RegistrationMasters.FirstOrDefault(x => x.UserId == (Guid)user.ProviderUserKey);

                    if (checkUser != null)
                    {
                        bool isChanged = user.ChangePassword(currentPassword, newPassword);
                        SiteActivityErrorsUtil.SiteActivity("Web-ChangePassword", user.ProviderUserKey.ToString(), "user change password");
                        if (isChanged)
                        {
                            user.Comment = null;
                            Membership.UpdateUser(user);
                            MailUtil.SendMail(email, "", "", "Stock Charcha change Password", "Password changed Successfully", false, "", "Change Password", string.Empty, "User change password");
                            return Json(new { IsSuccess = true, html = "Password changed successfully." });
                        }
                        else
                        {
                            return Json(new { IsSuccess = false, html = "There is something wrong please contact support team." });
                        }
                    }
                }
                else
                {
                    return Json(new { IsSuccess = false, html = "Email or Password not valid." });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "There is something wrong please contact support team." });
        }

        #endregion

        #region "NseBse Stock"
        public NseStockDetail NseStockDetail()
        {
            try
            {
                StockDetail nseStockDetail = BindNseStock("NIFTY");
                NseStockDetail nseStockDetailModel = new NseStockDetail
                {
                    StockRate = nseStockDetail.StockRate,
                    StockRatePercentage = nseStockDetail.StockRatePercentage,
                    StockUpDown = nseStockDetail.StockUpDown,
                    StockSymbol = nseStockDetail.StockSymbol,
                };
                return nseStockDetailModel;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new NseStockDetail();
        }
        public BseStockDetail BseStockDetail()
        {
            try
            {
                StockDetail bseStockDetail = BindBseStock(CommonConstants.Sensex);
                BseStockDetail bseStockDetailModel = new BseStockDetail
                {
                    StockRate = bseStockDetail.StockRate,
                    StockRatePercentage = bseStockDetail.StockRatePercentage,
                    StockUpDown = bseStockDetail.StockUpDown,
                    StockSymbol = bseStockDetail.StockSymbol,
                };
                return bseStockDetailModel;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new BseStockDetail();
        }
        public ActionResult NseBseTicker()
        {

            StockModel stockModel = new StockModel();
            try
            {
                stockModel.NseStockDetail = NseStockDetail();
                stockModel.BseStockDetail = BseStockDetail();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_NseBse", stockModel);
        }

        #endregion

        #region "Stock Details"

        public ActionResult Stock(string symbol)
        {
            try
            {
                bool nse = false;
                bool bse = false;

                var nseList = db.SymbolMasters.Where(x => x.NSESymbol.ToLower() == symbol.ToLower()).ToList();
                var getSymbol = db.SymbolMasters.Where(x => x.NSESymbol.ToLower() == symbol.ToLower() || x.BSESymbol.ToLower() == symbol.ToLower()).FirstOrDefault();

                if (!string.IsNullOrEmpty(getSymbol.NSESymbol))
                {
                    if (nseList.Count() > 0) { nse = true; } else { bse = true; }
                    if (bse)
                    {
                        return RedirectToRoute(new { controller = "Home", action = "Stock", symbol = !string.IsNullOrEmpty(getSymbol.NSESymbol) ? getSymbol.NSESymbol : getSymbol.BSESymbol });
                    }
                }
                else
                {
                    bse = true;
                }

                /*if (Request.Url != null)
                {
                    nse = Request.Url.AbsoluteUri.ToUpper().Contains("/NSE/");
                    bse = Request.Url.AbsoluteUri.ToUpper().Contains("/BSE/");
                }*/
                Guid userId = GetCurrentLoggedinUserId();
                string nseSymbol = nse ? symbol : string.Empty;
                string bseSymbol = bse ? symbol : string.Empty;
                string setSymbol = nse ? symbol : bse ? symbol : "";
                string type = nse ? CommonConstants.NSE : bse ? CommonConstants.BSE : string.Empty;

                RecentlyViewed(userId, setSymbol, type);
                int BlockSize = CommonConstants.BlockSize;
                int streamPrefrence = (int)db.Settings.FirstOrDefault().StreamPrefrence;

                /*string nseMaxConsensusName = BindConsensusChart(nseSymbol).Count > 0 ?
                                            BindConsensusChart(nseSymbol).OrderByDescending(x => x.TotalCnt).FirstOrDefault(x => x.TotalCnt > 0).TrendInSightName :
                                            string.Empty;

                string bseMaxConsensusName = BindConsensusChart(bseSymbol).Count > 0 ?
                                            BindConsensusChart(bseSymbol).OrderByDescending(x => x.TotalCnt).FirstOrDefault(x => x.TotalCnt > 0).TrendInSightName :
                                            string.Empty;*/

                StockModel stockModel = new StockModel
                {
                    StockDetail = nse ? BindNseStock(nseSymbol) : bse ? BindBseStock(bseSymbol) : new StockDetail(),
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    UserRecentlyViewedList = RecentlyViewedList(string.Empty),
                    RecentlyViewedUserList = RecentlyViewedUserList(string.Empty),
                    WatchListModel = nse ? WatchListBind(nseSymbol, CommonConstants.Nulll) : bse ? WatchListBind(bseSymbol, CommonConstants.Nulll) : new List<WatchList>(),
                    ShareIdeaBlogModelList = GetBlogItems(1, BlockSize, setSymbol, "allIdea", streamPrefrence),//ShareIdeaBlogList(setSymbol)
                    ShareIdeaTextLimit = (int)db.Settings.FirstOrDefault().ShareIdeaText,
                    //MaxConsensusName = nse ? nseMaxConsensusName : bseMaxConsensusName,
                    ConsensusChartModel = nse ? BindConsensusChart(nseSymbol) : bse ? BindConsensusChart(bseSymbol) : new List<ConsensusChartModel>(),
                    //WatchListCount = nse ? WatchListCount(nseSymbol) : bse ? WatchListCount(bseSymbol) : 0,
                    IsWatch = nse ? IsWatched(nseSymbol) : bse ? IsWatched(bseSymbol) : false,
                    StockExchange = nse ? CommonConstants.NSE : CommonConstants.BSE,
                    StockUpdate = TimeSpan.FromSeconds(Convert.ToInt64(db.Settings.Select(x => x.StockUpdate).FirstOrDefault())).TotalMilliseconds,
                    StreamPrefrence = streamPrefrence,
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                    LastFetchIdeaDate = DateTime.Now,
                    NewIdeaCount = newIdeaCount,
                    IsStockMarketOpenClose = IsStockMarketOpenClose,
                    ViewNSE_BSEModel = BindViewNSE_BSEModel(symbol)
                };
                ViewBag.userId = userId;
                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }

        public ActionResult StockTicker(string symbol, string type)
        {
            try
            {
                bool nse = false;
                bool bse = false;
                if (type == "NSE") { nse = true; } else { bse = true; }
                string nseSymbol = nse ? symbol : string.Empty;
                string bseSymbol = bse ? symbol : string.Empty;

                StockModel stockModel = new StockModel
                {
                    StockDetail = nse ? BindNseStock(nseSymbol) : bse ? BindBseStock(bseSymbol) : new StockDetail(),
                    ViewNSE_BSEModel = BindViewNSE_BSEModel(symbol),
                    StockExchange = type
                };
                //var stringView = RenderRazorViewToString("_StockDetail", stockModel);
                //return Json(new { IsSuccess = true, html = stringView });
                return PartialView("_StockTicker", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_StockTicker", new StockModel());
        }

        public ActionResult SetSymbolSession(string key, string value)
        {
            Session[key] = value;
            return Json(new { IsSuccess = true });
        }

        [HttpPost]
        public JsonResult StockDetail(string symbol)
        {
            try
            {
                bool nse = false;
                bool bse = false;

                var nseList = db.SymbolMasters.Where(x => x.NSESymbol.ToLower() == symbol.ToLower()).ToList();

                if (nseList.Count() > 0)
                { nse = true; }
                else { bse = true; }

                string nseSymbol = nse ? symbol : string.Empty;
                string bseSymbol = bse ? symbol : string.Empty;

                StockModel stockModel = new StockModel
                {
                    StockDetail = nse ? BindNseStock(nseSymbol) : bse ? BindBseStock(bseSymbol) : new StockDetail(),
                    IsWatch = nse ? IsWatched(nseSymbol) : bse ? IsWatched(bseSymbol) : false,
                    StockExchange = nse ? CommonConstants.NSE : CommonConstants.BSE,
                    IsStockMarketOpenClose = IsStockMarketOpenClose,
                    ViewNSE_BSEModel = BindViewNSE_BSEModel(symbol)
                };
                var stringView = RenderRazorViewToString("_StockDetail", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public StockDetail BindNseStock(string symbol)
        {
            //Response.Write("<script>alert('" + symbol + "');</script>");
            //var url = !string.IsNullOrEmpty(symbol) ? "http: //www.google.com/finance/info?infotype=infoquoteall&q=" + symbol : string.Empty;
            var url = !string.IsNullOrEmpty(symbol) ? "http://finance.google.com/finance/info?q=NSE:" + HttpUtility.UrlEncode(symbol) : string.Empty;
            if (url == string.Empty)
            {
                return new StockDetail();
            }
            try
            {
                var myRequest = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)myRequest.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    StockDetail stockDetail = new StockDetail();

                    WebClient client = new WebClient();
                    string json = client.DownloadString(url);
                    json = json.Replace("//", "");
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    var nseJsonModel = serializer.Deserialize<NseJsonModel[]>(json);
                    var stockName = db.SymbolMasters.FirstOrDefault(x => x.NSESymbol == symbol);
                    foreach (var jsonModel in nseJsonModel)
                    {
                        stockDetail.StockName = stockName != null ? TitleCase(stockName.CompanyName) : string.Empty;
                        stockDetail.StockRate = jsonModel.l;
                        stockDetail.StockRatePercentage = jsonModel.cp.Replace('+', ' ').Replace('-', ' ').Trim();
                        stockDetail.StockType = jsonModel.e;
                        stockDetail.StockLastUpdated = jsonModel.lt;
                        stockDetail.StockUpDown = jsonModel.c;
                        stockDetail.StockSymbol = jsonModel.t;
                        stockDetail.Exchange = CommonConstants.NSE;
                        IsStockMarketOpenClose = MarketOpenClose(jsonModel.lt_dts);
                    }
                    return stockDetail;
                }
                else
                {
                    return new StockDetail();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new StockDetail();
        }

        public StockDetail BindBseStock(string symbol)
        {
            string sensexType = "BOM";
            if (symbol == CommonConstants.Sensex)
            {
                sensexType = "INDEXBOM";
            }
            var url = !string.IsNullOrEmpty(symbol) ? "http://finance.google.com/finance/info?q=" + sensexType + ":" + HttpUtility.UrlEncode(symbol) : string.Empty;
            if (url == string.Empty)
            {
                return new StockDetail();
            }
            try
            {
                var myRequest = (HttpWebRequest)WebRequest.Create(url.Trim());
                var response = (HttpWebResponse)myRequest.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {

                    StockDetail stockDetail = new StockDetail();
                    WebClient client = new WebClient();
                    string json = client.DownloadString(url);
                    json = json.Replace("//", "");
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    var bseJsonModel = serializer.Deserialize<BseJsonModel[]>(json);
                    var stockName = db.SymbolMasters.FirstOrDefault(x => x.BSESymbol == symbol);
                    foreach (var jsonModel in bseJsonModel)
                    {
                        stockDetail.StockName = stockName != null ? TitleCase(stockName.CompanyName) : string.Empty;
                        stockDetail.StockRate = jsonModel.l;
                        stockDetail.StockRatePercentage = jsonModel.cp.Replace('+', ' ').Replace('-', ' ').Trim();
                        stockDetail.StockType = jsonModel.e;
                        stockDetail.StockLastUpdated = jsonModel.lt;
                        stockDetail.StockUpDown = jsonModel.c;
                        stockDetail.StockSymbol = jsonModel.t;
                        stockDetail.Exchange = CommonConstants.BSE;
                        IsStockMarketOpenClose = MarketOpenClose(jsonModel.lt_dts);
                    }
                    return stockDetail;
                }
                else
                {
                    return new StockDetail();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new StockDetail();
        }

        public ActionResult ViewNSE_BSE(string symbol, string type)
        {
            try
            {
                bool nse = false;
                bool bse = false;
                if (type == "NSE") { nse = true; } else { bse = true; }
                string nseSymbol = nse ? symbol : string.Empty;
                string bseSymbol = bse ? symbol : string.Empty;

                StockModel stockModel = new StockModel
                {
                    StockDetail = nse ? BindNseStock(nseSymbol) : bse ? BindBseStock(bseSymbol) : new StockDetail(),
                };
                //var stringView = RenderRazorViewToString("_StockDetail", stockModel);
                //return Json(new { IsSuccess = true, html = stringView });
                return PartialView("_StockTicker", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_StockTicker", new StockModel());
        }

        public ViewNSE_BSEModel BindViewNSE_BSEModel(string symbol)
        {
            try
            {
                var symbolMaster = db.SymbolMasters.Where(x => x.BSESymbol == symbol || x.NSESymbol == symbol).ToList();
                var checkNseBse = symbolMaster.Where(x => !string.IsNullOrEmpty(x.BSESymbol) && !string.IsNullOrEmpty(x.NSESymbol)).ToList();
                if (checkNseBse.Count() > 0)
                {
                    ViewNSEModel viewNSEModel = new ViewNSEModel
                    {
                        Symbol = symbolMaster.FirstOrDefault().NSESymbol,
                        Type = CommonConstants.NSE
                    };
                    ViewBSEModel viewBSEModel = new ViewBSEModel
                    {
                        Symbol = symbolMaster.FirstOrDefault().BSESymbol,
                        Type = CommonConstants.BSE
                    };
                    ViewNSE_BSEModel viewNSE_BSEModel = new ViewNSE_BSEModel
                    {
                        ViewBSEModel = viewBSEModel,
                        ViewNSEModel = viewNSEModel
                    };
                    return viewNSE_BSEModel;
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new ViewNSE_BSEModel();
        }

        #endregion

        #region "Consensus Chart"

        public static List<ConsensusChartModel> BindConsensusChart(string symbol)
        {
            try
            {
                List<ConsensusChartModel> consensusChartModelList = new List<ConsensusChartModel>();

                using (StockCharchaEntities context = new StockCharchaEntities())
                {
                    string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                    builder.ConnectTimeout = 2500;
                    SqlConnection con = new SqlConnection(builder.ConnectionString);
                    System.Data.Common.DbDataReader sqlReader;
                    con.Open();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "ConsensusChart";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@Symbol", SqlDbType.NVarChar).Value = symbol;
                        sqlReader = (System.Data.Common.DbDataReader)cmd.ExecuteReader();

                        while (sqlReader.Read())
                        {
                            ConsensusChartModel consensusChartModel = new ConsensusChartModel();
                            consensusChartModel.TotalCnt = sqlReader.GetInt32(0);
                            consensusChartModel.Percentage = sqlReader.GetInt32(1);
                            consensusChartModel.TrendInSight = sqlReader.GetInt32(2);
                            if (TrendInsights.StrongBuy == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.StrongBuy);
                            }
                            else if (TrendInsights.Buy == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.Buy);
                            }
                            else if (TrendInsights.Hold == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.Hold);
                            }
                            else if (TrendInsights.Sell == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.Sell);
                            }
                            else if (TrendInsights.StrongSell == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.StrongSell);
                            }
                            else
                            {
                                consensusChartModel.TrendInSightName = string.Empty;
                            }
                            consensusChartModelList.Add(consensusChartModel);
                        }
                    }
                    con.Close();
                }
                ConsensusChartModel cm = new ConsensusChartModel();
                ArrayList alist = new ArrayList();
                foreach (var item in consensusChartModelList)
                {
                    alist.Add(item.TrendInSightName);
                }
                if (consensusChartModelList.Count > 0)
                {
                    consensusChartModelList.AddRange(EnumUtil.GetStringListDescriptionOfEnumType<TrendInsights>().Where(x => !alist.Contains(x)).Select(y => new ConsensusChartModel
                    {
                        TotalCnt = 0,
                        TrendInSightName = y,
                        TrendInSight = Convert.ToInt32(EnumUtil.GetValueFromDescription<TrendInsights>(y))
                    }));
                }
                return consensusChartModelList.OrderBy(x => x.TrendInSight).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ConsensusChartModel>(); ;
        }

        public static List<ConsensusChartModel> GetConsensusChartMaxRating(string symbol)
        {
            try
            {
                List<ConsensusChartModel> consensusChartModelList = new List<ConsensusChartModel>();

                using (StockCharchaEntities context = new StockCharchaEntities())
                {
                    string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                    builder.ConnectTimeout = 2500;
                    SqlConnection con = new SqlConnection(builder.ConnectionString);
                    System.Data.Common.DbDataReader sqlReader;
                    con.Open();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "ConsensusChart";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@Symbol", SqlDbType.NVarChar).Value = symbol;
                        sqlReader = (System.Data.Common.DbDataReader)cmd.ExecuteReader();

                        while (sqlReader.Read())
                        {
                            ConsensusChartModel consensusChartModel = new ConsensusChartModel();
                            consensusChartModel.TotalCnt = sqlReader.GetInt32(0);
                            consensusChartModel.Percentage = sqlReader.GetInt32(1);
                            consensusChartModel.TrendInSight = sqlReader.GetInt32(2);
                            if (TrendInsights.StrongBuy == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.StrongBuy);
                            }
                            else if (TrendInsights.Buy == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.Buy);
                            }
                            else if (TrendInsights.Hold == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.Hold);
                            }
                            else if (TrendInsights.Sell == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.Sell);
                            }
                            else if (TrendInsights.StrongSell == (TrendInsights)sqlReader.GetInt32(2))
                            {
                                consensusChartModel.TrendInSightName = EnumUtil.GetEnumDescription(TrendInsights.StrongSell);
                            }
                            else
                            {
                                consensusChartModel.TrendInSightName = string.Empty;
                            }
                            consensusChartModelList.Add(consensusChartModel);
                        }
                    }
                    con.Close();
                }
                return consensusChartModelList.OrderByDescending(x => x.Percentage).Take(1).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ConsensusChartModel>(); ;
        }

        public JsonResult ConsensusChart(string symbol)
        {
            try
            {
                var list = BindConsensusChart(symbol).ToArray();
                return Json(new { IsSuccess = true, ex = "", html = list });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, ex = "", html = "Something went wrong." });
        }

        public ActionResult RefershStockRatingChart(string symbol) //, string type
        {
            try
            {
                if (!string.IsNullOrEmpty(symbol))
                {

                    StockModel stockModel = new StockModel
                    {
                        ConsensusChartModel = BindConsensusChart(symbol),
                    };
                    return PartialView("_StockRatingChart", stockModel);
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_StockRatingChart", new StockModel());
        }

        #endregion

        #region "Stock Graph"
        [HttpPost]
        public JsonResult StockGraph(int interval, string period, string symbol, string exchange)
        {
            try
            {
                CurrentRequest();
                string setExchange = string.Empty;
                if (exchange == CommonConstants.NSE)
                {
                    setExchange = CommonConstants.NSE;
                }
                else if (exchange == CommonConstants.BSE)
                {
                    setExchange = CommonConstants.BSE;
                }

                var list = BindGraph(interval, period, symbol, setExchange);
                return Json(new { IsSuccess = true, ex = "", html = list });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, ex = "", html = "Something went wrong." });
        }

        public static Array BindGraph(int interval, string period, string symbol, string exchange)
        {
            try
            {
                DataTable dtstock = new DataTable();

                dtstock.Columns.Add("Hours", typeof(string));
                dtstock.Columns.Add("Minutes", typeof(string));
                dtstock.Columns.Add("Year", typeof(string));
                dtstock.Columns.Add("Month", typeof(string));
                dtstock.Columns.Add("Day", typeof(string));
                dtstock.Columns.Add("Open", typeof(string));
                dtstock.Columns.Add("Close", typeof(string));
                dtstock.Columns.Add("High", typeof(string));
                dtstock.Columns.Add("Low", typeof(string));
                dtstock.Columns.Add("Volume", typeof(string));


                /*DataTable dtsession = new DataTable();
                dtsession.Columns.Add("datefromtimestemp", typeof(string));
                dtsession.Columns.Add("timefromtimestemp", typeof(string));
                dtsession.Columns.Add("Close", typeof(string));*/

                string url = string.Empty;
                if (exchange == CommonConstants.NSE)
                {
                    url = "http://www.google.com/finance/getprices?q=" + HttpUtility.UrlEncode(symbol) + "&x=NSE&i=" + interval + "&p=" + period + "&f=d,c,h,l,o,v";
                }
                else if (exchange == CommonConstants.BSE)
                {
                    string sensexType = "BOM";
                    if (symbol == CommonConstants.Sensex)
                    {
                        sensexType = "INDEXBOM";
                    }
                    url = "http://www.google.com/finance/getprices?q=" + HttpUtility.UrlEncode(symbol) + "&x=" + sensexType + "&i=" + interval + "&p=" + period + "&f=d,c,h,l,o,v";
                }

                var myRequest = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)myRequest.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        int lineno = 0, Offset = 0;
                        string originaltimestemp = "";
                        while (reader.Peek() >= 0)
                        {
                            string line = reader.ReadLine();
                            if (lineno >= 7)
                            {
                                int countOccurrences = CountStringOccurrences(line, ","); //line.Split(',').Length - 1;
                                if (countOccurrences == 5)
                                {
                                    DataRow dr = dtstock.NewRow();
                                    string timestemp = line.Split(',')[0].ToString();

                                    if (timestemp.ToLower().Contains("a"))
                                    {
                                        originaltimestemp = timestemp.Remove(0, 1);
                                        Offset = 0;
                                    }
                                    else
                                    {
                                        Offset = Convert.ToInt32(timestemp);
                                    }
                                    string date123 = Convert.ToString((Convert.ToInt64(originaltimestemp) + (interval * Offset)));
                                    DateTime dtfromtimestemp = UnixTimeStampToDateTime(Convert.ToDouble(date123));
                                    string[] stockdatetime = Convert.ToString(dtfromtimestemp).Split(' ');



                                    // dd-mm-yyyy hh:MM:sss
                                    string datefromtimestemp = stockdatetime[0].ToString().Trim().Replace('/', '-');
                                    string timefromtimestemp = stockdatetime[1].ToString().Trim();
                                    if (stockdatetime.Length == 3)
                                    {
                                        timefromtimestemp += " " + stockdatetime[2].ToString().Trim();
                                    }

                                    //convert to 24 hours coz chart requires 24 hrs formate
                                    timefromtimestemp = To24HrTime(timefromtimestemp).ToString();

                                    /*DataRow drSession = dtsession.NewRow();
                                    drSession["datefromtimestemp"] = datefromtimestemp;
                                    drSession["timefromtimestemp"] = To24HrTime(timefromtimestemp);
                                    drSession["Close"] = line.Split(',')[1].ToString();
                                    dtsession.Rows.Add(drSession);*/
                                    //string fulldatetime = String.Format("{0:yyyy,MM,dd,HH.mm}", dtfromtimestemp);
                                    //string fulldatetime = datefromtimestemp.Split('-')[2] + "," + (Convert.ToInt16(datefromtimestemp.Split('-')[1]) - 1) + "," + datefromtimestemp.Split('-')[0] + "," + timefromtimestemp.Split(':')[0] + "," + timefromtimestemp.Split(':')[1];

                                    dr["Hours"] = timefromtimestemp.Split(':')[0];
                                    dr["Minutes"] = timefromtimestemp.Split(':')[1];
                                    dr["Year"] = datefromtimestemp.Split('-')[2];


                                    if (currentRequest == false)// for live
                                    {
                                        dr["Month"] = (Convert.ToInt16(datefromtimestemp.Split('-')[0]) - 1);
                                        dr["Day"] = datefromtimestemp.Split('-')[1];
                                    }
                                    else
                                    {
                                        dr["Month"] = (Convert.ToInt16(datefromtimestemp.Split('-')[0]) - 1);
                                        dr["Day"] = datefromtimestemp.Split('-')[1];
                                    }
                                    /*
                                     
                                     if (currentRequest == false)// for live
                                    {
                                        dr["Month"] = (Convert.ToInt16(datefromtimestemp.Split('-')[0]) - 1);
                                        dr["Day"] = datefromtimestemp.Split('-')[1];
                                    }
                                    else
                                    {
                                        dr["Month"] = (Convert.ToInt16(datefromtimestemp.Split('-')[1]) - 1);
                                        dr["Day"] = datefromtimestemp.Split('-')[0];
                                    }
                                     */

                                    dr["Open"] = line.Split(',')[4].ToString();
                                    dr["Close"] = line.Split(',')[1].ToString();
                                    dr["High"] = line.Split(',')[2].ToString();
                                    dr["Low"] = line.Split(',')[3].ToString();
                                    dr["Volume"] = line.Split(',')[5].ToString();
                                    dtstock.Rows.Add(dr);
                                }
                            }
                            lineno++;
                        }
                    }
                }

                ArrayList list = new ArrayList();
                for (int i = 0; i < dtstock.Rows.Count; i++)
                {
                    list.Add(new[] { dtstock.Rows[i]["Year"], dtstock.Rows[i]["Month"], dtstock.Rows[i]["Day"], dtstock.Rows[i]["Hours"], dtstock.Rows[i]["Minutes"], dtstock.Rows[i]["Close"] });
                }
                //WebUtil.SetSessionValue<DataTable>("dtSession", dtsession);
                return list.ToArray();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            var list1 = new ArrayList();
            return list1.ToArray();
        }

        #endregion

        #region "DateTime Format Methods"
        /// <summary>
        /// <-- a##### denotes a unix time stamp. like a1449546600
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            try
            {
                // Unix timestamp is seconds past epoch
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
                dtDateTime = ConvertUTCToIST(dtDateTime);
                return dtDateTime;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new DateTime();
        }
        public static DateTime ConvertUTCToIST(DateTime datetime)
        {
            DateTime serverTime = datetime;//DateTime.Now; // gives you current Time in server timeZone
            DateTime utcTime = serverTime.ToUniversalTime(); // convert it to Utc using timezone setting of server computer

            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime ISTDT = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi); // convert from utc to local
            return ISTDT;
        }
        public static TimeSpan To24HrTime(string time)
        {
            char[] delimiters = new char[] { ':', ' ' };
            string[] spltTime = time.Split(delimiters);

            int hour = Convert.ToInt32(spltTime[0]);
            int minute = Convert.ToInt32(spltTime[1]);
            int seconds = Convert.ToInt32(spltTime[2]);
            if (spltTime.Length == 4)
            {
                string amORpm = spltTime[3];

                if (amORpm.ToUpper() == "PM")
                {
                    hour = (hour % 12) + 12;
                }
            }
            return new TimeSpan(hour, minute, seconds);
        }

        public void CurrentRequest()
        {
            currentRequest = Request.Url.AbsoluteUri.ToLower().Contains("localhost");
        }

        /// <summary>
        /// like 1449546497 in json response
        /// </summary>
        /// <param name="javaTimeStamp"></param>
        /// <returns></returns>
        public static DateTime JavaTimeStampToDateTime(double javaTimeStamp)
        {
            try
            {
                // Java timestamp is millisecods past epoch
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Math.Round(javaTimeStamp / 1000)).ToLocalTime();
                return dtDateTime;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new DateTime();
        }
        #endregion

        #region "User Profile"

        public ActionResult UserProfile()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                    RegistrationModel model = new RegistrationModel
                    {
                        Id = user.Id,
                        UserId = new Guid(user.UserId.ToString()),
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = Membership.GetUser(new Guid(Convert.ToString(userId))).Email,
                        UserName = user.aspnet_Users.UserName,
                        Mobile = user.Mobile,
                        Description = user.Description,
                        Website = user.Website,
                        //Photo = user.Photo,
                        userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                        UserTag = user.UserTag
                    };
                    StockModel stockModel = new StockModel
                    {
                        StockMenuList = Menu(),
                        LoggedInUser = SetLoggedInUser(),
                        RegistrationModel = model
                    };
                    return View(stockModel);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public ActionResult UpdateUserProfile1(StockModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.RegistrationModel.Email))
                {
                    string userNamee = Membership.GetUserNameByEmail(model.RegistrationModel.Email);
                    if (!string.IsNullOrEmpty(userNamee))
                    {
                        return Json(new { IsSuccess = false, html = "Email already exist." });
                    }
                }
                else
                {
                    return RedirectToAction("UserProfile");
                }

                RegistrationMaster registrationModel = db.RegistrationMasters.Find(model.RegistrationModel.Id);
                if (registrationModel != null)
                {
                    registrationModel.FirstName = model.RegistrationModel.FirstName;
                    registrationModel.LastName = model.RegistrationModel.LastName;
                    registrationModel.Mobile = model.RegistrationModel.Mobile;
                    //registrationModel.Photo = model.RegistrationModel.Photo ?? registrationModel.Photo;
                    registrationModel.Description = model.RegistrationModel.Description;
                    registrationModel.UserTag = model.RegistrationModel.UserTag;
                    registrationModel.Website = model.RegistrationModel.Website;
                    db.SaveChanges();
                    SiteActivityErrorsUtil.SiteActivity("UpdateUserProfile", model.RegistrationModel.Id.ToString(), "User Update Profile");
                }
                string userName = Membership.GetUser(new Guid(Convert.ToString(model.RegistrationModel.UserId))).UserName;
                return RedirectToAction("ViewProfile", new { username = userName });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public JsonResult UpdateUserProfile(RegistrationModel model)
        {
            try
            {
                MembershipUser mu = Membership.GetUser(model.UserName);
                if (!string.IsNullOrEmpty(model.Email))
                {
                    if (mu.Email.ToLower() != model.Email.ToLower())
                    {
                        string userNamee = Membership.GetUserNameByEmail(model.Email);
                        if (!string.IsNullOrEmpty(userNamee))
                        {
                            return Json(new { IsSuccess = false, html = "Email already exist." });
                        }
                    }
                }

                RegistrationMaster registrationModel = db.RegistrationMasters.Find(model.Id);
                if (registrationModel != null)
                {
                    registrationModel.FirstName = model.FirstName;
                    registrationModel.LastName = model.LastName;
                    registrationModel.Mobile = model.Mobile;
                    //registrationModel.Photo = model.RegistrationModel.Photo ?? registrationModel.Photo;
                    registrationModel.Description = model.Description;
                    registrationModel.UserTag = model.UserTag;
                    registrationModel.Website = model.Website;

                    //Update email
                    if (mu.Email.ToLower() != model.Email.ToLower())
                    {
                        mu.Email = model.Email;
                        Membership.UpdateUser(mu);
                    }

                    db.SaveChanges();
                    SiteActivityErrorsUtil.SiteActivity("UpdateUserProfile", model.Id.ToString(), "User Update Profile");
                }
                string userName = Membership.GetUser(new Guid(Convert.ToString(model.UserId))).UserName;
                //return RedirectToAction("ViewProfile", new { username = userName });
                return Json(new { IsSuccess = true, username = userName });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Somthing went wrong." });
        }

        [HttpPost]
        public JsonResult UploadUserImage()
        {
            string uId = string.Empty;
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    string userId = Convert.ToString(Request["userId"]);
                    uId = userId;
                    HttpPostedFileBase userImage = fileContent;
                    Bitmap bmp = new Bitmap(userImage.InputStream);
                    Bitmap bmp32 = new Bitmap(userImage.InputStream);
                    Bitmap bmp50 = new Bitmap(userImage.InputStream);
                    Bitmap bmp100 = new Bitmap(userImage.InputStream);
                    if (bmp.Height >= 100 && bmp.Width >= 100)
                    {
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            //32x32
                            string extension = Path.GetExtension(userImage.FileName);
                            Bitmap bitmapImage32 = ResizeImage(bmp32, CommonConstants.userImage32, CommonConstants.userImage32, ResizeOptions.ExactWidthAndHeight);
                            Bitmap bitmapImage50 = ResizeImage(bmp50, CommonConstants.userImage50, CommonConstants.userImage50, ResizeOptions.ExactWidthAndHeight);
                            Bitmap bitmapImage100 = ResizeImage(bmp100, CommonConstants.userImage100, CommonConstants.userImage100, ResizeOptions.ExactWidthAndHeight);
                            string imagesFilePath = Path.Combine(HttpContext.Server.MapPath("/Media/UserImages/" + userId + ""));
                            if (!Directory.Exists(imagesFilePath))
                            {
                                Directory.CreateDirectory(imagesFilePath);
                            }

                            bitmapImage32.Save(imagesFilePath + "/32" + extension);
                            bitmapImage50.Save(imagesFilePath + "/50" + extension);
                            bitmapImage100.Save(imagesFilePath + "/100" + extension);

                            SiteActivityErrorsUtil.SiteActivity("UpdateUserProfile", userId.ToString(), "User Update Profile");
                        }
                    }
                    else
                    {
                        return Json(new { IsSuccess = false, ex = "", html = "Upload atleast 100x100 Image." });
                    }
                }
                return Json(new { IsSuccess = true, ex = "", html = "File Uploaded.", userImage = GetUserImage(new Guid(uId), "100") });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, ex = "", html = "Something went wrong." });
        }

        public byte[] UploadImage(HttpPostedFileBase file)
        {
            string filename = string.Empty;
            byte[] bytes;
            int BytestoRead;
            int numBytesRead;
            filename = Path.GetFileName(file.FileName);
            bytes = new byte[file.ContentLength];
            BytestoRead = (int)file.ContentLength;
            numBytesRead = 0;
            while (BytestoRead > 0)
            {
                int n = file.InputStream.Read(bytes, numBytesRead, BytestoRead);
                if (n == 0) break;
                numBytesRead += n;
                BytestoRead -= n;
            }
            return bytes;
        }

        public void ShowImage(int id = 0)
        {
            try
            {
                byte[] image = db.RegistrationMasters.Where(x => x.Id == id).FirstOrDefault().Photo;
                Response.Buffer = true;
                Response.Clear();
                if (image != null)
                {
                    Response.BinaryWrite(image);
                }
                else
                {
                    Image defaultImage = Image.FromFile(Server.MapPath(@"~\Content\web\images\blank-photo2.png"));
                    byte[] imageByte = ImageToByteArraybyMemoryStream(defaultImage);
                    Response.BinaryWrite(imageByte);
                }
                Response.End();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
        }

        public JsonResult RetrieveImage(int id = 0)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                String base64string = Convert.ToBase64String(cover);
                return Json(new { Img = base64string }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Img = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public byte[] GetImageFromDataBase(int id = 0)
        {
            byte[] image = db.RegistrationMasters.Where(x => x.Id == id).FirstOrDefault().Photo;
            return image;
        }

        public ActionResult ViewProfile(string username)
        {
            try
            {
                if (GetCurrentLoggedinUserId() == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }
                Guid userId = (Guid)Membership.GetUser(username).ProviderUserKey;
                Guid setUserId = Guid.Empty;

                if (userId != Guid.Empty)
                {
                    setUserId = userId;
                }

                int streamPrefrence = (int)db.Settings.FirstOrDefault().StreamPrefrence;
                if (setUserId != Guid.Empty)
                {
                    var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == setUserId);
                    RegistrationModel model = new RegistrationModel
                    {
                        Id = user.Id,
                        UserId = setUserId,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        UserName = user.aspnet_Users.UserName,
                        Mobile = user.Mobile,
                        Description = user.Description,
                        Website = user.Website,
                        //Photo = user.Photo,
                        userImage = GetUserImage(new Guid(setUserId.ToString()), "100"),
                        UserTag = user.UserTag,
                        CreatedOn = String.Format("{0:Y}", user.CreatedOn)
                    };
                    int BlockSize = CommonConstants.BlockSize;
                    StockModel stockModel = new StockModel
                    {
                        StockMenuList = Menu(),
                        LoggedInUser = SetLoggedInUser(),
                        RegistrationModel = model,
                        UserRecentlyViewedList = RecentlyViewedList(model.UserName),
                        RecentlyViewedUserList = RecentlyViewedUserList(model.UserName),
                        Followed = UserFollowed(model.UserName),
                        Followers = UserFollowers(model.UserName),
                        ShareIdeaBlogModelList = GetBlogItemsProfile(1, BlockSize, model.UserName, "allIdea", streamPrefrence),
                        UserRatingModel = UserRate(model.UserName),
                        WatchListModel = ProfileWatchListBind(model.UserId),
                        ShareIdeaTextLimit = (int)db.Settings.FirstOrDefault().ShareIdeaText,
                        NseStockDetail = NseStockDetail(),
                        BseStockDetail = BseStockDetail(),
                        StreamPrefrence = streamPrefrence,
                        StockUpdate = TimeSpan.FromSeconds(Convert.ToInt64(db.Settings.Select(x => x.StockUpdate).FirstOrDefault())).TotalMilliseconds,
                        LastFetchIdeaDate = DateTime.Now,
                        NewIdeaCount = newIdeaCount,
                        IsFollow = IsFollow(model.UserName)
                    };

                    //Recently Viewed User Entry
                    RecentlyViewedUser(GetCurrentLoggedinUserId(), userId, CommonConstants.USER);
                    ViewBag.userId = model.UserId;
                    return View(stockModel);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return RedirectToAction("Index");
        }

        public bool IsFollow(string username)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                Guid followByUserId = (Guid)Membership.GetUser(username).ProviderUserKey;
                UserFollow followExist = db.UserFollows.FirstOrDefault(x => x.FollowBy == userId && x.FollowTo == followByUserId);
                if (followExist != null)
                {
                    if (followExist.IsDeleted == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return false;
        }
        public JsonResult UserFollow(string followBy)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please login first." });
                }

                Guid followByUserId = (Guid)Membership.GetUser(followBy).ProviderUserKey;
                UserFollow followExist = db.UserFollows.FirstOrDefault(x => x.FollowBy == userId && x.FollowTo == followByUserId);
                string msg = string.Empty;
                if (followExist != null)
                {
                    if (followExist.IsDeleted == true)
                    {
                        followExist.IsDeleted = false;
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("User Follow", userId.ToString(), "User Follow");
                        msg = "Followed successfully.";
                    }
                    else
                    {
                        followExist.IsDeleted = true;
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("User Follow", userId.ToString(), "User UnFollow");
                        msg = "UnFollowed successfully.";
                    }
                    return Json(new { IsSuccess = true, html = msg });
                }
                else
                {
                    UserFollow userFollow = new UserFollow
                    {
                        FollowBy = userId,
                        FollowTo = followByUserId,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.UserFollows.Add(userFollow);
                    db.SaveChanges();
                    SiteActivityErrorsUtil.SiteActivity("UserFollow", userId.ToString(), "User Follow");
                    return Json(new { IsSuccess = true, html = "Followed successfully." });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public Followed UserFollowed(string userName)
        {
            try
            {
                Guid userId = (Guid)Membership.GetUser(userName).ProviderUserKey;
                int userFollowed = db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).ToList().Count;
                Followed followed = new Followed();
                followed.UserFollowed = userFollowed;
                return followed;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new Followed();
        }

        public Followers UserFollowers(string userName)
        {
            try
            {
                Guid userId = (Guid)Membership.GetUser(userName).ProviderUserKey;
                int userFollowers = db.UserFollows.Where(x => x.FollowTo == userId && x.IsDeleted == false).ToList().Count;
                Followers followers = new Followers();
                followers.UserFollowers = userFollowers;
                return followers;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new Followers();
        }

        public ActionResult UserFollowersList(string username)
        {
            try
            {
                Guid userId = (Guid)Membership.GetUser(username).ProviderUserKey;

                string followBy = string.Join(",", db.UserFollows.Where(x => x.FollowTo == userId && x.IsDeleted == false).Select(x => x.FollowBy).ToList());
                var registrationList = db.RegistrationMasters.Where(x => followBy.Contains(x.UserId.ToString())).ToList();
                var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                RegistrationModel model = new RegistrationModel
                {
                    Id = user.Id,
                    UserId = new Guid(user.UserId.ToString()),
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.aspnet_Users.UserName,
                    Mobile = user.Mobile,
                    Description = user.Description,
                    Website = user.Website,
                    //Photo = user.Photo,
                    userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                    UserTag = user.UserTag,
                    CreatedOn = String.Format("{0:Y}", user.CreatedOn),
                    Title = CommonConstants.Followers
                };
                List<ProfileList> followerList = new List<ProfileList>();
                followerList.AddRange(registrationList.Select(x => new ProfileList
                {
                    Id = x.Id,
                    UserName = x.aspnet_Users.UserName,
                    Mobile = x.Mobile,
                    Description = x.Description,
                    Website = x.Website,
                    UserTag = x.UserTag,
                    //photo = x.Photo,
                    userImage = GetUserImage(new Guid(x.UserId.ToString()), "50"),
                    CreatedOn = String.Format("{0:Y}", x.CreatedOn)
                }));

                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    ProfileListModel = followerList,
                    RegistrationModel = model
                };

                return View("UserProfileList", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View("UserProfileList", new StockModel());
        }

        public ActionResult UserFollowedList(string username)
        {
            try
            {
                Guid userId = (Guid)Membership.GetUser(username).ProviderUserKey;

                string followTo = string.Join(",", db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).Select(x => x.FollowTo).ToList());
                var registrationList = db.RegistrationMasters.Where(x => followTo.Contains(x.UserId.ToString())).ToList();

                var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                RegistrationModel model = new RegistrationModel
                {
                    Id = user.Id,
                    UserId = new Guid(user.UserId.ToString()),
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.aspnet_Users.UserName,
                    Mobile = user.Mobile,
                    Description = user.Description,
                    Website = user.Website,
                    //Photo = user.Photo,
                    userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                    UserTag = user.UserTag,
                    CreatedOn = String.Format("{0:Y}", user.CreatedOn),
                    Title = CommonConstants.Following
                };

                List<ProfileList> followedList = new List<ProfileList>();
                followedList.AddRange(registrationList.Select(x => new ProfileList
                {
                    Id = x.Id,
                    UserName = x.aspnet_Users.UserName,
                    Mobile = x.Mobile,
                    Description = x.Description,
                    Website = x.Website,
                    UserTag = x.UserTag,
                    photo = x.Photo,
                    CreatedOn = String.Format("{0:Y}", x.CreatedOn)

                }));
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    ProfileListModel = followedList,
                    RegistrationModel = model
                };
                return View("UserProfileList", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View("UserProfileList", new StockModel());
        }

        public JsonResult UserFollowedFilter()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();

                string followTo = string.Join(",", db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).Select(x => x.FollowTo).ToList());
                var registrationList = db.RegistrationMasters.Where(x => followTo.Contains(x.UserId.ToString())).ToList();

                var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                RegistrationModel model = new RegistrationModel
                {
                    Id = user.Id,
                    UserId = new Guid(user.UserId.ToString()),
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.aspnet_Users.UserName,
                    Mobile = user.Mobile,
                    Description = user.Description,
                    Website = user.Website,
                    //Photo = user.Photo,
                    userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                    UserTag = user.UserTag,
                    CreatedOn = String.Format("{0:Y}", user.CreatedOn),
                    Title = CommonConstants.Following
                };

                List<ProfileList> followedList = new List<ProfileList>();
                followedList.AddRange(registrationList.Select(x => new ProfileList
                {
                    Id = x.Id,
                    UserName = x.aspnet_Users.UserName,
                    Mobile = x.Mobile,
                    Description = x.Description,
                    Website = x.Website,
                    UserTag = x.UserTag,
                    photo = x.Photo,
                    CreatedOn = String.Format("{0:Y}", x.CreatedOn)

                }));
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    ProfileListModel = followedList,
                    RegistrationModel = model
                };
                var stringView = RenderRazorViewToString("_UserProfileListFilter", stockModel);
                return Json(new { IsSuccess = true, html = stringView, msg = "Unfollowed Successfully." });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            var stringView1 = RenderRazorViewToString("_UserProfileListFilter", new StockModel());
            return Json(new { IsSuccess = false, html = stringView1, msg = "Something went wrong." });
        }

        public ActionResult ProfileWatchList()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                RegistrationModel model = new RegistrationModel
                {
                    Id = user.Id,
                    UserId = new Guid(user.UserId.ToString()),
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.aspnet_Users.UserName,
                    Mobile = user.Mobile,
                    Description = user.Description,
                    Website = user.Website,
                    //Photo = user.Photo,
                    userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                    UserTag = user.UserTag,
                    CreatedOn = String.Format("{0:Y}", user.CreatedOn),
                    Title = CommonConstants.Following
                };
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    WatchListModel = ProfileWatchListBind(),
                    RegistrationModel = model
                };
                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View();
        }

        public JsonResult ProfileWatchListFilter(string symbol)
        {
            try
            {
                if (!string.IsNullOrEmpty(symbol))
                {
                    string msg = string.Empty;
                    Guid userId = GetCurrentLoggedinUserId();
                    var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                    UserWatchList userWatchlist = db.UserWatchLists.FirstOrDefault(x => x.UserId == userId && x.Symbol == symbol);
                    if (userWatchlist != null)
                    {
                        userWatchlist.IsDeleted = true;
                        userWatchlist.ModifiedOn = DateTime.Now;
                        userWatchlist.ModifiedBy = userId;
                        db.SaveChanges();
                        msg = "Unwatch done.";
                    }

                    RegistrationModel model = new RegistrationModel
                    {
                        Id = user.Id,
                        UserId = new Guid(user.UserId.ToString()),
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        UserName = user.aspnet_Users.UserName,
                        Mobile = user.Mobile,
                        Description = user.Description,
                        Website = user.Website,
                        //Photo = user.Photo,
                        userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                        UserTag = user.UserTag,
                        CreatedOn = String.Format("{0:Y}", user.CreatedOn),
                        Title = CommonConstants.Following
                    };
                    StockModel stockModel = new StockModel
                    {
                        StockMenuList = Menu(),
                        LoggedInUser = SetLoggedInUser(),
                        WatchListModel = ProfileWatchListBind(),
                        RegistrationModel = model
                    };
                    var stringView = RenderRazorViewToString("_ProfileWatchListFilter", stockModel);
                    return Json(new { IsSuccess = true, html = stringView, msg = msg });
                }

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            var stringView1 = RenderRazorViewToString("_ProfileWatchListFilter", new StockModel());
            return Json(new { IsSuccess = false, html = stringView1, msg = "Something went wrong." });
        }

        public JsonResult UserRating(string user, double rate)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please login first." });
                }
                Guid rateTo = !string.IsNullOrEmpty(user) ? (Guid)Membership.GetUser(user).ProviderUserKey : Guid.Empty;
                UserRating userRate = new UserRating
                {
                    UserId = rateTo,
                    Rate = Convert.ToDecimal(rate),
                    RateBy = userId,
                    CreatedOn = DateTime.Now
                };
                db.UserRatings.Add(userRate);
                db.SaveChanges();
                SiteActivityErrorsUtil.SiteActivity("UserRating", userId.ToString(), "User Rating");
                decimal? rateAvg = db.UserRatings.Where(x => x.UserId == rateTo).Average(x => x.Rate);
                return Json(new { IsSuccess = true, rate = String.Format("{0:0.0}", rateAvg) });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false });
        }

        public UserRatingModel UserRate(string userName)
        {
            try
            {
                Guid userId = (Guid)Membership.GetUser(userName).ProviderUserKey;
                decimal? rateAvg = db.UserRatings.Where(x => x.UserId == userId).Average(x => x.Rate);

                UserRatingModel userRatingModel = new UserRatingModel
                {
                    Rate = rateAvg
                };
                return userRatingModel;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new UserRatingModel();
        }

        public JsonResult UserMessage(string message, string messageTo)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please login first." });
                }
                Guid userMessageTo = !string.IsNullOrEmpty(messageTo) ? (Guid)Membership.GetUser(messageTo).ProviderUserKey : Guid.Empty;
                UserMessage userMessage = new UserMessage
                {
                    MessageTo = userMessageTo,
                    MessageFrom = userId,
                    Description = message,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false,
                    IsRead = false
                };
                db.UserMessages.Add(userMessage);
                db.SaveChanges();
                SiteActivityErrorsUtil.SiteActivity("UserMessage", userId.ToString(), "User Message");
                return Json(new { IsSuccess = true });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false });
        }

        [ValidateInput(false)]
        public JsonResult ShareIdeaProfile(ShareIdeaBlogModel model)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please Login First." });
                }
                string setImage = string.Empty;
                if (!string.IsNullOrEmpty(model.NewFileName))
                {
                    setImage = "<br/> <a href='" + model.NewFileName + "' rel='prettyPhoto'><img src='" + model.NewFileName + "' alt='' style='height: 150px;'/></a>";
                }
                string shareIdeaDescriptionString = ShareIdeaDescription(model.Description) + setImage;
                if (!string.IsNullOrEmpty(model.Description))
                {
                    Blog blog = new Blog
                    {
                        Description = shareIdeaDescriptionString,
                        EmailOnReply = model.EmailOnReply,
                        Everyone = model.Everyone,
                        OnlyFollowers = model.OnlyFollowers,
                        TrendInSights = model.TrendInSights != null ? Convert.ToInt32(model.TrendInSights) : -1,
                        CreatedOn = DateTime.Now,
                        CreatedBy = userId,
                        IsPublished = true,
                        Symbol = null,
                        NewFileName = model.NewFileName,
                        OriginalFileName = model.OriginalFileName

                    };
                    db.Blogs.Add(blog);
                    db.SaveChanges();
                    if (headerShareIdeahashTags.Count() > 0)
                    {
                        foreach (var hashTag in headerShareIdeahashTags)
                        {
                            if (!hashTag.Contains('@'))
                            {
                                string refTag = hashTag.Replace('#', ' ').Trim();
                                //if (refTag != (!string.IsNullOrEmpty(symbols[0]) ? symbols[0] : symbols[1]))
                                //{
                                Blog blog1 = new Blog
                                {
                                    Description = shareIdeaDescriptionString,
                                    EmailOnReply = model.EmailOnReply,
                                    Everyone = model.Everyone,
                                    OnlyFollowers = model.OnlyFollowers,
                                    TrendInSights = model.TrendInSights != null ? Convert.ToInt32(model.TrendInSights) : -1,
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = userId,
                                    IsPublished = true,
                                    Symbol = refTag,
                                    NewFileName = model.NewFileName,
                                    OriginalFileName = model.OriginalFileName
                                };
                                db.Blogs.Add(blog1);
                                db.SaveChanges();
                                // }
                            }
                        }
                    }
                    SiteActivityErrorsUtil.SiteActivity("ShareIdeaProfile", blog.Id.ToString(), "ShareIdea Profile");
                }

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = ShareIdeaBlogListProfile(string.Empty)
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> ShareIdeaBlogListProfile(string userName)
        {
            try
            {
                Guid loggedinUserId = GetCurrentLoggedinUserId();
                Guid userId = !string.IsNullOrEmpty(userName)
                    ? (Guid)Membership.GetUser(userName).ProviderUserKey
                    : loggedinUserId;

                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                var blogList = db.Blogs.Where(x => x.CreatedBy == userId && x.IsPublished == true).ToList();

                shareIdeaBlogListModel.AddRange(blogList.Select(x => new ShareIdeaBlogListModel
                {
                    BlogId = x.Id,
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                    TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                                 Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                                 Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                                 Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                                 Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                                 string.Empty,
                    CreatedBy = x.CreatedBy,
                    ReShareId = x.ReShareId > 0 ? true : false,
                    ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                    Like = IsLiked(x.Id),
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = (loggedinUserId == userId && userId == x.CreatedBy) ? true : false
                }));
                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public List<ShareIdeaBlogListModel> GetBlogItemsProfile(int BlockNumber, int BlockSize, string userName, string selectedClass, int option = 0)
        {
            try
            {
                int startIndex = (BlockNumber - 1) * BlockSize;
                List<ShareIdeaBlogListModel> ShareIdeaBlogLists = new List<ShareIdeaBlogListModel>();
                if (selectedClass == "topIdea")
                {
                    ShareIdeaBlogLists = GetTopIdeasListProfile(userName, option);
                }
                else if (selectedClass == "allIdea")
                {
                    ShareIdeaBlogLists = GetAllIdeasListProfile(userName);
                    ViewBag.ShareIdeaListCount = ShareIdeaBlogLists.Count();
                }
                else if (selectedClass == "chartIdea")
                {
                    ShareIdeaBlogLists = GetChartIdeasListProfile(userName);
                }
                else if (selectedClass == "linksIdea")
                {
                    ShareIdeaBlogLists = GetLinksIdeasListProfile(userName);
                }
                else
                {
                    ShareIdeaBlogLists = new List<ShareIdeaBlogListModel>();
                }
                return ShareIdeaBlogLists.Skip(startIndex).Take(BlockSize).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        [HttpPost]
        public JsonResult InfinateScrollProfile(int BlockNumber, string userName, string selectedClass)
        {
            try
            {
                int BlockSize = CommonConstants.BlockSize;
                List<ShareIdeaBlogListModel> ideaBlogs = GetBlogItemsProfile(BlockNumber, BlockSize, userName, selectedClass);

                if (ideaBlogs.Count > 0)
                {
                    JsonModel jsonModel = new JsonModel();
                    jsonModel.NoMoreData = ideaBlogs.Count < BlockSize;
                    StockModel stockModel = new StockModel
                    {
                        LoggedInUser = SetLoggedInUser(),
                        ShareIdeaBlogModelList = ideaBlogs//ShareIdeaBlogList(setSymbol)
                    };
                    jsonModel.HTMLString = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                    return Json(new { IsSuccess = true, html = jsonModel });
                }
                else
                {
                    return Json(new { IsSuccess = false });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false });
        }


        public List<ShareIdeaBlogListModel> GetTopIdeasListProfile(string userName, int option = 0)
        {
            Guid loggedinUserId = GetCurrentLoggedinUserId();
            Guid userId = !string.IsNullOrEmpty(userName)
                ? (Guid)Membership.GetUser(userName).ProviderUserKey
                : loggedinUserId;
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogList = !string.IsNullOrEmpty(userName) ? ShareIdeaBlogListProfile(userName) : new List<ShareIdeaBlogListModel>();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                if ((StreamPrefrence)option == StreamPrefrence.Comments)
                {
                    var blogTopListComments = (from b in shareIdeaBlogList
                                               join bc in db.BlogComments on b.BlogId equals bc.BlogId
                                               group new { b, bc } by new { b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogId }
                                                   into x
                                                   select new
                                                   {
                                                       BlogId = x.Key.BlogId,
                                                       Description = x.Key.Description,
                                                       TrendInSights = x.Key.TrendInSights,
                                                       ReShareId = x.Key.ReShareId,
                                                       CreatedOn = x.Key.CreatedOn,
                                                       CreatedBy = x.Key.CreatedBy,
                                                       Total = x.Count(),

                                                   }).OrderByDescending(a => a.Total).ToList();
                    shareIdeaBlogListModel.AddRange(blogTopListComments.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId ?? 0,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId ?? 0),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = (loggedinUserId == userId && userId == x.CreatedBy) ? true : false
                    }));
                }
                else if ((StreamPrefrence)option == StreamPrefrence.Reshare)
                {
                    var blogTopListReshare = (from b in shareIdeaBlogList
                                              join bc in db.Blogs on b.BlogId equals bc.ReShareId
                                              group new { b, bc } by new { b.BlogId, b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId }
                                                  into x
                                                  select new
                                                  {
                                                      BlogId = x.Key.BlogId,
                                                      Description = x.Key.Description,
                                                      TrendInSights = x.Key.TrendInSights,
                                                      ReShareId = x.Key.ReShareId,
                                                      CreatedOn = x.Key.CreatedOn,
                                                      CreatedBy = x.Key.CreatedBy,
                                                      Total = x.Count(),

                                                  }).OrderByDescending(a => a.Total).ToList();

                    shareIdeaBlogListModel.AddRange(blogTopListReshare.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = (loggedinUserId == userId && userId == x.CreatedBy) ? true : false
                    }));
                }
                else if ((StreamPrefrence)option == StreamPrefrence.Likes)
                {
                    var blogTopListLike = (from b in shareIdeaBlogList
                                           join bc in db.BlogCommentsLikes on b.BlogId equals bc.BlogCommentsId
                                           where bc.Isdeleted == false
                                           group new { b, bc } by new { b.BlogId, b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogCommentsId }
                                               into x
                                               select new
                                               {
                                                   BlogId = x.Key.BlogId,
                                                   Description = x.Key.Description,
                                                   TrendInSights = x.Key.TrendInSights,
                                                   ReShareId = x.Key.ReShareId,
                                                   CreatedOn = x.Key.CreatedOn,
                                                   CreatedBy = x.Key.CreatedBy,
                                                   Total = x.Count(),

                                               }).OrderByDescending(a => a.Total).ToList();
                    shareIdeaBlogListModel.AddRange(blogTopListLike.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = (loggedinUserId == userId && userId == x.CreatedBy) ? true : false
                    }));
                }
                else
                {

                }
                return shareIdeaBlogListModel.ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetTopIdeasProfile(string userName, int option)
        {
            try
            {
                int BlockSize = CommonConstants.BlockSize;
                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = GetBlogItemsProfile(1, BlockSize, userName, "topIdea", option),
                    //RegistrationModel = GetRegistrationModelByUserId((Guid)Membership.GetUser(userName).ProviderUserKey)
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }


        public List<ShareIdeaBlogListModel> GetAllIdeasListProfile(string userName)
        {
            try
            {
                return !string.IsNullOrEmpty(userName) ? ShareIdeaBlogListProfile(userName) : new List<ShareIdeaBlogListModel>();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetAllIdeasProfile(string userName)
        {
            try
            {
                int BlockSize = CommonConstants.BlockSize;
                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = GetBlogItemsProfile(1, BlockSize, userName, "allIdea"),
                    //RegistrationModel = GetRegistrationModelByUserId((Guid)Membership.GetUser(userName).ProviderUserKey)
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetChartIdeasListProfile(string userName)
        {
            try
            {
                Guid loggedinUserId = GetCurrentLoggedinUserId();
                Guid userId = !string.IsNullOrEmpty(userName)
                    ? (Guid)Membership.GetUser(userName).ProviderUserKey
                    : loggedinUserId;

                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                var blogList = db.Blogs.Where(x => x.CreatedBy == userId && x.IsPublished == true && x.Description.ToString().Contains("<img")).ToList();

                shareIdeaBlogListModel.AddRange(blogList.Select(x => new ShareIdeaBlogListModel
                {
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                    TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                                 Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                                 Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                                 Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                                 Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                                 string.Empty,
                    CreatedBy = x.CreatedBy,
                    ReShareId = x.ReShareId > 0 ? true : false,
                    ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                    Like = IsLiked(x.Id),
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = (loggedinUserId == userId && userId == x.CreatedBy) ? true : false
                }));
                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetChartIdeasProfile(string userName)
        {
            try
            {
                int BlockSize = CommonConstants.BlockSize;
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = GetBlogItemsProfile(1, BlockSize, userName, "chartIdea");

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel,
                    //RegistrationModel = GetRegistrationModelByUserId((Guid)Membership.GetUser(userName).ProviderUserKey)
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetLinksIdeasListProfile(string userName)
        {
            try
            {
                Guid loggedinUserId = GetCurrentLoggedinUserId();
                Guid userId = !string.IsNullOrEmpty(userName)
                    ? (Guid)Membership.GetUser(userName).ProviderUserKey
                    : loggedinUserId;

                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                var blogList = db.Blogs.Where(x => x.CreatedBy == userId && x.IsPublished == true && x.IsLink == true).ToList();

                shareIdeaBlogListModel.AddRange(blogList.Select(x => new ShareIdeaBlogListModel
                {
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                    TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                                 Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                                 Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                                 Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                                 Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                                 string.Empty,
                    CreatedBy = x.CreatedBy,
                    ReShareId = x.ReShareId > 0 ? true : false,
                    ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                    Like = IsLiked(x.Id),
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = (loggedinUserId == userId && userId == x.CreatedBy) ? true : false
                }));
                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetLinksIdeasProfile(string userName)
        {
            try
            {
                int BlockSize = CommonConstants.BlockSize;
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = GetBlogItemsProfile(1, BlockSize, userName, "linksIdea");

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel,
                    //RegistrationModel = GetRegistrationModelByUserId((Guid)Membership.GetUser(userName).ProviderUserKey)
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public JsonResult UserTagExist(string usertag)
        {
            bool isExist = false;
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                var userTag = db.RegistrationMasters.Where(x => x.UserId != userId && x.UserTag.ToLower() == usertag.ToLower()).ToList();
                if (userTag.Count > 0)
                {
                    isExist = true;
                }
                return Json(new { IsSuccess = isExist });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = isExist });
        }

        //share Idea list fetch counts
        public void FetchIdeasProfileCount(string username, string activeTab, DateTime lastDate, int option = 0)
        {
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                if (activeTab == "topIdea")
                {
                    newIdeaCount = GetTopIdeasListProfile(username, option).Where(x => x.CreatedOn > lastDate).Count();
                }
                else if (activeTab == "allIdea")
                {
                    newIdeaCount = GetAllIdeasListProfile(username).Where(x => x.CreatedOn > lastDate).Count();
                }
                else if (activeTab == "chartIdea")
                {
                    newIdeaCount = GetChartIdeasListProfile(username).Where(x => x.CreatedOn > lastDate).Count();
                }
                else if (activeTab == "linksIdea")
                {
                    newIdeaCount = GetLinksIdeasListProfile(username).Where(x => x.CreatedOn > lastDate).Count();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
        }

        public ActionResult FetchIdeaProfileCount(string username, string activeTab, DateTime lastDate, int option = 0)
        {
            FetchIdeasProfileCount(username, activeTab, lastDate, option);
            StockModel stockModel = new StockModel
            {
                NewIdeaCount = newIdeaCount
            };
            return PartialView("_ShareIdeaCount", stockModel);
        }

        #endregion

        #region "Recently Viewed"

        #region "Symbols"

        public void RecentlyViewed(Guid userId, string symbol, string type)
        {
            try
            {
                if (userId != Guid.Empty && !string.IsNullOrEmpty(symbol))
                {
                    RecentlyViewed recentlyViewed = db.RecentlyVieweds.Where(x => x.Symbol == symbol && x.UserId == userId && x.Exchange == type).AsEnumerable().FirstOrDefault();
                    if (recentlyViewed == null)
                    {
                        RecentlyViewed recentlyView = new RecentlyViewed
                        {
                            UserId = userId,
                            Symbol = symbol,
                            CreatedOn = DateTime.Now,
                            Exchange = type
                        };
                        db.RecentlyVieweds.Add(recentlyView);
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("RecentlyViewed", recentlyView.Id.ToString(), "Recently Viewed");
                    }
                    else
                    {
                        recentlyViewed.CreatedOn = DateTime.Now;
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("RecentlyViewed", recentlyViewed.Id.ToString(), "Recently Viewed CreatedOn update");
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
        }

        public List<UserRecentlyViewed> RecentlyViewedList(string userName)
        {
            try
            {
                List<UserRecentlyViewed> recentlyViewedList = new List<UserRecentlyViewed>();
                Guid userId = !string.IsNullOrEmpty(userName)
                              ? (Guid)Membership.GetUser(userName).ProviderUserKey
                              : GetCurrentLoggedinUserId();

                if (userId != Guid.Empty)
                {
                    var viewedList = db.RecentlyVieweds.Where(x => x.UserId == userId && x.Exchange != CommonConstants.USER).ToList().OrderByDescending(x => x.CreatedOn);
                    recentlyViewedList.AddRange(viewedList.Select(x => new UserRecentlyViewed
                    {
                        Symbol = x.Symbol,
                        BseSymbol = string.Empty,
                        Exchange = x.Exchange
                    }));
                }
                return recentlyViewedList.Take(10).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<UserRecentlyViewed>();
        }

        #endregion

        #region "Users"
        public void RecentlyViewedUser(Guid userId, Guid viewedUserId, string type)
        {
            try
            {
                if (userId != Guid.Empty && viewedUserId != Guid.Empty)
                {
                    RecentlyViewed recentlyViewedUser = db.RecentlyVieweds.Where(x => x.ViewedUserId == viewedUserId && x.UserId == userId && x.Exchange == type).AsEnumerable().FirstOrDefault();
                    if (recentlyViewedUser == null)
                    {
                        RecentlyViewed recentlyView = new RecentlyViewed
                        {
                            UserId = userId,
                            ViewedUserId = viewedUserId,
                            CreatedOn = DateTime.Now,
                            Exchange = type
                        };
                        db.RecentlyVieweds.Add(recentlyView);
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("RecentlyViewedUser", recentlyView.Id.ToString(), "Recently Viewed User");
                    }
                    else
                    {
                        recentlyViewedUser.CreatedOn = DateTime.Now;
                        db.SaveChanges();
                        SiteActivityErrorsUtil.SiteActivity("RecentlyViewedUser", recentlyViewedUser.Id.ToString(), "Recently Viewed User CreatedOn update");
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
        }

        public List<UserRecentlyViewed> RecentlyViewedUserList(string userName)
        {
            try
            {
                List<UserRecentlyViewed> recentlyViewedUserList = new List<UserRecentlyViewed>();
                Guid userId = !string.IsNullOrEmpty(userName)
                              ? (Guid)Membership.GetUser(userName).ProviderUserKey
                              : GetCurrentLoggedinUserId();

                if (userId != Guid.Empty)
                {
                    var viewedUserList = db.RecentlyVieweds.Where(x => x.UserId == userId && x.Exchange == CommonConstants.USER && userId != x.ViewedUserId).ToList().OrderByDescending(x => x.CreatedOn);
                    recentlyViewedUserList.AddRange(viewedUserList.Select(x => new UserRecentlyViewed
                    {
                        ViewedUser = new Guid(Convert.ToString(x.ViewedUserId)),
                        Username = Membership.GetUser(new Guid(Convert.ToString(x.ViewedUserId))).UserName,
                        Exchange = x.Exchange
                    }));
                }
                return recentlyViewedUserList.Take(10).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<UserRecentlyViewed>();
        }

        #endregion

        public JsonResult RecentlyViewedListFilter()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    UserRecentlyViewedList = RecentlyViewedList(string.Empty),
                    RecentlyViewedUserList = RecentlyViewedUserList(string.Empty)
                };
                var stringView = RenderRazorViewToString("_RecentlyViewed", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        #endregion

        #region "Watch List"
        [HttpGet]
        public JsonResult WatchList(string searchText, string type)
        {
            try
            {
                List<SymbolModel> symbolModelList = new List<SymbolModel>();
                var symbols = SymbolSearchList(searchText);

                //var nseList = symbols.AsEnumerable().ToList().Where(x => !string.IsNullOrEmpty(x.NseSymbol)).ToList();  //.Where(x => !string.IsNullOrEmpty(x.NseSymbol)).ToList();
                //var bseList = symbols.AsEnumerable().ToList().Where(x => string.IsNullOrEmpty(x.NseSymbol)).ToList();
                if (type == CommonConstants.NSE)
                {
                    /*//NSE
                    symbolModelList.AddRange(nseList.AsEnumerable().Select(x => new SymbolModel
                    {
                        SearchType = 1,
                        NseSymbol = x.NseSymbol,
                        BseSymbol = string.Empty,
                        CompanyName = x.CompanyName,
                        IsinNumber = x.IsinNumber
                    }));*/
                }
                else if (type == CommonConstants.BSE)
                {
                    /*//BSE
                    symbolModelList.AddRange(bseList.AsEnumerable().Select(x => new SymbolModel
                    {
                        SearchType = 1,
                        NseSymbol = String.Empty,
                        BseSymbol = x.BseSymbol,
                        CompanyName = x.CompanyName,
                        IsinNumber = x.IsinNumber
                    }));*/
                }
                else if (type == "null")
                {
                    symbolModelList.AddRange(symbols.AsEnumerable().Select(x => new SymbolModel
                    {
                        SearchType = 1,
                        NseSymbol = x.NseSymbol,
                        BseSymbol = x.BseSymbol,
                        CompanyName = x.CompanyName,
                        IsinNumber = x.IsinNumber
                    }));
                }

                return Json(symbolModelList.Take(5).ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new List<SymbolModel>(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult WatchListFilter(string symbol, string type)
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    //null stands for NSE and BSE both.
                    WatchListModel = type == CommonConstants.NSE ? WatchListBind(symbol, CommonConstants.NSE) : type == CommonConstants.BSE ? WatchListBind(symbol, CommonConstants.BSE) : WatchListBind(symbol, "null")
                };
                var stringView1 = RenderRazorViewToString("_ProfileWatchListFilter", stockModel);
                var stringView = RenderRazorViewToString("_WatchList", stockModel);
                return Json(new { IsSuccess = true, html = stringView, html1 = stringView1 });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public JsonResult WatchListAdd(string item, string type)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                var watchList = db.UserWatchLists.FirstOrDefault(x => x.Symbol == item && x.UserId == userId);
                if (watchList == null)
                {
                    UserWatchList userWatchList = new UserWatchList
                    {
                        UserId = userId,
                        Symbol = item,
                        Exchange = type,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.UserWatchLists.Add(userWatchList);
                    db.SaveChanges();
                    SiteActivityErrorsUtil.SiteActivity("WatchListAdd", userWatchList.Id.ToString(), "User WatchList Add");
                    return Json(new { IsSuccess = true });
                }
                else if (watchList != null && watchList.IsDeleted == true)
                {
                    watchList.IsDeleted = false;
                    watchList.ModifiedOn = DateTime.Now;
                    watchList.ModifiedBy = userId;
                    db.SaveChanges();
                    return Json(new { IsSuccess = true });
                }
                return Json(new { IsSuccess = false, html = "Already added in watch list." });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<WatchList> WatchListBind(string symbol, string type)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    List<WatchList> watchListModel = new List<WatchList>();
                    var symbols = db.UserWatchLists.Where(x => x.UserId == userId && x.IsDeleted == false).Select(x => new { x.Symbol, x.Exchange }).ToList();
                    string nseSymbols = string.Empty;
                    string bseSymbols = string.Empty;

                    if (type == "null")
                    {
                        var nseSymbolList = symbols.Where(x => x.Exchange == CommonConstants.NSE).Select(x => x.Symbol).ToList();
                        var bseSymbolList = symbols.Where(x => x.Exchange == CommonConstants.BSE).Select(x => x.Symbol).ToList();
                        if (nseSymbolList.Count() > 0)
                        {
                            foreach (var item in nseSymbolList)
                            {
                                //NSE
                                var nseJsonModel = NseDetailList(item);
                                watchListModel.AddRange(nseJsonModel.Select(x => new WatchList
                                {
                                    CompanyName = db.SymbolMasters.FirstOrDefault(y => y.NSESymbol == x.t).CompanyName,
                                    StockSymbol = x.t,
                                    StockUpDown = x.c,
                                    StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                    StockRate = x.l,
                                    CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                    Exchange = CommonConstants.NSE
                                }));
                            }
                        }
                        if (bseSymbolList.Count() > 0)
                        {
                            foreach (var item in bseSymbolList)
                            {
                                //BSE
                                var nseJsonModel = BseDetailList(item);
                                watchListModel.AddRange(nseJsonModel.Select(x => new WatchList
                                {
                                    CompanyName = db.SymbolMasters.FirstOrDefault(y => y.BSESymbol == x.t).CompanyName,
                                    StockSymbol = x.t,
                                    StockUpDown = x.c,
                                    StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                    StockRate = x.l,
                                    CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                    Exchange = CommonConstants.BSE
                                }));
                            }
                        }
                    }
                    else if (type == CommonConstants.NSE)
                    {
                        nseSymbols = string.Join(",", symbols.Where(x => x.Exchange == CommonConstants.NSE).Select(x => x.Symbol).ToList());
                        //NSE
                        var urlNse = !string.IsNullOrEmpty(nseSymbols)
                            ? "http://finance.google.com/finance/info?q=NSE:" + HttpUtility.UrlEncode(nseSymbols)
                            : string.Empty;

                        if (!string.IsNullOrEmpty(urlNse))
                        {
                            WebClient client = new WebClient();
                            string json = client.DownloadString(urlNse);
                            json = json.Replace("//", "");
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            var nseJsonModel = serializer.Deserialize<NseJsonModel[]>(json);

                            watchListModel.AddRange(nseJsonModel.Select(x => new WatchList
                            {
                                CompanyName = db.SymbolMasters.FirstOrDefault(y => y.NSESymbol == x.t).CompanyName,
                                StockSymbol = x.t,
                                StockUpDown = x.c,
                                StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                StockRate = x.l,
                                CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                Exchange = CommonConstants.NSE
                            }));
                        }
                        else
                        {
                            new List<WatchList>();
                        }
                    }
                    else if (type == CommonConstants.BSE)
                    {
                        bseSymbols = string.Join(",", symbols.Where(x => x.Exchange == CommonConstants.BSE).Select(x => x.Symbol).ToList());
                        //BSE
                        var urlBse = !string.IsNullOrEmpty(bseSymbols)
                            ? "http://finance.google.com/finance/info?q=BOM:" + HttpUtility.UrlEncode(bseSymbols)
                            : string.Empty;
                        if (!string.IsNullOrEmpty(urlBse))
                        {
                            WebClient client = new WebClient();
                            string json = client.DownloadString(urlBse);
                            json = json.Replace("//", "");
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            var nseJsonModel = serializer.Deserialize<NseJsonModel[]>(json);

                            watchListModel.AddRange(nseJsonModel.Select(x => new WatchList
                            {
                                CompanyName = db.SymbolMasters.FirstOrDefault(y => y.BSESymbol == x.t).CompanyName,
                                StockSymbol = x.t,
                                StockUpDown = x.c,
                                StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                StockRate = x.l,
                                CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                Exchange = CommonConstants.BSE
                            }));
                        }
                        else
                        {
                            new List<WatchList>();
                        }
                    }

                    return watchListModel.OrderByDescending(x => x.CreatedOn).ToList();
                }
                return new List<WatchList>();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<WatchList>();
        }

        public NseJsonModel[] NseDetailList(string symbol)
        {
            try
            {
                //NSE
                var urlNse = "http://finance.google.com/finance/info?q=NSE:" + HttpUtility.UrlEncode(symbol);

                WebClient client = new WebClient();
                string json = client.DownloadString(urlNse);
                json = json.Replace("//", "");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var nseJsonModel = serializer.Deserialize<NseJsonModel[]>(json);

                return nseJsonModel;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return null;
        }

        public NseJsonModel[] BseDetailList(string symbol)
        {
            Guid userId = GetCurrentLoggedinUserId();
            try
            {
                //BSE
                var urlBse = "http://finance.google.com/finance/info?q=BOM:" + HttpUtility.UrlEncode(symbol);

                WebClient client = new WebClient();
                string json = client.DownloadString(urlBse);
                json = json.Replace("//", "");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var bseJsonModel = serializer.Deserialize<NseJsonModel[]>(json);
                return bseJsonModel;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return null;
        }

        public List<WatchList> ProfileWatchListBind()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    List<WatchList> watchListModel = new List<WatchList>();
                    var symbols = db.UserWatchLists.Where(x => x.UserId == userId && x.IsDeleted == false).Select(x => new { x.Symbol, x.Exchange }).ToList();
                    var nseSymbolList = symbols.Where(x => x.Exchange == CommonConstants.NSE).Select(x => x.Symbol).ToList();
                    var bseSymbolList = symbols.Where(x => x.Exchange == CommonConstants.BSE).Select(x => x.Symbol).ToList();

                    if (nseSymbolList.Count() > 0)
                    {
                        //NSE
                        foreach (var item in nseSymbolList)
                        {
                            var nseJsonModel = NseDetailList(item);
                            watchListModel.AddRange(nseJsonModel.Select(x => new WatchList
                            {
                                CompanyName = db.SymbolMasters.FirstOrDefault(y => y.NSESymbol == x.t).CompanyName,
                                StockSymbol = x.t,
                                StockUpDown = x.c,
                                StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                StockRate = x.l,
                                CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                Exchange = CommonConstants.NSE
                            }));
                        }

                    }

                    if (bseSymbolList.Count() > 0)
                    {
                        //BSE
                        foreach (var item in bseSymbolList)
                        {
                            var bseJsonModel = BseDetailList(item);

                            watchListModel.AddRange(bseJsonModel.Select(x => new WatchList
                            {
                                CompanyName = db.SymbolMasters.FirstOrDefault(y => y.BSESymbol == x.t).CompanyName,
                                StockSymbol = x.t,
                                StockUpDown = x.c,
                                StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                StockRate = x.l,
                                CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                Exchange = CommonConstants.BSE
                            }));
                        }
                    }
                    if (watchListModel.Count() > 0)
                    {
                        return watchListModel.OrderByDescending(x => x.CreatedOn).ToList();
                    }
                    else
                    {
                        return new List<WatchList>();
                    }
                }
                return new List<WatchList>();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<WatchList>();
        }

        public List<WatchList> ProfileWatchListBind(Guid userId)
        {
            try
            {
                //Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    List<WatchList> watchListModel = new List<WatchList>();
                    var symbols = db.UserWatchLists.Where(x => x.UserId == userId && x.IsDeleted == false).Select(x => new { x.Symbol, x.Exchange }).ToList();
                    var nseSymbolList = symbols.Where(x => x.Exchange == CommonConstants.NSE).Select(x => x.Symbol).ToList();
                    var bseSymbolList = symbols.Where(x => x.Exchange == CommonConstants.BSE).Select(x => x.Symbol).ToList();

                    if (nseSymbolList.Count() > 0)
                    {
                        //NSE
                        foreach (var item in nseSymbolList)
                        {
                            var nseJsonModel = NseDetailList(item);
                            watchListModel.AddRange(nseJsonModel.Select(x => new WatchList
                            {
                                CompanyName = db.SymbolMasters.FirstOrDefault(y => y.NSESymbol == x.t).CompanyName,
                                StockSymbol = x.t,
                                StockUpDown = x.c,
                                StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                StockRate = x.l,
                                CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                Exchange = CommonConstants.NSE
                            }));
                        }

                    }

                    if (bseSymbolList.Count() > 0)
                    {
                        //BSE
                        foreach (var item in bseSymbolList)
                        {
                            var bseJsonModel = BseDetailList(item);

                            watchListModel.AddRange(bseJsonModel.Select(x => new WatchList
                            {
                                CompanyName = db.SymbolMasters.FirstOrDefault(y => y.BSESymbol == x.t).CompanyName,
                                StockSymbol = x.t,
                                StockUpDown = x.c,
                                StockRatePercentage = x.cp.Replace('+', ' ').Replace('-', ' ').Trim(),
                                StockRate = x.l,
                                CreatedOn = db.UserWatchLists.FirstOrDefault(s => s.Symbol == x.t && s.UserId == userId).CreatedOn,
                                Exchange = CommonConstants.BSE
                            }));
                        }
                    }
                    if (watchListModel.Count() > 0)
                    {
                        return watchListModel.OrderByDescending(x => x.CreatedOn).ToList();
                    }
                    else
                    {
                        return new List<WatchList>();
                    }
                }
                return new List<WatchList>();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<WatchList>();
        }

        public int WatchListCount(string symbol)
        {
            int watchListCount = 0;
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    watchListCount = db.UserWatchLists.Where(x => x.Symbol == symbol && x.IsDeleted == false).ToList().Count();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return watchListCount;
        }

        public bool IsWatched(string symbol)
        {
            bool isWatch = false;
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    var watchList = db.UserWatchLists.Where(x => x.UserId == userId && x.Symbol == symbol && x.IsDeleted == false).ToList();
                    if (watchList.Count() > 0)
                    {
                        isWatch = true;
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return isWatch;
        }

        public JsonResult UnWatch(string symbol, string type, bool isUnwatch)
        {
            try
            {
                type = type.Trim();
                if (!string.IsNullOrEmpty(symbol))
                {
                    string msg = string.Empty;
                    if (isUnwatch)
                    {
                        Guid userId = GetCurrentLoggedinUserId();
                        UserWatchList userWatchlist = db.UserWatchLists.FirstOrDefault(x => x.UserId == userId && x.Symbol == symbol);
                        if (userWatchlist != null)
                        {
                            userWatchlist.IsDeleted = true;
                            userWatchlist.ModifiedOn = DateTime.Now;
                            userWatchlist.ModifiedBy = userId;
                            db.SaveChanges();
                            msg = "Unwatch done.";
                        }
                    }
                    StockModel stockModel = new StockModel
                    {
                        StockDetail = type == CommonConstants.NSE ? BindNseStock(symbol) : type == CommonConstants.BSE ? BindBseStock(symbol) : new StockDetail(),
                        //WatchListCount = type == CommonConstants.NSE ? WatchListCount(symbol) : type == CommonConstants.BSE ? WatchListCount(symbol) : 0,
                        IsWatch = type == CommonConstants.NSE ? IsWatched(symbol) : type == CommonConstants.BSE ? IsWatched(symbol) : false,
                    };
                    var stringView = RenderRazorViewToString("_WatchListButton", stockModel);
                    return Json(new { IsSuccess = true, html = stringView, msg = msg });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            var stringView1 = RenderRazorViewToString("_WatchListButton", new StockModel());
            return Json(new { IsSuccess = false, html = stringView1, msg = "Something went wrong." });
        }

        public ActionResult IsWatchCheck(string symbol, string type)
        {
            StockModel stockModel = new StockModel();
            try
            {
                if (!string.IsNullOrEmpty(symbol))
                {
                    bool isWatch = IsWatched(symbol);
                    stockModel.IsWatch = isWatch;
                    StockDetail stockDetail = new Models.Web.StockDetail
                    {
                        StockSymbol = symbol,
                        Exchange = type
                    };
                    stockModel.StockDetail = stockDetail;
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_WatchListButton", stockModel);
        }

        #endregion

        #region "Get NSE_BSE Type"
        public string NSE_BSEType(string symbol)
        {
            string nseBse = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(symbol))
                {
                    var nseList = db.SymbolMasters.Where(x => !string.IsNullOrEmpty(x.NSESymbol)).ToList();
                    var findNse = nseList.Where(x => x.NSESymbol.ToLower() == symbol.ToLower()).ToList();
                    if (findNse.Count() > 0)
                    {
                        nseBse = CommonConstants.NSE;
                    }
                    else
                    {
                        nseBse = CommonConstants.BSE;
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return nseBse;
        }


        #endregion

        #region "Share Idea Blog"

        public List<string> hashTags = new List<string>();
        public string ShareIdeaDescription(string description)
        {
            string input = description;
            string input1 = description;
            //List<string> hashTags = new List<string>();

            foreach (System.Text.RegularExpressions.Match match in System.Text.RegularExpressions.Regex.Matches(input, "(\\#\\w+) "))
            {
                if (!headerShareIdeahashTags.Contains(match.Groups[1].Value))
                {
                    headerShareIdeahashTags.Add(match.Groups[1].Value);
                    string nseBse = NSE_BSEType(match.Groups[1].Value.Replace("#", "").Trim());
                    input1 = input1.Replace(match.Groups[1].Value, "<a href='javascript:void(0);' onmouseout='Common.CommonMenuHoverComplete()' onmouseover='Common.CommonMenuHover(this,event)' onclick='Common.CommonMenu(this)' id='" + match.Groups[1].Value.Replace("#", "").Trim() + "," + nseBse + "'><b>" + match.Groups[1].Value + "</b></a>");
                }

            }
            string input2 = ShareIdeaDescriptionAt(input1);
            return input2.Replace("\n", "<br/>");
        }
        public string ShareIdeaDescriptionAt(string description)
        {
            string input = description;
            string input1 = description;
            List<string> hashTags1 = new List<string>();

            foreach (System.Text.RegularExpressions.Match match in System.Text.RegularExpressions.Regex.Matches(input, "(\\@\\w+) "))
            {
                if (!hashTags1.Contains(match.Groups[1].Value))
                {
                    hashTags1.Add(match.Groups[1].Value);
                    string nseBse = NSE_BSEType(match.Groups[1].Value.Replace("@", "").Trim());
                    input1 = input1.Replace(match.Groups[1].Value, "<a href='/Profile/" + match.Groups[1].Value.Replace("@", "").Trim() + "'><b>" + match.Groups[1].Value + "</b></a>");
                }
            }
            return input1.Replace("\n", "<br/>");
        }
        [ValidateInput(false)]
        public JsonResult ShareIdea(ShareIdeaBlogModel model)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please Login First." });
                }
                string[] symbols = model.Symbol.Split(',');

                if (!string.IsNullOrEmpty(model.Symbol))
                {
                    string setImage = string.Empty;
                    if (!string.IsNullOrEmpty(model.NewFileName))
                    {
                        setImage = "<br/><br/> <a href='" + model.OriginalFileName + "' rel='prettyPhoto'><img src='" + model.NewFileName + "' alt='' /></a>";
                    }
                    string shareIdeaDescriptionString = ShareIdeaDescription(model.Description) + setImage;
                    Blog blog = new Blog
                    {
                        Description = shareIdeaDescriptionString,
                        EmailOnReply = model.EmailOnReply,
                        Everyone = model.Everyone,
                        OnlyFollowers = model.OnlyFollowers,
                        TrendInSights = model.TrendInSights != null ? Convert.ToInt32(model.TrendInSights) : -1,
                        CreatedOn = DateTime.Now,
                        CreatedBy = userId,
                        IsPublished = true,
                        Symbol = !string.IsNullOrEmpty(symbols[0]) ? symbols[0] : symbols[1],
                        NewFileName = model.NewFileName,
                        OriginalFileName = model.OriginalFileName,
                        IsLink = model.IsLink
                    };
                    db.Blogs.Add(blog);
                    db.SaveChanges();
                    if (headerShareIdeahashTags.Count() > 0)
                    {
                        foreach (var hashTag in headerShareIdeahashTags)
                        {
                            if (!hashTag.Contains('@'))
                            {
                                string refTag = hashTag.Replace('#', ' ').Trim();
                                if (refTag != (!string.IsNullOrEmpty(symbols[0]) ? symbols[0] : symbols[1]))
                                {
                                    Blog blog1 = new Blog
                                    {
                                        Description = shareIdeaDescriptionString,
                                        EmailOnReply = model.EmailOnReply,
                                        Everyone = model.Everyone,
                                        OnlyFollowers = model.OnlyFollowers,
                                        TrendInSights = model.TrendInSights != null ? Convert.ToInt32(model.TrendInSights) : -1,
                                        CreatedOn = DateTime.Now,
                                        CreatedBy = userId,
                                        IsPublished = true,
                                        Symbol = refTag,
                                        NewFileName = model.NewFileName,
                                        OriginalFileName = model.OriginalFileName
                                    };
                                    db.Blogs.Add(blog1);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    SiteActivityErrorsUtil.SiteActivity("ShareIdea", blog.Id.ToString(), "Share Idea");
                }
                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = ShareIdeaBlogList(!string.IsNullOrEmpty(symbols[0]) ? symbols[0] : symbols[1])
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public EmailModel ShareIdeaEmailBody(string userName, string shareIdea, string comment)
        {
            var shareIdeaTemplate = db.TemplateMasters.Where(x => x.TemplateName.ToLower().Trim() == ("ShareIdea").ToLower().Trim()).FirstOrDefault();
            if (shareIdeaTemplate == null)
            {
                return new EmailModel();
            }
            EmailModel emailModel = new EmailModel
            {
                subject = shareIdeaTemplate.TemplateSubject,
                body = shareIdeaTemplate.TemplateDescription.Replace("{{UserName}}", userName.Trim()).Replace("{{ShareIdea}}", shareIdea).Replace("{{Comment}}", comment),
                emailType = "ShareIdea",
                emailfor = "New ShareIdea"
            };
            return emailModel;
        }

        public List<ShareIdeaBlogListModel> ShareIdeaBlogListWithoutLogin(string symbol)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();
                //Not login - Everyone
                if (userId == Guid.Empty)
                {
                    var blogList = db.Blogs.Where(x => x.Symbol == symbol && x.Everyone == true && x.IsPublished == true).ToList();

                    shareIdeaBlogListModel.AddRange(blogList.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.Id,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                        TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                                 Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                                 Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                                 Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                                 Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                                 string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId > 0 ? true : false,
                        ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                        Like = IsLiked(x.Id),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.Id).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,

                    }));
                    return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public List<ShareIdeaBlogListModel> ShareIdeaBlogList(string symbol)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                //Not login - Everyone
                if (userId == Guid.Empty)
                {
                    return shareIdeaBlogListModel = ShareIdeaBlogListWithoutLogin(symbol);
                }
                else //Login 
                {
                    List<Blog> blogListFollower = new List<Blog>();
                    var userFollowers =
                        string.Join(",",
                        db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).Select(x => x.FollowTo));

                    blogListFollower = db.Blogs.Where(x => (x.Symbol == symbol && x.IsPublished == true)
                                       && (x.Everyone == true || x.CreatedBy == userId || userFollowers.Contains(x.CreatedBy.ToString()))).ToList();

                    shareIdeaBlogListModel.AddRange(blogListFollower.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.Id,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                        TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId > 0 ? true : false,
                        ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                        Like = IsLiked(x.Id),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.Id).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                    }));
                    return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
                }

            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult BlogComment(string comment, long blogId, string attachFile, string originalFileComment)
        {
            try
            {
                string fileContent = string.Empty;
                Guid userId = GetCurrentLoggedinUserId();
                var blog = db.Blogs.FirstOrDefault(x => x.Id == blogId);
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please login first." });
                }
                if (!string.IsNullOrEmpty(attachFile))
                {
                    fileContent = "<br/><a href='" + originalFileComment + "' rel='prettyPhoto'><img src='" + attachFile + "' alt='' /></a>";
                }

                BlogComment blogComment = new BlogComment
                {
                    BlogId = blogId,
                    ParentId = null,
                    Description = comment,
                    Attachment = fileContent,
                    OriginalFileName = originalFileComment,
                    CreatedBy = userId,
                    CreatedOn = DateTime.Now,
                    Isdeleted = false
                };
                db.BlogComments.Add(blogComment);
                db.SaveChanges();
                if (blog != null)
                {
                    if (blog.EmailOnReply == true)
                    {
                        MembershipUser membershipUser = Membership.GetUser(blog.CreatedBy);
                        string userName = membershipUser.UserName;  //GetCurrentLoggedinUserName();
                        string email = membershipUser.Email;
                        if (!string.IsNullOrEmpty(email))
                        {
                            var emailBody = ShareIdeaEmailBody(userName, blog.Description, comment);
                            MailUtil.SendMail(email, "", "", emailBody.subject, emailBody.body, false, "", emailBody.emailType, string.Empty, emailBody.emailfor);
                        }
                    }
                }
                SiteActivityErrorsUtil.SiteActivity("BlogComment", blogComment.Id.ToString(), "User Blog Comment");
                return Json(new { IsSuccess = true });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false });
        }

        [HttpGet]
        public ActionResult GetCommentDialog(long blogId)
        {
            try
            {
                List<CommentListUserWise> commentListUserWise = new List<CommentListUserWise>();
                Blog blog = db.Blogs.FirstOrDefault(x => x.Id == blogId);
                if (blog == null || blogId == 0)
                {
                    return PartialView("_BlogComments", new CommentModel());
                }
                var commentList = db.BlogComments.Where(s => s.BlogId == blogId).ToList();

                commentListUserWise.AddRange(commentList.Select(x => new CommentListUserWise
                {
                    CommentId = x.Id,
                    CommentCreatedBy = Membership.GetUser(x.CreatedBy).UserName,
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    CommentCreatedOn = x.CreatedOn,
                    Description = x.Description,
                    AttachFileName = x.Attachment
                }));
                CommentModel commentModel = new CommentModel
                {
                    BlogId = blogId,
                    UserName = blog.aspnet_Users.UserName,
                    ParentId = null,
                    CommentTextLimit = db.Settings.FirstOrDefault().CommentTextLimit,
                    CommentListUserWise = commentListUserWise
                };
                return PartialView("_BlogComments", commentModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_BlogComments", new CommentModel());
        }

        //share Idea list fetch counts
        public void FetchIdeasCount(string symbol, string activeTab, DateTime lastDate)
        {
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                if (activeTab == "topIdea")
                {
                    newIdeaCount = GetTopIdeasList(symbol).Where(x => x.CreatedOn > lastDate).Count();
                }
                else if (activeTab == "allIdea")
                {
                    newIdeaCount = GetAllIdeasList(symbol).Where(x => x.CreatedOn > lastDate).Count();
                }
                else if (activeTab == "chartIdea")
                {
                    newIdeaCount = GetChartIdeasList(symbol).Where(x => x.CreatedOn > lastDate).Count();
                }
                else if (activeTab == "linksIdea")
                {
                    newIdeaCount = GetLinksIdeasList(symbol).Where(x => x.CreatedOn > lastDate).Count();
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
        }

        public ActionResult FetchIdeaCount(string symbol, string activeTab, DateTime lastDate)
        {
            FetchIdeasCount(symbol, activeTab, lastDate);
            StockModel stockModel = new StockModel
            {
                NewIdeaCount = newIdeaCount
            };
            return PartialView("_ShareIdeaCount", stockModel);
        }

        //share Idea list and call methods

        public List<ShareIdeaBlogListModel> GetTopIdeasList(string symbol, int option = 0)
        {
            Guid userId = GetCurrentLoggedinUserId();
            try
            {
                List<ShareIdeaBlogListModel> shareIdeaBlogList = !string.IsNullOrEmpty(symbol) ? ShareIdeaBlogList(symbol) : new List<ShareIdeaBlogListModel>();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                if ((StreamPrefrence)option == StreamPrefrence.Comments)
                {
                    var blogTopListComments = (from b in shareIdeaBlogList
                                               join bc in db.BlogComments on b.BlogId equals bc.BlogId
                                               group new { b, bc } by new { b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogId, b.ReshareBlock }
                                                   into x
                                                   select new
                                                   {
                                                       BlogId = x.Key.BlogId,
                                                       Description = x.Key.Description,
                                                       TrendInSights = x.Key.TrendInSights,
                                                       ReShareId = x.Key.ReShareId,
                                                       CreatedOn = x.Key.CreatedOn,
                                                       CreatedBy = x.Key.CreatedBy,
                                                       Total = x.Count(),
                                                       ReshareBlock = x.Key.ReshareBlock
                                                   }).OrderByDescending(a => a.Total).ToList();
                    shareIdeaBlogListModel.AddRange(blogTopListComments.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId ?? 0,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId ?? 0),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                        ReshareBlock = x.ReshareBlock
                    }));
                }
                else if ((StreamPrefrence)option == StreamPrefrence.Reshare)
                {
                    var blogTopListReshare = (from b in shareIdeaBlogList
                                              join bc in db.Blogs on b.BlogId equals bc.ReShareId
                                              group new { b, bc } by new { b.BlogId, b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, b.ReshareBlock }
                                                  into x
                                                  select new
                                                  {
                                                      BlogId = x.Key.BlogId,
                                                      Description = x.Key.Description,
                                                      TrendInSights = x.Key.TrendInSights,
                                                      ReShareId = x.Key.ReShareId,
                                                      CreatedOn = x.Key.CreatedOn,
                                                      CreatedBy = x.Key.CreatedBy,
                                                      Total = x.Count(),
                                                      ReshareBlock = x.Key.ReshareBlock
                                                  }).OrderByDescending(a => a.Total).ToList();

                    shareIdeaBlogListModel.AddRange(blogTopListReshare.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                        ReshareBlock = x.ReshareBlock
                    }));
                }
                else if ((StreamPrefrence)option == StreamPrefrence.Likes)
                {
                    var blogTopListLike = (from b in shareIdeaBlogList
                                           join bc in db.BlogCommentsLikes on b.BlogId equals bc.BlogCommentsId
                                           where bc.Isdeleted == false
                                           group new { b, bc } by new { b.BlogId, b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogCommentsId, b.ReshareBlock }
                                               into x
                                               select new
                                               {
                                                   BlogId = x.Key.BlogId,
                                                   Description = x.Key.Description,
                                                   TrendInSights = x.Key.TrendInSights,
                                                   ReShareId = x.Key.ReShareId,
                                                   CreatedOn = x.Key.CreatedOn,
                                                   CreatedBy = x.Key.CreatedBy,
                                                   Total = x.Count(),
                                                   ReshareBlock = x.Key.ReshareBlock
                                               }).OrderByDescending(a => a.Total).ToList();
                    shareIdeaBlogListModel.AddRange(blogTopListLike.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.BlogId,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        TrendInSights = x.TrendInSights,
                        TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                             Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                             string.Empty,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CreatedBy = x.CreatedBy,
                        ReShareId = x.ReShareId,
                        Like = IsLiked(x.BlogId),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                        ReshareBlock = x.ReshareBlock
                    }));
                }
                /* else if ((StreamPrefrence)option == StreamPrefrence.CommentsAndLikes)
                 {
                     var blogTopListComments = (from b in shareIdeaBlogList
                                                join bc in db.BlogComments on b.BlogId equals bc.BlogId
                                                group new { b, bc } by new { b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogId, b.ReshareBlock }
                                                    into x
                                                    select new
                                                    {
                                                        BlogId = x.Key.BlogId,
                                                        Description = x.Key.Description,
                                                        TrendInSights = x.Key.TrendInSights,
                                                        ReShareId = x.Key.ReShareId,
                                                        CreatedOn = x.Key.CreatedOn,
                                                        CreatedBy = x.Key.CreatedBy,
                                                        Total = x.Count(),
                                                        ReshareBlock = x.Key.ReshareBlock
                                                    }).OrderByDescending(a => a.Total).ToList();
                     shareIdeaBlogListModel.AddRange(blogTopListComments.Select(x => new ShareIdeaBlogListModel
                     {
                         BlogId = x.BlogId ?? 0,
                         UserName = Membership.GetUser(x.CreatedBy).ToString(),
                         Description = x.Description,
                         CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                         Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                         TrendInSights = x.TrendInSights,
                         TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                              string.Empty,
                         CreatedOn = Convert.ToDateTime(x.CreatedOn),
                         CreatedBy = x.CreatedBy,
                         ReShareId = x.ReShareId,
                         Like = IsLiked(x.BlogId ?? 0),
                         CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                         LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                         DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                         ReshareBlock = x.ReshareBlock
                     }));



                     var blogTopListLike = (from b in shareIdeaBlogList
                                            join bc in db.BlogCommentsLikes on b.BlogId equals bc.BlogCommentsId
                                            where bc.Isdeleted == false
                                            group new { b, bc } by new { b.BlogId, b.Description, b.CreatedOn, b.CreatedBy, b.TrendInSights, b.ReShareId, bc.BlogCommentsId, b.ReshareBlock }
                                                into x
                                                select new
                                                {
                                                    BlogId = x.Key.BlogId,
                                                    Description = x.Key.Description,
                                                    TrendInSights = x.Key.TrendInSights,
                                                    ReShareId = x.Key.ReShareId,
                                                    CreatedOn = x.Key.CreatedOn,
                                                    CreatedBy = x.Key.CreatedBy,
                                                    Total = x.Count(),
                                                    ReshareBlock = x.Key.ReshareBlock
                                                }).OrderByDescending(a => a.Total).ToList();
                     shareIdeaBlogListModel.AddRange(blogTopListLike.Select(x => new ShareIdeaBlogListModel
                     {
                         BlogId = x.BlogId,
                         UserName = Membership.GetUser(x.CreatedBy).ToString(),
                         Description = x.Description,
                         CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                         Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                         TrendInSights = x.TrendInSights,
                         TrendInSightsColor = Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 0 ? CommonConstants.StrongBuy :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 1 ? CommonConstants.Buy :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 2 ? CommonConstants.Hold :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 3 ? CommonConstants.Sell :
                                              Convert.ToInt32(Enum.Parse(typeof(TrendInsights), EnumUtil.GetValueFromDescription<TrendInsights>(Convert.ToString(x.TrendInSights)).ToString())) == 4 ? CommonConstants.StrongSell :
                                              string.Empty,
                         CreatedOn = Convert.ToDateTime(x.CreatedOn),
                         CreatedBy = x.CreatedBy,
                         ReShareId = x.ReShareId,
                         Like = IsLiked(x.BlogId),
                         CommentsCount = db.BlogComments.Where(s => s.BlogId == x.BlogId).ToList().Count,
                         LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.BlogId && s.Isdeleted == false).ToList().Count,
                         DeleteIdeaOption = userId == x.CreatedBy ? true : false,
                         ReshareBlock = x.ReshareBlock
                     }));
                     shareIdeaBlogListModel = shareIdeaBlogListModel.OrderBy(x => x.CreatedOn).ToList();
                 }*/
                else { }
                return shareIdeaBlogListModel.ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetTopIdeas(string symbol, int option)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();
                if (userId == Guid.Empty)
                {
                    shareIdeaBlogListModel = GetTopIdeasList(symbol, option);
                    if (shareIdeaBlogListModel.Count() <= 0)
                    {
                        shareIdeaBlogListModel = GetAllIdeasList(symbol);
                    }
                }
                else
                {
                    int BlockSize = CommonConstants.BlockSize;
                    shareIdeaBlogListModel = GetBlogItems(1, BlockSize, symbol, "topIdea", option);
                }

                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetAllIdeasList(string symbol)
        {
            try
            {
                return !string.IsNullOrEmpty(symbol) ? ShareIdeaBlogList(symbol) : new List<ShareIdeaBlogListModel>();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetAllIdeas(string symbol)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();
                if (userId == Guid.Empty)
                {
                    shareIdeaBlogListModel = GetAllIdeasList(symbol);
                }
                else
                {
                    int BlockSize = CommonConstants.BlockSize;
                    shareIdeaBlogListModel = GetBlogItems(1, BlockSize, symbol, "allIdea");
                }
                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetChartIdeasList(string symbol)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();
                List<Blog> blogListFollower = new List<Blog>();

                var userFollowers =
                    string.Join(",",
                    db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).Select(x => x.FollowTo));

                blogListFollower = db.Blogs
                                  .Where(x => (x.Symbol == symbol
                                         && x.IsPublished == true
                                      && x.Description.ToString().Contains("<img"))
                                         && (x.Everyone == true
                                             || x.CreatedBy == userId
                                             || userFollowers.Contains(x.CreatedBy.ToString()))).ToList();

                shareIdeaBlogListModel.AddRange(blogListFollower.Select(x => new ShareIdeaBlogListModel
                {
                    BlogId = x.Id,
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    ReShareId = x.ReShareId > 0 ? true : false,
                    ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                    Like = IsLiked(x.Id),
                    TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                    TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                                 Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                                 Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                                 Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                                 Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                                 string.Empty,
                    CommentsCount = db.BlogComments.Where(s => s.BlogId == x.Id).ToList().Count,
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = userId == x.CreatedBy ? true : false
                }));
                shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();

                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetChartIdeas(string symbol)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                //Not login - Everyone
                if (userId == Guid.Empty)
                {
                    var blogList = db.Blogs.Where(x => x.Symbol == symbol && x.Everyone == true && x.IsPublished == true && x.Description.ToString().Contains("<img")).ToList();

                    shareIdeaBlogListModel.AddRange(blogList.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.Id,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        ReShareId = x.ReShareId > 0 ? true : false,
                        ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                        Like = IsLiked(x.Id),
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.Id).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false
                    }));
                    shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
                }
                else //Login 
                {
                    int BlockSize = CommonConstants.BlockSize;
                    shareIdeaBlogListModel = GetBlogItems(1, BlockSize, symbol, "chartIdea");
                }
                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public List<ShareIdeaBlogListModel> GetLinksIdeasList(string symbol)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                List<Blog> blogListFollower = new List<Blog>();
                var userFollowers =
                    string.Join(",",
                    db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).Select(x => x.FollowTo));

                blogListFollower = db.Blogs
                                  .Where(x => (x.Symbol == symbol
                                         && x.IsPublished == true
                                      //&& x.Description.ToString().Contains("<a") && !x.Description.ToString().Contains("prettyPhoto"))
                                      && x.IsLink == true)
                                         && (x.Everyone == true
                                             || x.CreatedBy == userId
                                             || userFollowers.Contains(x.CreatedBy.ToString()))).ToList();

                shareIdeaBlogListModel.AddRange(blogListFollower.Select(x => new ShareIdeaBlogListModel
                {
                    BlogId = x.Id,
                    UserName = Membership.GetUser(x.CreatedBy).ToString(),
                    Description = x.Description,
                    CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                    Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                    CreatedOn = Convert.ToDateTime(x.CreatedOn),
                    ReShareId = x.ReShareId > 0 ? true : false,
                    ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                    Like = IsLiked(x.Id),
                    TrendInSights = EnumUtil.GetEnumDescription((TrendInsights)Convert.ToInt32(x.TrendInSights)),
                    TrendInSightsColor = Convert.ToInt32(x.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                                 Convert.ToInt32(x.TrendInSights) == 1 ? CommonConstants.Buy :
                                                 Convert.ToInt32(x.TrendInSights) == 2 ? CommonConstants.Hold :
                                                 Convert.ToInt32(x.TrendInSights) == 3 ? CommonConstants.Sell :
                                                 Convert.ToInt32(x.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                                 string.Empty,
                    CommentsCount = db.BlogComments.Where(s => s.BlogId == x.Id).ToList().Count,
                    LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                    DeleteIdeaOption = userId == x.CreatedBy ? true : false
                }));
                shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
                return shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        public JsonResult GetLinksIdeas(string symbol)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                List<ShareIdeaBlogListModel> shareIdeaBlogListModel = new List<ShareIdeaBlogListModel>();

                //Not login - Everyone
                if (userId == Guid.Empty)
                {
                    var blogList = db.Blogs.Where(x => x.Symbol == symbol && x.IsPublished == true && x.Everyone == true && x.IsLink == true).ToList();

                    shareIdeaBlogListModel.AddRange(blogList.Select(x => new ShareIdeaBlogListModel
                    {
                        BlogId = x.Id,
                        UserName = Membership.GetUser(x.CreatedBy).ToString(),
                        Description = x.Description,
                        CreatedDateTime = DateUtil.ToTimeAgo(Convert.ToDateTime(x.CreatedOn)),
                        Image = GetUserImage(new Guid(x.CreatedBy.ToString()), "50"),
                        ReShareId = x.ReShareId > 0 ? true : false,
                        ReshareBlock = x.ReShareId > 0 ? ReshareBlock(x.ReShareId) : new ReshareBlockModel(),
                        Like = IsLiked(x.Id),
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        CommentsCount = db.BlogComments.Where(s => s.BlogId == x.Id).ToList().Count,
                        LikesCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == x.Id && s.Isdeleted == false).ToList().Count,
                        DeleteIdeaOption = userId == x.CreatedBy ? true : false
                    }));
                    shareIdeaBlogListModel.OrderByDescending(x => x.CreatedOn).ToList();
                }
                else //Login 
                {
                    int BlockSize = CommonConstants.BlockSize;
                    shareIdeaBlogListModel = GetBlogItems(1, BlockSize, symbol, "linksIdea");
                }
                StockModel stockModel = new StockModel
                {
                    LoggedInUser = SetLoggedInUser(),
                    ShareIdeaBlogModelList = shareIdeaBlogListModel
                };
                var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                return Json(new { IsSuccess = true, html = stringView });
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        #endregion

        #region "Menu"

        public List<StockMenu> Menu()
        {
            try
            {
                List<StockMenu> stockMenuList = new List<StockMenu>();

                //int MenuByHitComments = db.Settings.Select(x => x.MenuByHitComments).FirstOrDefault() ?? 0;
                int CommentsDays = db.Settings.Select(x => x.MenuByDays).FirstOrDefault() ?? 0;

                using (StockCharchaEntities context = new StockCharchaEntities())
                {
                    string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["StockCharchaMembershipEntities"].ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                    builder.ConnectTimeout = 2500;
                    SqlConnection con = new SqlConnection(builder.ConnectionString);
                    System.Data.Common.DbDataReader sqlReader;
                    con.Open();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "GetMenu";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        //cmd.Parameters.Add("@MenuByHitComments", SqlDbType.Int).Value = MenuByHitComments;
                        //cmd.Parameters.Add("@CommentsDays", SqlDbType.Int).Value = CommentsDays;
                        sqlReader = (System.Data.Common.DbDataReader)cmd.ExecuteReader();

                        while (sqlReader.Read())
                        {
                            StockMenu stockMenu = new StockMenu();
                            stockMenu.StockSymbol = sqlReader.GetString(0);
                            stockMenu.Total = sqlReader.GetInt32(1);
                            stockMenu.StockcompanyName = sqlReader.GetString(2);
                            stockMenu.Exchange = sqlReader.GetString(3);
                            stockMenuList.Add(stockMenu);
                        }
                    }
                    con.Close();
                }

                return stockMenuList.OrderByDescending(x => x.Total).Take(13).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<StockMenu>();
        }

        public JsonResult MenuHover(string symbol, string type)
        {
            try
            {
                if (!string.IsNullOrEmpty(symbol) && !string.IsNullOrEmpty(type))
                {
                    StockModel stockModel = new StockModel
                    {
                        StockDetail = type == CommonConstants.NSE ? BindNseStock(symbol) :
                                      type == CommonConstants.BSE ? BindBseStock(symbol) : new StockDetail(),
                        ConsensusChartModel = GetConsensusChartMaxRating(symbol) ?? new List<ConsensusChartModel>(),
                        IsWatch = type == CommonConstants.NSE ? IsWatched(symbol) :
                                  type == CommonConstants.BSE ? IsWatched(symbol) : false,
                    };
                    var stringView = RenderRazorViewToString("_MenuHover", stockModel);
                    return Json(new { IsSuccess = true, html = stringView });
                }
                else
                {
                    return Json(new { IsSuccess = false });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false });
        }

        public ActionResult MenuHoverGet(string symbol, string type)
        {
            try
            {
                if (!string.IsNullOrEmpty(symbol) && !string.IsNullOrEmpty(type))
                {
                    StockModel stockModel = new StockModel
                    {
                        StockDetail = type == CommonConstants.NSE ? BindNseStock(symbol) :
                                      type == CommonConstants.BSE ? BindBseStock(symbol) : new StockDetail(),
                        ConsensusChartModel = GetConsensusChartMaxRating(symbol) ?? new List<ConsensusChartModel>(),
                        IsWatch = type == CommonConstants.NSE ? IsWatched(symbol) :
                                  type == CommonConstants.BSE ? IsWatched(symbol) : false,
                    };
                    var stringView = RenderRazorViewToString("_MenuHover", stockModel);
                    return Json(new { IsSuccess = true, html = stringView }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "ReShare Idea"

        [HttpGet]
        public ActionResult GetReshareDialog(long blogId)
        {
            try
            {
                Blog blog = db.Blogs.FirstOrDefault(x => x.Id == blogId);
                if (blog == null || blogId == 0)
                {
                    return PartialView("_ReshareDialog", new ReshareModel());
                }
                ShareIdeaBlogListModel shareIdeaModel = new ShareIdeaBlogListModel
                {
                    BlogId = blogId,
                    Image = GetUserImage(new Guid(blog.CreatedBy.ToString()), "50"),
                    UserName = blog.aspnet_Users.UserName,
                    Description = blog.Description,
                    TrendInSights = Convert.ToString((TrendInsights)blog.TrendInSights),
                    TrendInSightsColor = Convert.ToInt32(blog.TrendInSights) == 0 ? CommonConstants.StrongBuy :
                                         Convert.ToInt32(blog.TrendInSights) == 1 ? CommonConstants.Buy :
                                         Convert.ToInt32(blog.TrendInSights) == 2 ? CommonConstants.Hold :
                                         Convert.ToInt32(blog.TrendInSights) == 3 ? CommonConstants.Sell :
                                         Convert.ToInt32(blog.TrendInSights) == 4 ? CommonConstants.StrongSell :
                                         string.Empty,
                };

                ReshareModel reshareModel = new ReshareModel
                {
                    BlogId = blogId,
                    ShareIdeaModel = shareIdeaModel
                };
                return PartialView("_ReshareDialog", reshareModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_ReshareDialog", new ReshareModel());
        }
        public JsonResult ReShareIdea(long blogId, string symbol, string activeButton, int option = 0, string description = "", bool reshareTimeline = false)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Login first to re-share idea." });
                }
                if (blogId > 0)
                {
                    var model = db.Blogs.FirstOrDefault(x => x.Id == blogId);
                    if (model != null)
                    {
                        Blog blog = new Blog
                        {
                            Description = !string.IsNullOrEmpty(description) ? description.Replace("\n", "<br/>") : string.Empty,
                            EmailOnReply = model.EmailOnReply,
                            Everyone = model.Everyone,
                            OnlyFollowers = model.OnlyFollowers,
                            TrendInSights = model.TrendInSights != null ? Convert.ToInt32(model.TrendInSights) : -1,
                            CreatedOn = DateTime.Now,
                            CreatedBy = GetCurrentLoggedinUserId(),
                            IsPublished = true,
                            Symbol = symbol,
                            NewFileName = model.NewFileName,
                            OriginalFileName = model.OriginalFileName,
                            ReShareId = blogId
                        };
                        db.Blogs.Add(blog);
                        db.SaveChanges();
                    }
                    StockModel stockModel = new StockModel();
                    if (reshareTimeline)
                    {
                        if (activeButton == "topIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetTopTimelineList(option);
                        }
                        else if (activeButton == "allIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetAllTimelineList();
                        }
                        else if (activeButton == "chartIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetChartTimelineList();
                        }
                        else if (activeButton == "linksIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetLinksTimelineList();
                        }
                    }
                    else if (!string.IsNullOrEmpty(symbol))
                    {
                        if (activeButton == "topIdea")
                        {
                            string symboll = !string.IsNullOrEmpty(symbol) ? symbol : string.Empty;
                            stockModel.ShareIdeaBlogModelList = GetTopIdeasList(symboll, option);
                        }
                        else if (activeButton == "allIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetAllIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                        else if (activeButton == "chartIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetChartIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                        else if (activeButton == "linksIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetLinksIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                    }
                    else
                    {
                        string username = GetCurrentLoggedinUserName();
                        if (activeButton == "topIdea")
                        {
                            string usernamee = !string.IsNullOrEmpty(username) ? username : string.Empty;
                            stockModel.ShareIdeaBlogModelList = GetTopIdeasListProfile(usernamee, option);
                        }
                        else if (activeButton == "allIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetAllIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                        else if (activeButton == "chartIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetChartIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                        else if (activeButton == "linksIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetLinksIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                    }
                    stockModel.LoggedInUser = SetLoggedInUser();
                    var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                    return Json(new { IsSuccess = true, html = stringView });
                }
                else
                {
                    return Json(new { IsSuccess = false, html = "Something went wrong." });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        public ReshareBlockModel ReshareBlock(long? blogId)
        {
            try
            {
                ReshareBlockModel reshareBlockModel = new ReshareBlockModel();
                if (blogId > 0)
                {
                    var model = db.Blogs.FirstOrDefault(x => x.Id == blogId);
                    if (model != null)
                    {
                        reshareBlockModel.BlogId = Convert.ToInt32(blogId);
                        reshareBlockModel.Image = db.RegistrationMasters.FirstOrDefault(u => u.UserId == model.CreatedBy).Id;
                        reshareBlockModel.userImage = GetUserImage(new Guid(model.CreatedBy.ToString()), "50");
                        reshareBlockModel.UserName = model.aspnet_Users.UserName;
                        reshareBlockModel.Description = model.Description;
                    }
                }

                return reshareBlockModel;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new ReshareBlockModel();
        }

        #endregion

        #region "Like Idea"

        public bool IsLiked(long blogId)
        {
            bool isLike = false;
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                var model = db.BlogCommentsLikes.FirstOrDefault(x => x.BlogCommentsId == blogId && x.Isdeleted == false && (x.CreatedBy == userId || x.ModifiedBy == userId));
                if (model != null)
                {
                    isLike = true;
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return isLike;
        }

        public JsonResult LikeIdea(long blogId, string symbol, string likeTable, string activeButton, int option = 0)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Login first to like idea." });
                }
                if (blogId > 0)
                {
                    string msg = string.Empty;
                    int likeCount = db.BlogCommentsLikes.Where(s => s.BlogCommentsId == blogId && s.Isdeleted == false).ToList().Count;
                    var model = db.BlogCommentsLikes.FirstOrDefault(x => x.BlogCommentsId == blogId && (x.CreatedBy == userId || x.ModifiedBy == userId));
                    if (model == null)
                    {
                        BlogCommentsLike blogCommentLike = new BlogCommentsLike
                        {
                            BlogCommentsId = blogId,
                            LikeTable = likeTable,
                            CreatedBy = GetCurrentLoggedinUserId(),
                            CreatedOn = DateTime.Now,
                            Isdeleted = false
                        };
                        db.BlogCommentsLikes.Add(blogCommentLike);
                        db.SaveChanges();
                        likeCount++;
                        msg = "Idea Liked";
                    }
                    else if (model != null && model.Isdeleted == true)
                    {
                        model.ModifiedBy = GetCurrentLoggedinUserId();
                        model.ModifiedOn = DateTime.Now;
                        model.Isdeleted = false;
                        db.SaveChanges();
                        likeCount++;
                        msg = "Idea Liked";
                    }
                    else
                    {
                        model.ModifiedBy = GetCurrentLoggedinUserId();
                        model.ModifiedOn = DateTime.Now;
                        model.Isdeleted = true;
                        db.SaveChanges();
                        likeCount--;
                        msg = "Idea Unliked";
                    }

                    StockModel stockModel = new StockModel();
                    /*if (!string.IsNullOrEmpty(symbol))
                    {
                        if (activeButton == "topIdea")
                        {
                            string symboll = !string.IsNullOrEmpty(symbol) ? symbol : string.Empty;
                            stockModel.ShareIdeaBlogModelList = GetTopIdeasList(symboll, option);
                        }
                        else if (activeButton == "allIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetAllIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                        else if (activeButton == "chartIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetChartIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                        else if (activeButton == "linksIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetLinksIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                    }
                    else
                    {
                        string username = GetCurrentLoggedinUserName();
                        if (activeButton == "topIdea")
                        {
                            string usernamee = !string.IsNullOrEmpty(username) ? username : string.Empty;
                            stockModel.ShareIdeaBlogModelList = GetTopIdeasListProfile(usernamee, option);
                        }
                        else if (activeButton == "allIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetAllIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                        else if (activeButton == "chartIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetChartIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                        else if (activeButton == "linksIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetLinksIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                    }
                    var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                    return Json(new { IsSuccess = true, html = stringView, stringMsg = msg });*/

                    return Json(new { IsSuccess = true, stringMsg = msg, likeCnt = likeCount });
                }
                else
                {
                    return Json(new { IsSuccess = false, html = "Something went wrong." });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        [HttpGet]
        public ActionResult GetLikeListDialog(long blogId)
        {
            try
            {

                Blog blog = db.Blogs.FirstOrDefault(x => x.Id == blogId);
                if (blog == null || blogId == 0)
                {
                    return PartialView("_LikeListDialog", new ReshareModel());
                }
                var likeList = db.BlogCommentsLikes.Where(x => x.BlogCommentsId == blogId && x.Isdeleted == false).ToList();
                var registrationModel = db.RegistrationMasters.ToList();


                Guid userId = GetCurrentLoggedinUserId();

                string followTo = string.Join(",", db.UserFollows.Where(x => x.FollowBy == userId && x.IsDeleted == false).Select(x => x.FollowTo).ToList());
                var registrationList = db.RegistrationMasters.Where(x => followTo.Contains(x.UserId.ToString())).ToList();


                List<LikeListModel> likeUsersListModel = new List<LikeListModel>();
                likeUsersListModel.AddRange(likeList.Select(x => new LikeListModel
                {
                    UserId = new Guid(x.CreatedBy.ToString()),
                    Username = Membership.GetUser(new Guid(Convert.ToString(x.CreatedBy))).UserName,
                    Firstname = registrationModel.FirstOrDefault(y => y.UserId == x.CreatedBy).FirstName,
                    Lastname = registrationModel.FirstOrDefault(y => y.UserId == x.CreatedBy).LastName,
                    UserImage = GetUserImage(new Guid(x.CreatedBy.ToString()), "32"),
                    IsFollowed = LikeFollowed(new Guid(x.CreatedBy.ToString()))
                }));

                return PartialView("_LikeListDialog", likeUsersListModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_LikeListDialog", new ReshareModel());
        }
        public bool LikeFollowed(Guid userId)
        {
            try
            {
                Guid currentUserId = GetCurrentLoggedinUserId();
                int userFollowed = db.UserFollows.Where(x => x.FollowBy == currentUserId && x.FollowTo == userId && x.IsDeleted == false).ToList().Count;
                return userFollowed > 0 ? true : false;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return false;
        }

        #endregion

        #region "Remove Idea"

        public JsonResult RemoveIdea(long blogId, string symbol, string activeButton, int option = 0)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Login first to remove idea." });
                }
                if (blogId > 0)
                {
                    string msg = string.Empty;
                    Blog blog = db.Blogs.FirstOrDefault(x => x.Id == blogId && (x.CreatedBy == userId));
                    if (blog != null)
                    {
                        blog.IsPublished = false;
                        db.SaveChanges();
                        msg = "Idea Removed";
                    }
                    StockModel stockModel = new StockModel();
                    if (!string.IsNullOrEmpty(symbol))
                    {
                        if (activeButton == "topIdea")
                        {
                            string symboll = !string.IsNullOrEmpty(symbol) ? symbol : string.Empty;
                            stockModel.ShareIdeaBlogModelList = GetTopIdeasList(symboll, option);
                        }
                        else if (activeButton == "allIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetAllIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                        else if (activeButton == "chartIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetChartIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                        else if (activeButton == "linksIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetLinksIdeasList(!string.IsNullOrEmpty(symbol) ? symbol : string.Empty);
                        }
                    }
                    else
                    {
                        string username = GetCurrentLoggedinUserName();
                        if (activeButton == "topIdea")
                        {
                            string usernamee = !string.IsNullOrEmpty(username) ? username : string.Empty;
                            stockModel.ShareIdeaBlogModelList = GetTopIdeasListProfile(usernamee, option);
                        }
                        else if (activeButton == "allIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetAllIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                        else if (activeButton == "chartIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetChartIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                        else if (activeButton == "linksIdea")
                        {
                            stockModel.ShareIdeaBlogModelList = GetLinksIdeasListProfile(!string.IsNullOrEmpty(username) ? username : string.Empty);
                        }
                    }
                    stockModel.LoggedInUser = SetLoggedInUser();
                    var stringView = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                    return Json(new { IsSuccess = true, html = stringView, stringMsg = msg });
                }
                else
                {
                    return Json(new { IsSuccess = false, html = "Something went wrong." });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, html = "Something went wrong." });
        }

        #endregion

        #region Image Resize

        public enum ResizeOptions
        {
            // Use fixed width & height without keeping the proportions
            ExactWidthAndHeight,

            // Use maximum width (as defined) and keeping the proportions
            MaxWidth,

            // Use maximum height (as defined) and keeping the proportions
            MaxHeight,

            // Use maximum width or height (the biggest) and keeping the proportions
            MaxWidthAndHeight
        }

        public static System.Drawing.Bitmap DoResize(System.Drawing.Bitmap originalImg, int widthInPixels, int heightInPixels)
        {
            System.Drawing.Bitmap bitmap;
            try
            {
                bitmap = new System.Drawing.Bitmap(widthInPixels, heightInPixels);
                using (System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(bitmap))
                {
                    // Quality properties
                    graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                    graphic.DrawImage(originalImg, 0, 0, widthInPixels, heightInPixels);
                    return bitmap;
                }
            }
            finally
            {
                if (originalImg != null)
                {
                    originalImg.Dispose();
                }
            }
        }

        public static System.Drawing.Bitmap ResizeImage(System.Drawing.Bitmap image, int width, int height, ResizeOptions resizeOptions)
        {
            float f_width;
            float f_height;
            float dim;
            switch (resizeOptions)
            {
                case ResizeOptions.ExactWidthAndHeight:
                    return DoResize(image, width, height);

                case ResizeOptions.MaxHeight:
                    f_width = image.Width;
                    f_height = image.Height;

                    if (f_height <= height)
                        return DoResize(image, (int)f_width, (int)f_height);

                    dim = f_width / f_height;
                    width = (int)((float)(height) * dim);
                    return DoResize(image, width, height);

                case ResizeOptions.MaxWidth:
                    f_width = image.Width;
                    f_height = image.Height;

                    if (f_width <= width)
                        return DoResize(image, (int)f_width, (int)f_height);

                    dim = f_width / f_height;
                    height = (int)((float)(width) / dim);
                    return DoResize(image, width, height);

                case ResizeOptions.MaxWidthAndHeight:
                    int tmpHeight = height;
                    int tmpWidth = width;
                    f_width = image.Width;
                    f_height = image.Height;

                    if (f_width <= width && f_height <= height)
                        return DoResize(image, (int)f_width, (int)f_height);

                    dim = f_width / f_height;

                    // Check if the width is ok
                    if (f_width < width)
                        width = (int)f_width;
                    height = (int)((float)(width) / dim);
                    // The width is too width
                    if (height > tmpHeight)
                    {
                        if (f_height < tmpHeight)
                            height = (int)f_height;
                        else
                            height = tmpHeight;
                        width = (int)((float)(height) * dim);
                    }
                    return DoResize(image, width, height);
                default:
                    return image;
            }
        }

        public static byte[] ImageToArray(Bitmap image, ImageFormat format)
        {
            using (MemoryStream mem = new MemoryStream())
            {
                mem.Position = 0;
                image.Save(mem, format);
                return mem.ToArray();
            }
        }

        public static ImageFormat GetImageFormat(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            if (string.IsNullOrEmpty(extension))
                throw new ArgumentException(
                    string.Format("Unable to determine file extension for fileName: {0}", fileName));

            switch (extension.ToLower())
            {
                case @".bmp":
                    return ImageFormat.Bmp;
                case @".gif":
                    return ImageFormat.Gif;
                case @".jpg":
                case @".jpeg":
                    return ImageFormat.Jpeg;
                case @".png":
                    return ImageFormat.Png;
                default:
                    throw new NotImplementedException();
            }
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (System.Drawing.Image)b;
        }

        #endregion

        #region "Membership User Methods"

        public Guid GetCurrentLoggedinUserId()
        {
            try
            {
                return WebUtil.GetSessionValue<Guid>(CommonConstants.UserIdWeb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetCurrentLoggedinUserName()
        {
            try
            {
                return WebUtil.GetSessionValue<string>(CommonConstants.UserNameWeb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Account Settings"

        public ActionResult UpdateUserProfileGet()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                    RegistrationModel model = new RegistrationModel
                    {
                        Id = user.Id,
                        UserId = new Guid(user.UserId.ToString()),
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        UserName = user.aspnet_Users.UserName,
                        Email = Membership.GetUser(new Guid(Convert.ToString(userId))).Email,
                        Mobile = user.Mobile,
                        Description = user.Description,
                        Website = user.Website,
                        //Photo = user.Photo,
                        userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                        UserTag = user.UserTag
                    };
                    StockModel stockModel = new StockModel
                    {
                        RegistrationModel = model
                    };
                    return PartialView("_UpdateProfile", stockModel);
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_UpdateProfile", new StockModel());
        }

        public ActionResult ChangePasswordGet()
        {
            try
            {
                return PartialView("_ChangePassword");
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_ChangePassword");
        }

        public ActionResult WatchListGet()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    WatchListModel = ProfileWatchListBind()
                };
                return PartialView("_ProfileWatchListFilter", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_ProfileWatchListFilter", new StockModel());
        }

        public ActionResult Settings(string url = "")
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
                    RegistrationModel model = new RegistrationModel
                    {
                        Id = user.Id,
                        UserId = new Guid(user.UserId.ToString()),
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        UserName = user.aspnet_Users.UserName,
                        Email = Membership.GetUser(new Guid(Convert.ToString(userId))).Email,
                        Mobile = user.Mobile,
                        Description = user.Description,
                        Website = user.Website,
                        //Photo = user.Photo,
                        userImage = GetUserImage(new Guid(user.UserId.ToString()), "100"),
                        UserTag = user.UserTag
                    };

                    StockModel stockModel = new StockModel
                    {
                        StockMenuList = Menu(),
                        LoggedInUser = SetLoggedInUser(),
                        NseStockDetail = NseStockDetail(),
                        BseStockDetail = BseStockDetail(),
                        RegistrationModel = model,
                        //IsStockMarketOpenClose = IsStockMarketOpenClose
                    };

                    if (!string.IsNullOrEmpty(url))
                    {
                        string url1 = EncryptDecryptClass.Decrypt(url, true);
                        if (url1 == "watchlists")
                        {
                            ViewData["setPage"] = Convert.ToString(url1).ToLower();
                        }
                        else if (url1 == "ChangePassword")
                        {
                            ViewData["setPage"] = Convert.ToString(url1).ToLower();
                        }
                    }
                    return View(stockModel);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }

        #endregion

        #region "Message Methods"

        public ActionResult UserListGet()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }
                //userList
                List<UserListModel> usersListModel = new List<UserListModel>();
                var userMessagesList = db.UserMessages.ToList();
                if (userMessagesList.Count() > 0)
                {
                    var userList = (userMessagesList.Where(x => x.MessageFrom == userId && x.MessageTo != userId).Select(x => x.MessageTo).Distinct())
                                    .Union(userMessagesList.Where(x => x.MessageTo == userId && x.MessageFrom != userId).Select(x => x.MessageFrom).Distinct()).ToList();
                    var registrationModel = db.RegistrationMasters.ToList();

                    usersListModel.AddRange(userList.Select(x => new UserListModel
                    {
                        UserId = x.Value,
                        Username = Membership.GetUser(new Guid(Convert.ToString(x.Value))).UserName,
                        Firstname = registrationModel.FirstOrDefault(y => y.UserId == x.Value).FirstName,
                        Lastname = registrationModel.FirstOrDefault(y => y.UserId == x.Value).LastName,
                        UserImage = GetUserImage(new Guid(x.Value.ToString()), "32"),
                    }));
                }

                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                    UserListModel = usersListModel
                };

                return View("Messages", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View("Messages", new StockModel());
        }
        public ActionResult MessageListByUser(Guid selecteduserId)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();

                //MessageListByUser
                List<MessageListModel> messageListModel = new List<MessageListModel>();
                var userMessagesList = db.UserMessages.ToList();
                if (userMessagesList.Count() > 0)
                {
                    var messageList = ((userMessagesList.Where(x => x.MessageTo == selecteduserId && x.MessageFrom == userId))
                                    .Union(userMessagesList.Where(x => x.MessageTo == userId && x.MessageFrom == selecteduserId)))
                                    .OrderBy(x => x.CreatedOn).ToList();

                    messageListModel.AddRange(messageList.Select(x => new MessageListModel
                    {
                        LoggedInUserId = userId,
                        MessageTo = new Guid(x.MessageTo.ToString()),
                        UserImageTo = GetUserImage(new Guid(x.MessageTo.ToString()), "32"),
                        UserImageFrom = GetUserImage(new Guid(x.MessageFrom.ToString()), "32"),
                        Message = x.Description,
                        CreatedDate = string.Format("{0:MMM dd, yyyy hh:mm tt}", x.CreatedOn)
                    }));
                }

                MessageModel messageModel = new MessageModel
                {
                    SelectedUser = selecteduserId,
                    MessageListModel = messageListModel
                };

                StockModel stockModel = new StockModel
                {
                    MessageModel = messageModel
                };

                return PartialView("_UserByMessageList", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_UserByMessageList", new StockModel());
        }

        public ActionResult ReplyMessage(Guid toUserId, string message)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }

                //Save Message
                UserMessage userMessage = new UserMessage
                {
                    MessageTo = toUserId,
                    MessageFrom = userId,
                    Description = message,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false
                };
                db.UserMessages.Add(userMessage);
                db.SaveChanges();

                //MessageListByUser
                List<MessageListModel> messageListModel = new List<MessageListModel>();
                var userMessagesList = db.UserMessages.ToList();
                if (userMessagesList.Count() > 0)
                {
                    var messageList = ((userMessagesList.Where(x => x.MessageTo == toUserId && x.MessageFrom == userId))
                                    .Union(userMessagesList.Where(x => x.MessageTo == userId && x.MessageFrom == toUserId)))
                                    .OrderBy(x => x.CreatedOn).ToList();

                    messageListModel.AddRange(messageList.Select(x => new MessageListModel
                    {
                        LoggedInUserId = userId,
                        MessageTo = new Guid(x.MessageTo.ToString()),
                        UserImageTo = GetUserImage(new Guid(x.MessageTo.ToString()), "32"),
                        UserImageFrom = GetUserImage(new Guid(x.MessageFrom.ToString()), "32"),
                        Message = x.Description,
                        CreatedDate = string.Format("{0:MMM dd, yyyy hh:mm tt}", x.CreatedOn)
                    }));
                }

                MessageModel messageModel = new MessageModel
                {
                    SelectedUser = toUserId,
                    MessageListModel = messageListModel
                };

                StockModel stockModel = new StockModel
                {
                    MessageModel = messageModel
                };

                return PartialView("_UserByMessageList", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_UserByMessageList", new StockModel());
        }

        public ActionResult GetMessageCounter()
        {
            int unReadMessageCount = 0;
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    var userMessagesList = db.UserMessages.ToList();
                    if (userMessagesList.Count() > 0)
                    {
                        unReadMessageCount = userMessagesList.Where(x => x.MessageTo == userId && x.IsRead == false).ToList().Count();
                    }
                    return Json(new { IsSuccess = true, count = unReadMessageCount }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false, count = unReadMessageCount }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetMessageRead()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId != Guid.Empty)
                {
                    var userMessagesList = db.UserMessages.ToList();
                    if (userMessagesList.Count() > 0)
                    {
                        var readMessage = userMessagesList.Where(x => x.MessageTo == userId && x.IsRead == false).ToList();

                        foreach (var item in readMessage)
                        {
                            var userMessage = db.UserMessages.Where(x => x.Id == item.Id).FirstOrDefault();
                            userMessage.IsRead = true;
                            db.SaveChanges();
                        }
                    }
                    return Json(new { IsSuccess = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Crop"

        public const int AvatarStoredWidth = 100;  // ToDo - Change the size of the stored avatar image
        public const int AvatarStoredHeight = 100; // ToDo - Change the size of the stored avatar image
        public const int AvatarScreenWidth = 400;  // ToDo - Change the value of the width of the image on the screen

        public const string TempFolder = "/Temp";
        public const string MapTempFolder = "~" + TempFolder;
        public const string AvatarPath = "/Media/UserImages/";

        public readonly string[] _imageFileExtensions = { ".jpg", ".png", ".gif", ".jpeg" };


        [HttpGet]
        public ActionResult GetUploadImageDialog()
        {
            try
            {
                return PartialView("_UploadImage");
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_UploadImage");
        }

        [HttpGet]
        public ActionResult _Upload()
        {
            return PartialView();
        }

        [ValidateAntiForgeryToken]
        public ActionResult _Upload(IEnumerable<HttpPostedFileBase> files)
        {
            if (files == null || !files.Any()) return Json(new { success = false, errorMessage = "No file uploaded." });
            var file = files.FirstOrDefault();  // get ONE only
            if (file == null || !IsImage(file)) return Json(new { success = false, errorMessage = "File is of wrong format." });
            if (file.ContentLength <= 0) return Json(new { success = false, errorMessage = "File cannot be zero length." });
            var webPath = GetTempSavedFilePath(file);
            return Json(new { success = true, fileName = webPath.Replace("/", "\\") }); // success
        }

        [HttpPost]
        public ActionResult Save(string t, string l, string h, string w, string fileName)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                // Calculate dimensions
                var top = Convert.ToInt32(t.Replace("-", "").Replace("px", ""));
                var left = Convert.ToInt32(l.Replace("-", "").Replace("px", ""));
                var height = Convert.ToInt32(h.Replace("-", "").Replace("px", ""));
                var width = Convert.ToInt32(w.Replace("-", "").Replace("px", ""));

                // Get file from temporary folder
                var fn = Path.Combine(Server.MapPath(MapTempFolder), Path.GetFileName(fileName));
                // ...get image and resize it, ...

                var img32 = new WebImage(fn);
                img32.Resize(width, height);
                img32.Crop(top, left, img32.Height - top - AvatarStoredHeight, img32.Width - left - AvatarStoredWidth);

                var img50 = new WebImage(fn);
                img50.Resize(width, height);
                img50.Crop(top, left, img50.Height - top - AvatarStoredHeight, img50.Width - left - AvatarStoredWidth);

                var img100 = new WebImage(fn);
                img100.Resize(width, height);
                img100.Crop(top, left, img100.Height - top - AvatarStoredHeight, img100.Width - left - AvatarStoredWidth);

                System.IO.File.Delete(fn);

                string extension = Path.GetExtension(img100.FileName);

                //100
                img100 = img100.Resize(CommonConstants.userImage100, CommonConstants.userImage100);
                //50
                img50 = img50.Resize(CommonConstants.userImage50, CommonConstants.userImage50);
                //32
                img32 = img32.Resize(CommonConstants.userImage32, CommonConstants.userImage32);

                string imagesFilePath = Path.Combine(HttpContext.Server.MapPath(AvatarPath + userId + ""));
                if (!Directory.Exists(imagesFilePath))
                {
                    Directory.CreateDirectory(imagesFilePath);
                }

                img32.Save(imagesFilePath + "/32" + extension);
                img50.Save(imagesFilePath + "/50" + extension);
                img100.Save(imagesFilePath + "/100" + extension);

                string userImage = GetUserImage(new Guid(userId.ToString()), "100");
                return Json(new { success = true, avatarFileLocation = userImage });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errorMessage = "Unable to upload file.\nERRORINFO: " + ex.Message });
            }
        }

        private bool IsImage(HttpPostedFileBase file)
        {
            if (file == null) return false;
            return file.ContentType.Contains("image") ||
                _imageFileExtensions.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        private string GetTempSavedFilePath(HttpPostedFileBase file)
        {
            // Define destination
            var serverPath = HttpContext.Server.MapPath(TempFolder);
            if (Directory.Exists(serverPath) == false)
            {
                Directory.CreateDirectory(serverPath);
            }

            // Generate unique file name
            var fileName = Path.GetFileName(file.FileName);
            fileName = SaveTemporaryAvatarFileImage(file, serverPath, fileName);

            // Clean up old files after every save
            CleanUpTempFolder(1);
            return Path.Combine(TempFolder, fileName);
        }

        private static string SaveTemporaryAvatarFileImage(HttpPostedFileBase file, string serverPath, string fileName)
        {
            var img = new WebImage(file.InputStream);
            var ratio = img.Height / (double)img.Width;
            img.Resize(AvatarScreenWidth, (int)(AvatarScreenWidth * ratio));

            var fullFileName = Path.Combine(serverPath, fileName);
            if (System.IO.File.Exists(fullFileName))
            {
                System.IO.File.Delete(fullFileName);
            }

            img.Save(fullFileName);
            return Path.GetFileName(img.FileName);
        }

        private void CleanUpTempFolder(int hoursOld)
        {
            try
            {
                var currentUtcNow = DateTime.UtcNow;
                var serverPath = HttpContext.Server.MapPath("/Temp");
                if (!Directory.Exists(serverPath)) return;
                var fileEntries = Directory.GetFiles(serverPath);
                foreach (var fileEntry in fileEntries)
                {
                    var fileCreationTime = System.IO.File.GetCreationTimeUtc(fileEntry);
                    var res = currentUtcNow - fileCreationTime;
                    if (res.TotalHours > hoursOld)
                    {
                        System.IO.File.Delete(fileEntry);
                    }
                }
            }
            catch
            {
                // Deliberately empty.
            }
        }


        #endregion

        #region "Report User Methods"

        [HttpGet]
        public ActionResult GetReportUserDialog(Guid toUser)
        {
            try
            {
                if (toUser == Guid.Empty)
                {
                    return PartialView("_ReportUser", new ReshareModel());
                }

                ReportUser reportUser = new ReportUser
                {
                    ToUser = toUser
                };
                return PartialView("_ReportUser", reportUser);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_ReshareDialog", new ReportUser());
        }

        public ActionResult ReportUser(Guid toUser, string comment)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                ReportUser reportUser = new ReportUser
                {
                    FromUser = userId,
                    ToUser = toUser,
                    Comments = comment,
                    CreatedOn = DateTime.Now,
                    IsActive = true
                };
                db.ReportUsers.Add(reportUser);
                db.SaveChanges();
                return Json(new { IsSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Help"

        public ActionResult HelpUser()
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }
                //questionList
                List<HelpQuestionModel> helpQuestionListModel = new List<HelpQuestionModel>();
                var helpQuestionList = db.HelpQuestions.Where(x => x.FromUser == userId).ToList();

                if (helpQuestionList.Count() > 0)
                {
                    helpQuestionListModel.AddRange(helpQuestionList.Select(x => new HelpQuestionModel
                    {
                        Id = x.Id,
                        FromUser = new Guid(x.FromUser.ToString()),
                        Description = x.Description,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        IsResponded = Convert.ToBoolean(x.IsResponded),
                        IsActive = Convert.ToBoolean(x.IsActive)
                    }));
                }

                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                    HelpQuestionListModel = helpQuestionListModel
                };

                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }

        public ActionResult AddQuestion()
        {
            try
            {
                return PartialView("_HelpAddQuestion");
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HelpAddQuestion", new StockModel());
        }

        public ActionResult SaveQuestion(string question)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }

                //Save Question
                HelpQuestion helpQuestion = new HelpQuestion
                {
                    FromUser = userId,
                    Description = question,
                    CreatedOn = DateTime.Now,
                    IsResponded = false,
                    IsActive = true
                };
                db.HelpQuestions.Add(helpQuestion);
                db.SaveChanges();

                //questionList
                List<HelpQuestionModel> helpQuestionListModel = new List<HelpQuestionModel>();
                var helpQuestionList = db.HelpQuestions.Where(x => x.FromUser == userId).ToList();

                if (helpQuestionList.Count() > 0)
                {
                    helpQuestionListModel.AddRange(helpQuestionList.Select(x => new HelpQuestionModel
                    {
                        Id = x.Id,
                        FromUser = new Guid(x.FromUser.ToString()),
                        Description = x.Description,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        IsResponded = Convert.ToBoolean(x.IsResponded),
                        IsActive = Convert.ToBoolean(x.IsActive)
                    }));
                }

                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                    HelpQuestionListModel = helpQuestionListModel
                };

                return PartialView("_HelpQuestion", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HelpQuestion", new StockModel());
        }

        public ActionResult AnswerListByQuestion(long questionId)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }

                //answerList
                List<HelpAnswerModel> helpAnswerListModel = new List<HelpAnswerModel>();
                var helpAnswerList = db.HelpAnswers.Where(x => x.HelpQuestionId == questionId).ToList();

                if (helpAnswerList.Count() > 0)
                {
                    helpAnswerListModel.AddRange(helpAnswerList.Select(x => new HelpAnswerModel
                    {
                        Id = x.Id,
                        HelpQuestionId = questionId,
                        FromUser = new Guid(x.FromUser.ToString()),
                        Description = x.Description,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        IsRead = Convert.ToBoolean(x.IsRead),
                        IsActive = Convert.ToBoolean(x.IsActive),
                        Username = Membership.GetUser(new Guid(x.FromUser.ToString())).UserName,
                        UserImage = GetUserImage(new Guid(x.FromUser.ToString()), CommonConstants.userImage32.ToString()),
                        LoggedInUserId = userId
                    }));
                }

                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                    HelpAnswerListModel = helpAnswerListModel
                };
                ViewBag.questionId = questionId;
                return PartialView("_HelpAnswerListByQuestion", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HelpAnswerListByQuestion", new StockModel());
        }

        public ActionResult SaveAnswer(long questionId, string answer)
        {
            try
            {
                Guid userId = GetCurrentLoggedinUserId();
                if (userId == Guid.Empty)
                {
                    return RedirectToAction("Index");
                }

                //Save Answer
                HelpAnswer helpAnswer = new HelpAnswer
                {
                    HelpQuestionId = questionId,
                    FromUser = userId,
                    Description = answer,
                    CreatedOn = DateTime.Now,
                    IsRead = false,
                    IsActive = true
                };
                db.HelpAnswers.Add(helpAnswer);
                db.SaveChanges();

                //answerList
                List<HelpAnswerModel> helpAnswerListModel = new List<HelpAnswerModel>();
                var helpAnswerList = db.HelpAnswers.Where(x => x.HelpQuestionId == questionId).ToList();

                if (helpAnswerList.Count() > 0)
                {
                    helpAnswerListModel.AddRange(helpAnswerList.Select(x => new HelpAnswerModel
                    {
                        Id = x.Id,
                        FromUser = new Guid(x.FromUser.ToString()),
                        Description = x.Description,
                        CreatedOn = Convert.ToDateTime(x.CreatedOn),
                        IsRead = Convert.ToBoolean(x.IsRead),
                        IsActive = Convert.ToBoolean(x.IsActive),
                        Username = Membership.GetUser(new Guid(x.FromUser.ToString())).UserName,
                        UserImage = GetUserImage(new Guid(x.FromUser.ToString()), CommonConstants.userImage32.ToString()),
                        LoggedInUserId = userId
                    }));
                }

                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                    HelpAnswerListModel = helpAnswerListModel
                };
                ViewBag.questionId = questionId;
                return PartialView("_HelpAnswerListByQuestion", stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return PartialView("_HelpAnswerListByQuestion", new StockModel());
        }

        #endregion

        public RegistrationModel GetRegistrationModelByUserId(Guid userId)
        {
            var user = db.RegistrationMasters.AsEnumerable().FirstOrDefault(x => x.UserId == userId);
            RegistrationModel model = new RegistrationModel
            {
                Id = user.Id,
                UserId = userId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.aspnet_Users.UserName,
                Mobile = user.Mobile,
                Description = user.Description,
                Website = user.Website,
                //Photo = user.Photo,
                userImage = GetUserImage(new Guid(userId.ToString()), "100"),
                UserTag = user.UserTag,
                CreatedOn = String.Format("{0:Y}", user.CreatedOn)
            };
            return model;
        }
        public string GetUserImage(Guid userId, string size)
        {
            string userImage = string.Empty;
            try
            {
                if (userId != Guid.Empty)
                {
                    string[] userImages = new string[0];
                    string path = Path.Combine(HttpContext.Server.MapPath("/Media/UserImages/" + userId)).Trim();
                    if (Directory.Exists(path))
                    {
                        userImages = Directory.GetFiles(Path.Combine(HttpContext.Server.MapPath("/Media/UserImages/" + userId + "")), size + "*.*");
                        if (userImages.Count() > 0)
                        {
                            userImage = ConfigurationManager.AppSettings["ImagePath"] + "/Media/UserImages/" + userId + "/" + Path.GetFileName(userImages[0]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return userImage;
        }

        public bool MarketOpenClose(string dateTime)
        {
            DateTime currentDateTime = ConvertUTCToIST(DateTime.Now);
            DateTime IsMarketOpenClose = ConvertUTCToIST(DateTime.Now);

            try
            {
                IsMarketOpenClose = !string.IsNullOrEmpty(dateTime)
                                                            ? DateTime.ParseExact(dateTime.Replace('T', ' ').Replace('Z', ' ').Replace('-', '/').Trim(), "yyyy/MM/dd H:mm:ss", CultureInfo.InvariantCulture)
                                                            : currentDateTime;
                if (IsMarketOpenClose.Date == currentDateTime.Date)
                {
                    if (currentDateTime.Hour > 9 && currentDateTime.Hour < 16)
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return false;
        }
        public bool UpdateStockHit(string symbol)
        {
            SymbolMaster symbolModel = db.SymbolMasters.FirstOrDefault(x => x.NSESymbol == symbol);
            if (symbolModel != null)
            {
                symbolModel.Hit = symbolModel.Hit + 1;
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public DataTable CreateStockChartTable()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("StockDate", typeof(string));
                dt.Columns.Add("StockTime", typeof(string));
                dt.Columns.Add("StockDateTime", typeof(string));
                dt.Columns.Add("Open", typeof(string));
                dt.Columns.Add("Close", typeof(string));
                dt.Columns.Add("High", typeof(string));
                dt.Columns.Add("Low", typeof(string));
                dt.Columns.Add("Volume", typeof(string));
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return dt;
        }

        public static int CountStringOccurrences(string text, string pattern)
        {
            // Loop through all instances of the string 'text'.
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }
            return count;
        }

        [HttpPost]
        public JsonResult UploadAttachedFile()
        {
            BlogModel blogModel = new BlogModel();
            try
            {
                if (WebUtil.GetSessionValue<Guid>(CommonConstants.UserIdWeb) == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please Login First." });
                }
                Guid userId = GetCurrentLoggedinUserId();

                string originalFileName = string.Empty;
                string extension = String.Empty;
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    HttpPostedFileBase attachment = fileContent;
                    Image originalImage = Image.FromStream(attachment.InputStream, true, true);
                    Image img = Image.FromStream(attachment.InputStream, true, true);

                    Size sz = new Size(CommonConstants.ShareIdeaAttachment, CommonConstants.ShareIdeaAttachment);
                    img = ResizeImage(img, sz);
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        originalFileName = Path.GetFileName(fileContent.FileName);
                        extension = Path.GetExtension(originalFileName);

                        string imagesFilePath = Path.Combine(HttpContext.Server.MapPath("/Media/Blog/" + userId));
                        if (!Directory.Exists(imagesFilePath))
                        {
                            Directory.CreateDirectory(imagesFilePath);
                        }
                        img.Save(imagesFilePath + "/100" + extension);
                        originalImage.Save(imagesFilePath + "/oi" + extension);
                        //string filePath = Path.Combine(imagesFilePath, newFileName + extension);
                        //fileContent.SaveAs(filePath);
                    }
                }
                blogModel.NewFileName = ConfigurationManager.AppSettings["ImagePath"] + "/Media/Blog/" + userId + "/100" + extension + "?" + Guid.NewGuid();
                blogModel.OriginalFilePath = ConfigurationManager.AppSettings["ImagePath"] + "/Media/Blog/" + userId + "/oi" + extension + "?" + Guid.NewGuid();
                blogModel.OriginalFileName = originalFileName;
                blogModel.EditorImage = "<img src='" + blogModel.NewFileName + "' alt='Image' style='width: 60px;height: 60px;float: right;'/>";
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }
            return Json(new { IsSuccess = true, html = blogModel });
        }

        [HttpPost]
        public JsonResult CommentUploadAttachedFile()
        {
            CommentModel commentModel = new CommentModel();
            try
            {
                if (WebUtil.GetSessionValue<Guid>(CommonConstants.UserIdWeb) == Guid.Empty)
                {
                    return Json(new { IsSuccess = false, html = "Please Login First." });
                }
                Guid userId = GetCurrentLoggedinUserId();

                string newFileName = string.Empty;
                string originalFileName = string.Empty;
                string extension = String.Empty;
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    HttpPostedFileBase attachment = fileContent;
                    Image originalImage = Image.FromStream(attachment.InputStream, true, true);
                    Image img = Image.FromStream(attachment.InputStream, true, true);

                    Size sz = new Size(CommonConstants.ShareIdeaAttachment, CommonConstants.ShareIdeaAttachment);
                    img = ResizeImage(img, sz);
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        newFileName = Guid.NewGuid().ToString();
                        originalFileName = Path.GetFileName(fileContent.FileName);
                        extension = Path.GetExtension(originalFileName);

                        string imagesFilePath = Path.Combine(HttpContext.Server.MapPath("/Media/Comments/" + userId));
                        if (!Directory.Exists(imagesFilePath))
                        {
                            Directory.CreateDirectory(imagesFilePath);
                        }
                        img.Save(imagesFilePath + "/100" + extension);
                        originalImage.Save(imagesFilePath + "/oi" + extension);
                        //string filePath = Path.Combine(imagesFilePath, newFileName + extension);
                        //fileContent.SaveAs(filePath);
                    }
                }
                commentModel.AttachFileName = ConfigurationManager.AppSettings["ImagePath"] + "/Media/Comments/" + userId + "/100" + extension + "?" + Guid.NewGuid();
                commentModel.OriginalFilePath = ConfigurationManager.AppSettings["ImagePath"] + "/Media/Comments/" + userId + "/oi" + extension + "?" + Guid.NewGuid();
                //commentModel.OriginalFileName = originalFileName;
                commentModel.CommentImage = "<img src='" + commentModel.AttachFileName + "' alt='Image' style='width: 60px;height: 60px;float: right;'/>";
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }
            return Json(new { IsSuccess = true, html = commentModel });
        }

        /// <summary>
        /// Returns one block of items
        /// </summary>
        /// <param name="BlockNumber">Starting from 1</param>
        /// <param name="BlockSize">Items count in a block</param>
        /// <returns></returns>
        public List<ShareIdeaBlogListModel> GetBlogItems(int BlockNumber, int BlockSize, string symbol, string selectedClass, int option = 0)
        {
            try
            {
                int startIndex = (BlockNumber - 1) * BlockSize;
                List<ShareIdeaBlogListModel> ShareIdeaBlogLists = new List<ShareIdeaBlogListModel>();
                if (selectedClass == "topIdea")
                {
                    ShareIdeaBlogLists = GetTopIdeasList(symbol, option);
                    if (ShareIdeaBlogLists.Count() <= 0)
                    {
                        ShareIdeaBlogLists = GetAllIdeasList(symbol);
                    }
                }
                else if (selectedClass == "allIdea")
                {
                    ShareIdeaBlogLists = GetAllIdeasList(symbol);
                }
                else if (selectedClass == "chartIdea")
                {
                    ShareIdeaBlogLists = GetChartIdeasList(symbol);
                }
                else if (selectedClass == "linksIdea")
                {
                    ShareIdeaBlogLists = GetLinksIdeasList(symbol);
                }
                else
                {
                    ShareIdeaBlogLists = new List<ShareIdeaBlogListModel>();
                }
                return ShareIdeaBlogLists.Skip(startIndex).Take(BlockSize).ToList();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return new List<ShareIdeaBlogListModel>();
        }

        /*public ActionResult GetData(int pageIndex, int pageSize,string symbol)
        {
            System.Threading.Thread.Sleep(4000);
            List<ShareIdeaBlogListModel> ShareIdeaBlogLists = new List<ShareIdeaBlogListModel>();
            ShareIdeaBlogLists = GetAllIdeasList(symbol);
            var query = (ShareIdeaBlogLists).Skip(pageIndex * pageSize).Take(pageSize);
            return Json(query.ToList(), JsonRequestBehavior.AllowGet);
        }
               Paging       */

        [HttpPost]
        public JsonResult InfinateScroll(int BlockNumber, string symbol, string selectedClass, int option = 0)
        {
            //////////////// THis line of code only for demo. Needs to be removed ///////////////
            //System.Threading.Thread.Sleep(3000);
            ////////////////////////////////////////////////////////////////////////////////////////
            try
            {
                int BlockSize = CommonConstants.BlockSize;
                List<ShareIdeaBlogListModel> ideaBlogs = GetBlogItems(BlockNumber, BlockSize, symbol, selectedClass, option);

                if (ideaBlogs.Count > 0)
                {
                    JsonModel jsonModel = new JsonModel();
                    jsonModel.NoMoreData = ideaBlogs.Count < BlockSize;
                    StockModel stockModel = new StockModel
                    {
                        LoggedInUser = SetLoggedInUser(),
                        ShareIdeaBlogModelList = ideaBlogs//ShareIdeaBlogList(setSymbol)
                    };
                    jsonModel.HTMLString = RenderRazorViewToString("_ShareIdeaBlogList", stockModel);
                    return Json(new { IsSuccess = true, html = jsonModel });
                }
                else
                {
                    return Json(new { IsSuccess = false });
                }
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return Json(new { IsSuccess = false });
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        private static byte[] ImageToByteArraybyMemoryStream(System.Drawing.Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }

        public static string TitleCase(String str)
        {
            string titleCase = str.ToLower();
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            titleCase = textInfo.ToTitleCase(titleCase);
            return titleCase;
        }

        public static string getRandomKey(int bytelength)
        {
            byte[] buff = new byte[bytelength];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(buff);
            StringBuilder sb = new StringBuilder(bytelength * 2);
            for (int i = 0; i < buff.Length; i++)
                sb.Append(string.Format("{0:X2}", buff[i]));
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult LoginCheck()
        {
            if (WebUtil.GetSessionValue<Guid>(CommonConstants.UserIdWeb) == Guid.Empty)
            {
                return Json(new { IsSuccess = false, html = "Please Login First." });
            }
            return Json(new { IsSuccess = true });
        }

        public ActionResult AboutUs()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                };
                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }
        public ActionResult Helps()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                };
                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }
        public ActionResult Blog()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                };
                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }
        public ActionResult Rules()
        {
            try
            {
                StockModel stockModel = new StockModel
                {
                    StockMenuList = Menu(),
                    LoggedInUser = SetLoggedInUser(),
                    NseStockDetail = NseStockDetail(),
                    BseStockDetail = BseStockDetail(),
                };
                return View(stockModel);
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return View(new StockModel());
        }
    }
}