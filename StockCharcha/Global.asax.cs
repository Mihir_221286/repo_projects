﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace StockCharcha
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static int pagesize = 10;
        public void user()
        {
            //Roles.CreateRole("Admin");
            Membership.CreateUser("Rahul", "111111", "rahul.b@diinsy.com");
            Roles.AddUserToRole("Rahul", "Admin");
        }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //user();
        }
    }
}
