﻿using StockCharcha.Utils.Utilities;
using System;
using System.Web.Mvc;
using StockCharcha.Utils;

namespace StockCharcha.Filters
{
    public class SessionExistFilter : ActionFilterAttribute
    {
        //// <summary>
        //// Filter checks session is exist or not
        //// Redirect to login page if session does not exist
        //// </summary>

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Guid sessionValue = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId);
            if (sessionValue != Guid.Empty)
            {
                return;
            }
            filterContext.Result = new RedirectResult("~/Admin/Index");
        }
    }
}