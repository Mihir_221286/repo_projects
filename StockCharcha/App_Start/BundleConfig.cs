﻿using System.Web.Optimization;

namespace StockCharcha
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            #region Common Css And Js

            bundles.Add(new StyleBundle("~/Content/CommonBootstrapCss").Include("~/Content/web/bootstrap/css/bootstrap.min.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Content/CommonCss").Include(
                                        "~/Content/web/css/style.css",
                                        //"~/Content/web/css/font-awesome.min.css",
                                        "~/Scripts/Web/Autocomplete/jquery-ui.css",
                                        "~/Content/web/css/loading.css",
                                        "~/Content/web/bootstrap/css/bootstrap-select.css",
                                        "~/Content/Menu/demo.css"));

            bundles.Add(new ScriptBundle("~/bundles/CommonJs").Include(
                                       "~/Scripts/jquery-1.10.2.min.js",
                                       "~/Scripts/bootstrap.js",
                                       "~/Content/web/bootstrap/js/bootstrap-select.js",
                                       "~/Content/Menu/modernizr.custom.js",
                                       "~/Scripts/jquery.loadTemplate-1.4.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/CommonCustomJs").Include(
                                      "~/Scripts/Shared/Common.js",
                                      "~/Scripts/Shared/User.js"));

            #endregion

            #region Stock Details

            bundles.Add(new StyleBundle("~/Content/DetailCss").Include("~/Content/web/css/style-scroll.css"));
            bundles.Add(new StyleBundle("~/Content/DetailPrettycss").Include("~/Content/prettyPhoto/css/prettyPhoto.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/DetailJs").Include(
                                                 "~/Content/prettyPhoto/js/jquery.prettyPhoto.js",
                                                 "~/Content/jquery-textcomplete-master/dist/jquery.textcomplete.js"));

            bundles.Add(new ScriptBundle("~/bundles/DetailCustomJs").Include(
                                      "~/Scripts/Shared/Common.js",
                                      "~/Scripts/Web/StockDetail.js",
                                      "~/Scripts/Shared/User.js"));

            #endregion

            BundleTable.EnableOptimizations = true;
        }
    }
}
