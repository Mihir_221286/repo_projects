﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace StockCharcha
{
    public class RouteConfig
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            // Get route data values
            var routeData = requestContext.RouteData;
            var action = routeData.GetRequiredString("action");
            var controller = routeData.GetRequiredString("controller");

            //modify your action name here

            //requestContext.RouteData.Values["action"] = actionName;
            //requestContext.RouteData.Values["controller"] = "SpecialController";

            return new MvcHandler(requestContext);
        }
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Dashboard",
                url: "dashboard",
                defaults: new { controller = "Home", action = "Dashboard" }
            );
            routes.MapRoute(
                name: "Timeline",
                url: "timeline",
                defaults: new { controller = "Home", action = "Timeline" }
                        );
            /*routes.MapRoute(
                name: "BSEStock",
                url: "BSE/{symbol}",
                defaults: new { controller = "Home", action = "Stock", symbol = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "NSEStock",
                url: "NSE/{symbol}",
                defaults: new { controller = "Home", action = "Stock" }
            );
            */
            routes.MapRoute(
                name: "symbol",
                url: "symbol/{symbol}",
                defaults: new { controller = "Home", action = "Stock", symbol = UrlParameter.Optional }
                );
            routes.MapRoute(
                name: "Search",
                url: "search/{searchString}",
                defaults: new { controller = "Home", action = "Search", searchString = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Profile",
                url: "updateprofile",
                defaults: new { controller = "Home", action = "UserProfile" }
            );
            routes.MapRoute(
                           name: "ViewProfile",
                           url: "profile/{username}",
                           defaults: new { controller = "Home", action = "ViewProfile", username = UrlParameter.Optional }
                       );
            routes.MapRoute(
                name: "ProfileWatchList",
                url: "watchlist",
                defaults: new { controller = "Home", action = "ProfileWatchList" }
            );
            routes.MapRoute(
                name: "Activate",
                url: "activate",
                defaults: new { controller = "Home", action = "Activatation" }
            );
            routes.MapRoute(
                name: "Followers",
                url: "followers/{username}",
                defaults: new { controller = "Home", action = "UserFollowersList", username = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Followed",
                url: "followed/{username}",
                defaults: new { controller = "Home", action = "UserFollowedList", username = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "DirectActivate",
                url: "directactivate/{email}/{activationCode}",
                defaults: new { controller = "Home", action = "DirectActivate", email = UrlParameter.Optional, activationCode = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "AboutUs",
                url: "aboutUs",
                defaults: new { controller = "Home", action = "AboutUs" }
            );
            routes.MapRoute(
                name: "Helps",
                url: "helps",
                defaults: new { controller = "Home", action = "Helps" }
            );
            routes.MapRoute(
                            name: "HelpUser",
                            url: "userhelp",
                            defaults: new { controller = "Home", action = "HelpUser" }
                        );
            routes.MapRoute(
                name: "Blog",
                url: "blog",
                defaults: new { controller = "Home", action = "Blog" }
            );
            routes.MapRoute(
                name: "Rules",
                url: "rules",
                defaults: new { controller = "Home", action = "Rules" }
            );
            routes.MapRoute(
                name: "SettingsByUrl",
                url: "settings/{url}",
                defaults: new { controller = "Home", action = "Settings", url = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "UserMessages",
                url: "messages",
                defaults: new { controller = "Home", action = "UserListGet" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
