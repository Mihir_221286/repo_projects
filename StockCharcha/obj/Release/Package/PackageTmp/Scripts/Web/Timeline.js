﻿var BlockNumber = 2;  //Infinate Scroll starts from second block
var pathname = window.location.pathname;

var Timeline = {
    GetTopIdeas: function () {
        Common.ShowSendingProgress();
        option = "Like";
        $.ajax({
            url: "/Home/GetTopTimelineIdeas",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ option: $("#streamPrefrence").val() }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#topIdea").addClass("active");
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetAllIdeas: function () {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetAllTimelineIdeas",
            type: "POST",
            //dataType: "json",
            //data: JSON.stringify({ symbol: symbol }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").addClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").removeClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetChartIdeas: function () {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetChartTimelineIdeas",
            type: "POST",
            //dataType: "json",
            //data: JSON.stringify({ symbol: symbol }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").addClass("active");
                    $("#linksIdea").removeClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetLinksIdeas: function () {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetLinksTimelineIdeas",
            type: "POST",
            //dataType: "json",
            //data: JSON.stringify({ symbol: symbol }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").addClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
};
$(document).ready(function () {
    
});