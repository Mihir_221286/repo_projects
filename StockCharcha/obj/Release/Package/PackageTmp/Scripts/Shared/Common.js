﻿var timer;
var attachFileComment;
var originalFileComment;
var pathname = window.location.pathname;
var moveLeft = 20;
var moveDown = 10;
var Common = {
    ShowSendingProgress: function () {
        //var modal = $('<div  />');
        //modal.addClass("modal");
        //modal.attr("id", "modalSending");
        //  $('body').attr("id", "modalSending");
        //  var loading = $("#modalSending.loading");
        //  loading.show();
        //var top = '415px';
        //var left = '560px';
        //loading.css({ top: top, left: left, color: '#ffffff' });
        $(".webLoading").fadeIn("slow");
    },
    StopProgress: function () {
        // $('body').removeAttr("id", "modalSending");
        //$("div.modal").hide();
        //  var loading = $(".loading");
        //  loading.hide();
        $(".webLoading").fadeOut("slow");
    },
    ScrollToElement: function (elm) {
        $("html, body").stop().animate({
            scrollLeft: elm.offset().left,
            scrollTop: elm.offset().top
        }, 500);
    },
    //Show Success
    ShowSuccess: function (message, isAutoHide, targetDom) {
        var notificationArea = targetDom || $(".notifications");
        notificationArea.loadTemplate($("#successTemplate"),
        {
            status: message
        });
        if (isAutoHide) {
            setTimeout(function () {
                notificationArea.children().fadeOut("slow", function () {
                    notificationArea.empty();
                });
            }, 4000);
        }
    },
    //Show Error
    ShowError: function (message, isAutoHide, targetDom) {
        var notificationArea = targetDom || $(".notifications");
        notificationArea.loadTemplate($("#errorTemplate"),
        {
            status: message
        });
    },

    SymbolMenu: function (symbol) {
        var list = symbol.split(",")
        if (list[1] == "NSE") {
            var nseSymbol = list[0];
            if (nseSymbol !== "" && nseSymbol !== "null") {
                var href = "/symbol/";
                var url = href + encodeURIComponent(nseSymbol);
                window.location.href = url;
            }
        } else if (list[1] == "BSE") {
            var bseSymbol = list[0];
            if (bseSymbol !== "" && bseSymbol !== "null") {
                var href = "/symbol/";
                var url = href + encodeURIComponent(bseSymbol);
                window.location.href = url;
            } else { }
        }
    },
    Logincheck: function () {
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    isLogin = true;
                } else {
                    //Common.ShowError("Please login first.", true);
                    //Common.ScrollToElement($(".notifications"));
                    isLogin = false;
                }
                return isLogin;
            }

        });
    },
    MenuHoverCall: function (symbol) {
        $(".mouseOverLoadingDiv").show();
        var list = symbol.split(",")
        $('.menuHoverData').html('');
        $.ajax({
            url: "/Home/MenuHover",
            type: "POST",
            //dataType: "json",
            //async: false,
            data: JSON.stringify({ symbol: list[0], type: list[1] }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    $('.menuHoverData').html(response.html).fadeIn("slow");
                } else { }
            },
            complete: function () {
                Common.StopProgress();
                $(".mouseOverLoadingDiv").hide();
            }
        });

    },
    WatchListAddMenu: function (item, type) {
        Common.ShowSendingProgress();
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    var symbol = item;
                    $.ajax({
                        url: "/Home/WatchListAdd",
                        type: "POST",
                        dataType: "json",
                        data: { item: item, type: type },
                        success: function (response) {
                            if (response.IsSuccess) {
                                //Common.ShowSuccess("Added to watchlist.", true);
                                StockDetail.WatchListFilter(item, "null");
                            } else {
                                Common.StopProgress();
                                alert(response.html);
                            }
                        }
                    });
                } else {
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                    Common.StopProgress();
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    HideBlogCommentError: function () {
        $('#commentsErrorSpan').text("");
        $('#commentsErrorDiv').hide();
    },
    ShowBlogCommentError: function (errorMessage) {
        $('#commentsErrorSpan').text(errorMessage);
        $('#commentsErrorDiv').show();
    },
    BtnBlogComment: function () {
        $("#blogComment").val('');
        Common.HideBlogCommentError();
    },

    //Find URL from entered text.
    Strip: function (html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return tmp.innerText.replace(urlRegex, function (url) {
            return '\n' + url;
        })
    },
    RenderHTML: function (text) {
        var rawText = StockDetail.Strip(text);
        var urlRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/ig
        //  /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

        return rawText.replace(urlRegex, function (url) {
            if ((url.indexOf(".jpg") > 0) || (url.indexOf(".png") > 0) || (url.indexOf(".gif") > 0)) {
                return '<img src="' + url + '">' + '<br/>';
            } else {
                if ((url.indexOf("http") >= 0)) {
                    IsLink = true;
                    return '<a href="' + url + '" target="_blank">' + url + '</a>';//+ '<br/>'
                } else {
                    url = url.toLowerCase().replace("www.", "http://www.");
                    IsLink = true;
                    return '<a href="' + url + '" target="_blank">' + url + '</a>';//+ '<br/>'
                }

            }
        })
    },
    //

    SendBlogComment: function (el) {
        Common.HideBlogCommentError();
        var blogId = $("#blogId").val();
        var blogComment = $("#blogComment").val();
        if (blogComment == null || blogComment === '') {
            Common.ShowBlogCommentError("Please Enter Comment.");
        }
        else {
            blogComment = Common.RenderHTML($("#blogComment").val());
            //Common.ShowSendingProgress();
            $.ajax({
                url: '/Home/BlogComment',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ comment: blogComment, blogId: blogId, attachFile: attachFileComment, originalFileComment: originalFileComment }),
                success: function (response) {
                    if (response.IsSuccess) {
                        //$('#CloseBlogCommentModal').trigger('click');
                        var test = el.closest('div[id="commentList"]');
                        var commentDiv = test.previousSibling;
                        if ($(test).length > 0) {
                            $(test).fadeOut(500, function () {
                                $(this).remove();
                            });
                        }
                        var url = $('#BlogCommentsModal').data('url');
                        var url1 = url + '?blogId=' + blogId;
                        $.get(url1, function (data) {
                            $($(data).fadeIn(500)).insertAfter(commentDiv);
                        });
                        var cmtCnt = parseInt($(test.previousSibling).children().find('ul li span').closest("#commentCounts").text()) + 1;
                        $(test.previousSibling).children().find('ul li span').closest("#commentCounts").text(cmtCnt);
                        attachFileComment = "";
                        originalFileComment = "";
                        //Common.ShowSuccess("Comment added successfully", true);
                        return;
                    } else {
                        Common.ShowBlogCommentError(response.html);
                    }
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
    },
    ReShareIdea: function () {
        Common.ShowSendingProgress();
        var blogId = $("#blogId").val();
        var activeButtonReshare = $('.tab li button.active').attr('id');
        var dataParam = { blogId: blogId, symbol: $("#StockSymbol").val(), activeButton: activeButtonReshare, option: $("#streamPrefrence").val(), description: $("#reshareDescription").val() };
        if (pathname.indexOf('timeline') > 0) {
            dataParam = { blogId: blogId, symbol: $("#StockSymbol").val(), activeButton: activeButtonReshare, option: $("#streamPrefrence").val(), description: $("#reshareDescription").val(), reshareTimeline: true }
        }
        $.ajax({
            url: "/Home/ReShareIdea",
            type: "POST",
            dataType: "json",
            data: dataParam,
            success: function (response) {
                if (response.IsSuccess) {
                    //Common.ShowSuccess("Idea ReShare Done Successfully.", true);
                    $('#CloseReshareDialog').trigger('click');
                    Common.CloseModal();
                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                }
                else {
                    Common.ShowError(response.html, true);
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    CloseModal: function () {
        $('#modal').modal('hide');
        $('body').removeClass('modal-open');
        $("#modal").removeClass("in");
        $(".modal-backdrop").remove();
    },
    LikeIdea: function (blogId, likeTable, ele) {
        Common.ShowSendingProgress();
        var element = ele;
        var active_button = $('.tab li button.active').attr('id');
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    $.ajax({
                        url: "/Home/LikeIdea",
                        type: "POST",
                        dataType: "json",
                        data: { blogId: blogId, symbol: $("#StockSymbol").val(), likeTable: likeTable, activeButton: active_button, option: $("#streamPrefrence").val() },
                        success: function (response) {
                            if (response.IsSuccess) {
                                if ($(element).hasClass('likeactive')) {
                                    $(element).removeClass('likeactive')
                                } else {
                                    $(element).addClass('likeactive')
                                }
                                $(ele.previousElementSibling).find('span').text(response.likeCnt)
                                //$("#likeCounts").html(response.likeCnt);
                                //Common.ShowSuccess(response.stringMsg, true);
                                //likeactive
                                //$("#shareIdeaList").html('');
                                //$("#shareIdeaList").html(response.html);
                            }
                            else {
                                Common.ShowError(response.html, true);
                            }
                        },
                        complete: function () {
                            Common.StopProgress();
                        }
                    });
                } else {
                    $("#btnSignIn").trigger('click');
                }
            },
            complete: function () {
                Common.StopProgress();
            }

        });


    },
    btnLikeListClick: function (el) {
        var $this = el;
        var blogId = el.id;
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    Common.BtnBlogComment();
                    var url = $('#LikeListModal').data('url');
                    var url1 = url + '?blogId=' + blogId;
                    $.get(url1, function (data) {
                        $('#LikeListModalContainer').html(data);
                        $('#LikeListModal').removeClass('hide');
                        $('#LikeListModal').modal('show');
                    });
                } else {
                    $("#btnSignIn").trigger('click');
                }
            }, complete: function () { }
        });
    },
    RemoveIdea: function (blogId) {
        Common.ShowSendingProgress();
        var active_button = $('.tab li button.active').attr('id');
        var result = confirm("Are you sure, you Want to delete?");
        if (result) {
            //Logic to delete the item
            $.ajax({
                url: "/Home/RemoveIdea",
                type: "POST",
                dataType: "json",
                data: { blogId: blogId, symbol: $("#StockSymbol").val(), activeButton: active_button, option: $("#streamPrefrence").val() },
                success: function (response) {
                    if (response.IsSuccess) {
                        //Common.ShowSuccess(response.stringMsg, true);
                        $("#shareIdeaList").html('');
                        $("#shareIdeaList").html(response.html);
                    }
                    else {
                        Common.ShowError(response.html, true);
                    }
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
        Common.StopProgress();
    },
    ShareIdeaLimit: function () {
        $("textarea[data-limit-input]").keyup(function (e) {
            var $this = $(this),
                charLength = $this.val().length,
                charLimit = $this.attr("data-limit-input");
            // Displays count
            $this.next("span").html(charLength + " of " + charLimit + " characters used");
            // Alert when max is reached
            if ($this.val().length > charLimit) {
                $this.next("span").html("<strong>You may only have up to " + charLimit + " characters.</strong>");
            }
        });
        $('#ckEditor').on('change', function () {
            var elem = document.getElementById('ckEditor');
            var $this = $(this),
                charLength = $this.val().length,
                charLimit = $this.attr("data-limit-input");
            if (charLength > charLimit) {
                elem.value = elem.value.substring(0, charLimit);
                alert("Your charecter limit is " + charLimit);
            }

        });
        $("textarea[data-limit-input]").keydown(function (e) {
            var $this = $(this),
                charLength = $this.val().length,
                charLimit = $this.attr("data-limit-input");

            if ($this.val().length > charLimit && e.keyCode !== 8 && e.keyCode !== 46) {
                return false;
            }
        });
    },
    NseBseFilter: function () {
        $.get('/Home/NseBseTicker',
        function (data) {
            $("#NseBse").html(data);
        });
    },
    ShareIdeaToggleReset: function (el) {
        if ($(el).hasClass('active')) {
            $(el).removeClass('active')
            $("#ckEditor").focus();
        } else {
            $("#drpTrend").children().removeClass('active');
            $(el).addClass('active');
        }


    },
    btnCommentClick: function (el) {
        var $this = el;
        $(el).hide();
        $(el.nextElementSibling).show()
        var blogId = el.id;
        var test = $this.closest('div[class="tabrowftr"]');
        if ($(test).next().closest('div[id="commentList"]').length > 0) {
            $(test).next().closest('div[id="commentList"]').fadeOut(500, function () {
                //$(el).remove();
                $("div").remove("#commentList");
                $($this).next().hide();
                $($this).show();
                return false;
            });
        }
        else {
            $.ajax({
                type: "POST",
                url: '/Home/LoginCheck',
                success: function (result) {
                    if (result.IsSuccess) {
                        Common.BtnBlogComment();
                        var url = $('#BlogCommentsModal').data('url');
                        var url1 = url + '?blogId=' + blogId;
                        $.get(url1, function (data) {
                            $($(data).fadeIn(500)).insertAfter(test);
                            //$('.fbcommentbox').next('p').remove();
                            //$('#BlogCommentsModalContainer').html(data);
                            //$('#BlogCommentsModal').removeClass('hide');
                            //$('#BlogCommentsModal').modal('show');
                        });
                    } else {
                        $("#btnSignIn").trigger('click');
                        //Common.ShowError("Please login first.", true);
                        //Common.ScrollToElement($(".notifications"));
                    }
                }, complete: function () {
                    setTimeout(function () {
                        $($this).next().hide();
                        $($this).show();
                    }, 2000);
                }
            });
        }
    },
    btnReshareClick: function (el) {
        var $this = el;
        var blogId = el.id;
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    Common.BtnBlogComment();
                    var url = $('#ReshareCommentsModal').data('url');
                    var url1 = url + '?blogId=' + blogId;
                    $.get(url1, function (data) {
                        $('#ReshareModalContainer').html(data);
                        $('#ReshareCommentsModal').removeClass('hide');
                        $('#ReshareCommentsModal').modal('show');
                    });
                } else {
                    $("#btnSignIn").trigger('click');
                    //Common.ShowError("Please login first.", true);
                    //Common.ScrollToElement($(".notifications"));
                }
            }, complete: function () { }
        });
    },
    CommentUploadAttachment: function (e) {
        //$('.fileAttachComment').on('change', function (e) {
        var files = e.files;
        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }

                $.ajax({
                    type: "POST",
                    url: '/Home/CommentUploadAttachedFile',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        if (result.IsSuccess) {
                            attachFileComment = result.html.AttachFileName;
                            originalFileComment = result.html.OriginalFilePath;
                            $('#commentImage').html(result.html.CommentImage)
                            $('#pinAttach').css("display", "none");
                        } else {
                            $('.fileAttachComment').replaceWith($(".fileAttachComment").clone(true));
                            Common.ShowError("Please login first.", true);
                            Common.ScrollToElement($(".notifications"));
                        }
                    },
                    error: function (xhr, status, p3, p4) {
                        var err = "Error " + " " + status + " " + p3 + " " + p4;
                        if (xhr.responseText && xhr.responseText[0] == "{")
                            err = JSON.parse(xhr.responseText).Message;
                        console.log(err);
                    }
                });
            } else {
                alert("This browser doesn't support HTML5 file uploads!");
            }
        }
        //});
    },
    CommentRemoveAttachment: function () {
        $('#commentUploadFile').replaceWith($("#commentUploadFile").clone(true));
        $('#commentImage').html('');
        attachFileComment = "";
        originalFileComment = "";
        $('#pinAttach').removeAttr('style').css("float", "left");
    },
    CommonMenu: function (el) {
        Common.SymbolMenu($(el).attr('id'));
    },
    CommonMenuHoverCall: function (symbol) {
        //$(".mouseOverLoadingDiv").show();
        $('.menuHoverGlobal').html('');
        var list = symbol.split(",")

        $.ajax({
            url: "/Home/MenuHover",
            type: "POST",
            //dataType: "json",
            //async: false,
            data: JSON.stringify({ symbol: list[0], type: list[1] }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    $('.menuHoverGlobal').html(response.html).fadeIn("slow");
                } else { }
            },
            complete: function () {
                Common.StopProgress();
                //$(".mouseOverLoadingDiv").hide();
            }
        });

    },
    CommonMenuHover: function (el, e) {
        var $this = $(el);
        var imgLeft = $this.offset().left - $this.width();
        var imgTop = $this.offset().top - $this.height();
        var ee = e;
        $('.menuHoverGlobal').html('');
        $this.parent().children().next('ul').css({ 'display': "none" });
        clearTimeout(timer);
        timer = setTimeout(function () {
            $this.parent().children().next('ul').removeAttr("style");
            Common.CommonMenuHoverCall($this.attr('id'));
            var ofSet = {
                left: ee.pageX - 35,
                top: ee.pageY + 20
            };
            $('div#test123').show().offset(ofSet);
        }, 1000);
    },
    CommonMenuHoverComplete: function () {
        var $this1 = $(this);
        clearTimeout(timer);
        //$("div#test123").css('display', 'none');
    },
    UploadImageClick: function (el) {
        var $this = el;
        var blogId = el.id;
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    Common.BtnBlogComment();
                    var url = $('#uploadImageModal').data('url');
                    //var url1 = url + '?blogId=' + blogId;
                    $.get(url, function (data) {
                        $('#uploadImageContainer').html(data);
                        $('#uploadImageModal').removeClass('hide');
                        $('#uploadImageModal').modal('show');
                    });
                } else {
                    $("#btnSignIn").trigger('click');
                    //Common.ShowError("Please login first.", true);
                    //Common.ScrollToElement($(".notifications"));
                }
            }, complete: function () { }
        });
    },
    btnHeaderShareIdeaClick: function (el) {
        Common.ShowSendingProgress();
        var $this = el;
        var blogId = el.id;
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    Common.BtnBlogComment();
                    var url = $('#HeadershareIdeaModal').data('url');
                    //var url1 = url + '?blogId=' + blogId;
                    $.get(url, function (data) {
                        $('#HeadershareIdeaModalContainer').html(data);
                        $('#HeadershareIdeaModal').removeClass('hide');
                        $('#HeadershareIdeaModal').modal('show');
                        Common.StopProgress();
                    });
                } else {
                    $("#btnSignIn").trigger('click');
                    Common.StopProgress();
                }
            }, complete: function () { }
        });

    },
    HeaderShareIdeaCancel: function () {
        $('#CancelHeadershareIdeaDialog').trigger('click');
    },
    LikeListUserFollow: function (ele) {
        Common.ShowSendingProgress();
        var element = ele;
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    $.ajax({
                        url: "/Home/UserFollow",
                        type: "POST",
                        dataType: "json",
                        data: { followBy: $('.userFollow').attr('id') },
                        success: function (response) {
                            if (response.IsSuccess) {
                                if ($(element).hasClass("btn-warning")) {
                                    $(element).removeClass("btn-warning").addClass("btn-primary");
                                    $(element).html('Follow');
                                } else {
                                    $(element).removeClass("btn-primary").addClass("btn-warning");
                                    $(element).html('Following');
                                }
                                //Common.ShowSuccess(response.html, true);
                            } else {
                                //Common.StopProgress();
                                Common.ShowError(response.html, true);
                                Common.ScrollToElement($(".notifications"));
                            }
                        },
                        complete: function () {
                            Common.StopProgress();
                        }
                    });
                } else {
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            }

        });
    },
    GenrateNotification: function generate(type, text) {
        var n = noty({
            text: text,
            type: type,
            dismissQueue: false, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            layout: 'topRight',
            closeWith: ['click'],
            theme: 'relax',
            maxVisible: 1,
            timeout: 10000,
            animation: {
                open: 'animated bounceInLeft',
                close: 'animated bounceOutLeft',
                easing: 'swing',
                speed: 500
            },
            callback: {
                onShow: function () { },
                afterShow: function () { },
                onClose: function () { },
                afterClose: function () {
                    $.get('/Home/SetMessageRead', function (data) { if (data.IsSuccess) { } });
                },
                onCloseClick: function () { },
            }
        });
        console.log('html: ' + n);
    },
    GetMessageCount: function () {
        $.get('/Home/GetMessageCounter',
        function (data) {
            if (data.IsSuccess) {
                if (data.count > 0) {
                    Common.GenrateNotification('success', "New Message Arrived...<a href='/messages'>Click here</a>")
                }
            }
        });
    }
};
$(document).ready(function () {
    $("#tags").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Home/SymbolList",
                type: "POST",
                dataType: "json",
                data: { term: request.term, searchOption: $("#searchOption").val() },
                success: function (data) {
                    //response($.map(data, function (item) {
                    //    return { label: item.CompanyName, value: item.NseSymbol + "," + item.BseSymbol };
                    //}));
                    response(data);
                }
            });
        },
        minLength: 1,
        focus: function (event, ui) {
            // Set value in text box
            if (ui.item.SearchType === 2) {
                //$("#tags").val(ui.item.UserName);
            } else {
                //$("#tags").val(ui.item.CompanyName);
            }
            return false;
        },
        select: function (event, ui) {
            if (ui.item.SearchType === 2) {
                $.post('/Home/SetSymbolSession',
                    { key: "UserSearch", value: ui.item.UserId }, function (data) {
                        if (data.IsSuccess) {
                            window.location.href = '/Profile/' + ui.item.UserName;
                        }
                    });
                return false;
            }
            if (ui.item.SearchType === 3) {
                window.location.href = '/Search/' + ui.item.SearchedValue;
                return false;
            }
            var nseSymbol = ui.item.NseSymbol;
            var bseSymbol = ui.item.BseSymbol;
            if (nseSymbol !== "" && nseSymbol !== "null") {
                window.location.href = '/symbol/' + nseSymbol;
            }
            else if (bseSymbol !== "" && bseSymbol !== "null") {
                window.location.href = '/symbol/' + bseSymbol;
            }
            return false;
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {
        var link = "";
        if (item.NseSymbol !== null && item.NseSymbol !== "") {
            link = item.CompanyName;
        } else if (item.BseSymbol !== null && item.BseSymbol !== "") {
            link = item.CompanyName;
        } else if (item.SearchType === 2) {
            link = item.UserName;
        }
        else if (item.SearchType === 3) {
            link = "<span style='text-align:center;'><b style='text-align:center;display:block;width:100%'>SEE MORE</b></span>";
        }
        return $("<li class='ui-menu-item-explore' tabindex='-1'>")
            .append("<a href ='javascript:void(0);'>" + link + "</a>")
            .append("</li>")
            .appendTo(ul);
    };
    $('.menuClick').click(function (evt) {
        evt.preventDefault();
        Common.SymbolMenu($(this).attr('id'));
    });
    $('.menuClick').mouseover(function (api) {
        var $this = $(this);
        $('.menuHoverData').html('');
        $this.parent().children().next('ul').css({ 'display': "none" });
        clearTimeout(timer);
        timer = setTimeout(function () {
            $this.parent().children().next('ul').removeAttr("style");
            //var src = api.originalEvent.srcElement;
            Common.MenuHoverCall($this.attr('id'));
        }, 1000);
    }).mouseleave(function () {
        var $this1 = $(this);
        clearTimeout(timer);
    });
    Common.ShareIdeaLimit();
    setInterval(function () {
        Common.NseBseFilter();
    }, 10000);

    $('#tags').keypress(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            window.location.href = '/search/' + $(this).val();
            return false;
        }
    });

    setInterval(function () {
        Common.GetMessageCount();
    }, 10000);

    ///////

    $('#test123').mouseover(function (api) {
        var $this = $(this);
        $this.css({ 'display': "block" });
    }).mouseleave(function () {
        var $this1 = $(this);
        clearTimeout(timer);
        $this1.css({ 'display': "none" });
        $('.menuHoverGlobal').html('');
    });

    //////
    //Common.CommentUploadAttachment();
});
$(window).load(function () {
    // Animate loader off screen
    $(".webLoading").fadeOut("slow");
    //$(".flexMenu-viewMore").remove(); //Menu [...] dropdown removed.
});