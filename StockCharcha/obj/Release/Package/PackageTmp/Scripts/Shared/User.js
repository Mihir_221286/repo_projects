﻿var registeredEmail = null;
// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

var User = {
    IsValidEmailAddress: function (emialAddress) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
        return re.test(emialAddress);
    },
    HideRegistrationError: function () {
        $('#registerErrorSpan').text("");
        $('#registerErrorDiv').hide();
    },
    ShowRegistrationError: function (errorMessage) {
        $('#registerErrorSpan').text(errorMessage);
        $('#registerErrorDiv').show();
    },
    HideLoginError: function () {
        $('#loginErrorSpan').text("");
        $('#loginErrorDiv').hide();
    },
    ShowLoginError: function (errorMessage) {
        $('#loginErrorSpan').text(errorMessage);
        $('#loginErrorDiv').show();
    },
    HideResetPasswordError: function () {
        $('#forgotPasswordErrorSpan').text("");
        $('#forgotPasswordErrorDiv').hide();
    },
    ShowResetPasswordError: function (errorMessage) {
        $('#forgotPasswordErrorSpan').text(errorMessage);
        $('#forgotPasswordErrorDiv').show();
    },
    HideAccountVerificationError: function () {
        $('#accountVarificationErrorSpan').text("");
        $('#accountVarificationErrorDiv').hide();
    },
    ShowAccountVerificationError: function (errorMessage) {
        $('#accountVarificationErrorSpan').text(errorMessage);
        $('#accountVarificationErrorDiv').show();
    },
    // User registration
    UserRegister: function () {
        User.HideRegistrationError();
        var firstName = $("#FirstName").val();
        var lastName = $("#LastName").val();
        var userName = $("#UserName").val();
        var email = $("#Email").val();
        var password = $("#Password").val();
        var confirmPassword = $("#ConfirmPassword").val();

        if (firstName == null || firstName === '') {
            User.ShowRegistrationError("Please Enter Your First Name.");
        }
        else if (lastName == null || lastName === '') {
            User.ShowRegistrationError("Please Enter Your Last Name.");
        }
        else if (userName == null || userName === '') {
            User.ShowRegistrationError("Please Enter Your UserName.");
        }
        else if (email == null || email === '') {
            User.ShowRegistrationError("Please Enter Your Email Address.");
        }
        else if (!User.IsValidEmailAddress(email)) {
            User.ShowRegistrationError("Please Enter Your Valid Email Address.");
        }
        else if (password == null || password === '') {
            User.ShowRegistrationError("Please Enter Your Password.");
        }
        else if (password !== '' && password.length < 6) {
            User.ShowRegistrationError("The password must be at least 6 characters long.");
        }
        else if (confirmPassword == null || confirmPassword === '') {
            User.ShowRegistrationError("Please Enter Your Confirm Password.");
        }
        else if (confirmPassword !== '' && confirmPassword.length < 6) {
            User.ShowRegistrationError("The Confirm Password must be at least 6 characters long.");
        }
        else if (password !== confirmPassword) {
            User.ShowRegistrationError("The Password And Confirm Password not matched.");
        }
        else {
            Common.ShowSendingProgress();
            $.ajax({
                url: '/Home/Registration',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ firstName: firstName, lastName: lastName, userName: userName, email: email, password: password, confirmPassword: confirmPassword }),
                success: function (response) {
                    if (response.IsSuccess) {
                        registeredEmail = response.html;
                        //$('#signup').fadeToggle("slow");
                        //setTimeout(function () {
                        //    $('#signupSuccess').fadeToggle("slow");
                        //}, 610);
                        $("#CloseRegistrationModal").trigger('click');
                        Common.ShowSuccess("Successful : Check your Email for activation code.", true);
                        window.location = '/Activate';
                    } else {
                        User.ShowRegistrationError(response.html);
                    }
                },
                error: function (response) {
                    alert(response.html);
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
    },

    //Activate Account
    Activate: function () {
        User.HideAccountVerificationError();
        var activationCode = $("#activationCode").val();
        var emailActivate = $("#txtEmailActivate").val();

        if (emailActivate == null || emailActivate === '') {
            User.ShowAccountVerificationError("Please Enter Your Email Address.");
        }
        else if (activationCode == null || activationCode === '') {
            User.ShowAccountVerificationError("Please Enter Verification Code.");
        }

        else if (!User.IsValidEmailAddress(emailActivate)) {
            User.ShowAccountVerificationError("Please Enter Your Valid Email Address.");
        }
        else {
            Common.ShowSendingProgress();
            $.ajax({
                url: '/Home/Activate',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ Email: emailActivate, ActivationCode: activationCode }),
                success: function (response) {
                    if (response != null) {
                        //Login On Success Activation
                        if (response.IsSuccess) {
                            Common.ShowSuccess(response.html, true);
                            window.location = '/Profile';
                        } else {
                            //Show Message on Failure
                            User.ShowAccountVerificationError(response.message);
                        }
                    }
                },
                error: function (data) {
                    alert(data);
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
    },

    // User login
    UserLogin: function () {
        User.HideLoginError();
        var email = $("#email").val();
        var password = $("#pwd").val();
        if (email == null || email === '') {
            User.ShowLoginError("Please Enter Email Address.");
        }
        else if (!User.IsValidEmailAddress(email)) {
            User.ShowLoginError("Please Enter Valid Email Address.");
        }
        else if (password == null || password === '') {
            User.ShowLoginError("Please Enter Password.");
        }
        else if (password !== '' && password.length < 6) {
            User.ShowLoginError("The password must be at least 6 characters long.");
        }
        else {
            var changePasswordEncrypt = "DMYSp6l1GbjX1gPuaZEsPCK7xw%3d%3d";
            $.ajax({
                url: '/Home/Login',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ email: email, password: password }),
                beforeSend: function () {
                    Common.ShowSendingProgress();
                },
                complete: function () {
                    Common.StopProgress();
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        if (response.syspass) {
                            window.location = "/Settings/" + changePasswordEncrypt;
                        } else {
                            var pathname = window.location.pathname;
                            User.UserLoggedIn();
                            //$.fancybox.close();
                            $('#CloseLoginModal').trigger('click');
                            if ((pathname.indexOf('/Index') > 0 || pathname.indexOf('/') >= 0) && (pathname.indexOf('symbol') < 0 )) {
                                window.location = "/Timeline";
                            }
                            if (pathname.indexOf('symbol') > 0 ) {
                                /*var NSE_BSE = "";
                                if (pathname.indexOf('NSE') > 0) { NSE_BSE = "NSE"; }
                                else { NSE_BSE = "BSE"; }*/
                                StockDetail.stockDetailData($("#hfDefaultSymbol").val()); //$("#StockSymbol").val(), NSE_BSE
                                StockDetail.GetAllIdeas($("#hfDefaultSymbol").val()); //$("#StockSymbol").val()
                                StockDetail.WatchListFilter($("#hfDefaultSymbol").val(), "null");  //$("#StockSymbol").val()
                                StockDetail.RecentlyViewedFilter();
                                StockDetail.IsWatchCheck($("#hfDefaultSymbol").val(), $("#hfDefaultExchange").val()); //$("#StockSymbol").val(), NSE_BSE
                                $("#btnAddToWatchList").fadeIn("2000");
                            }
                            //window.location.reload();
                            //return;
                        }
                    } else {
                        User.ShowLoginError(response.html);
                    }
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
    },

    // User logout
    UserLogout: function () {
        Common.ShowSendingProgress();
        $.ajax({
            url: '/Home/LogOut',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    User.UserLoggedIn();
                    //StockDetail.WatchListFilter();
                    window.location = "/Home/Index";
                    Common.StopProgress();
                    return;
                }
            }
        });
    },

    UserLoggedIn: function () {
        $.ajax({
            url: '/Home/LoggedInUser',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    $("#LoggedInUser").html('');
                    $("#LoggedInUser").html(response.html);
                    return;
                }
            }
        });
    },
    //Reset Password
    ResetPassword: function () {
        User.HideResetPasswordError();
        var email = $("#emailforgotPassword").val();
        if (email == null || email === '') {
            User.ShowResetPasswordError("Please Enter Email Address.");
        }
        else if (!User.IsValidEmailAddress(email)) {
            User.ShowResetPasswordError("Please Enter Valid Email Address.");
        }
        else {
            Common.ShowSendingProgress();
            $.ajax({
                url: '/Home/ResetPassword',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ email: email }),
                async: false,
                success: function (response) {
                    if (response.IsSuccess) {
                        setTimeout(function () {
                            $('#forgotPassword').fadeToggle("slow");
                            setTimeout(function () {
                                $('#headerTextLogin').html('');
                                $('#headerTextLogin').html('Login');
                                User.ClearLoginInfo();
                                User.HideLoginError();
                                User.HideResetPasswordError();
                                $('#login').fadeToggle("slow");
                            }, 600);
                        }, 1000);

                    } else {
                        User.ShowResetPasswordError(response.html);
                    }
                },
                error: function (response) {
                    alert(response.html);
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
    },

    //Reset Password
    ChangePassword: function () {
        User.HideAccountVerificationError();
        var emailActivate = $("#txtEmailActivate").val();
        var currentPassword = $("#txtCurrentPassword").val();
        var newPassword = $("#txtNewPassword").val();

        if (emailActivate == null || emailActivate === '') {
            User.ShowAccountVerificationError("Please Enter Your Email Address.");
        }
        else if (!User.IsValidEmailAddress(emailActivate)) {
            User.ShowAccountVerificationError("Please Enter Your Valid Email Address.");
        }
        else if (currentPassword == null || currentPassword === '') {
            User.ShowAccountVerificationError("Please Enter Current Password.");
        }
        else if (newPassword == null || newPassword === '') {
            User.ShowAccountVerificationError("Please Enter New Password.");
        }
        else if (newPassword !== '' && newPassword.length < 6) {
            User.ShowAccountVerificationError("The new password must be at least 6 characters long.");
        }
        else {
            Common.ShowSendingProgress();
            $.ajax({
                url: '/Home/ChangePasswordConfirm',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ email: emailActivate, currentPassword: currentPassword, newPassword: newPassword }),
                async: false,
                success: function (response) {
                    if (response.IsSuccess) {
                        Common.ShowSuccess(response.html, true);
                        $("#txtEmailActivate").val('');
                        $("#txtCurrentPassword").val('');
                        $("#txtNewPassword").val('');
                        //setTimeout(function () {
                        //    window.location = '/Home/Index'
                        //}, 1000);
                    } else {
                        User.ShowResetPasswordError(response.html);
                    }
                },
                error: function (response) {
                    alert(response.html);
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
    },

    ClearRegistrationInfo: function () {
        $("#FirstName").val('');
        $("#LastName").val('');
        $("#UserName").val('');
        $("#Email").val('');
        $("#Password").val('');
        $("#ConfirmPassword").val('');
    },
    BtnJoin: function () {
        User.ClearRegistrationInfo();
        User.HideRegistrationError();
        $('#signupSuccess').hide();
        $('#signup').show();
    },
    ClearLoginInfo: function () {
        $("#email").val('');
        $("#pwd").val('');
        $("#emailforgotPassword").val('');
    },
    BtnSignIn: function () {
        User.ClearLoginInfo();
        $('#headerTextLogin').html('Login');
        User.HideResetPasswordError();
        $('#forgotPassword').hide();
        $('#login').show();
    },
    ForgotPwd: function () {
        $('#login').fadeToggle("slow");
        $('#headerTextLogin').html('');
        $('#headerTextLogin').html('Forgot Password');
        User.ClearLoginInfo();
        User.HideLoginError();
        User.HideResetPasswordError();
        setTimeout(function () {
            $('#forgotPassword').fadeToggle("slow");
        }, 610);
    },
    BtnBack: function () {
        $('#forgotPassword').fadeToggle("slow");
        $('#headerTextLogin').html('');
        $('#headerTextLogin').html('Login');
        User.ClearLoginInfo();
        User.HideLoginError();
        User.HideResetPasswordError();
        setTimeout(function () {
            $('#login').fadeToggle("slow");
        }, 610);
    },
    HideUpdateUserError: function () {
        $('#updateUserErrorSpan').text("");
        $('#updateUserErrorDiv').hide();
    },
    ShowUpdateUserError: function (errorMessage) {
        $('#updateUserErrorSpan').text(errorMessage);
        $('#updateUserErrorDiv').show();
    },

    // start phone mobile end --- By 5339
    IsInteger: function (s) {
        var i;
        for (i = 0; i < s.length; i++) {
            // Check that current character is number.
            var c = s.charAt(i);
            if (((c < "0") || (c > "9"))) return false;
        }
        // All characters are numbers.
        return true;
    },
    Trim: function (s) {
        var i;
        var returnString = "";
        // Search through string's characters one by one.
        // If character is not a whitespace, append to returnString.
        for (i = 0; i < s.length; i++) {
            // Check that current character isn't whitespace.
            var c = s.charAt(i);
            if (c !== " ") returnString += c;
        }
        return returnString;
    },
    StripCharsInBag: function (s, bag) {
        var i;
        var returnString = "";
        // Search through string's characters one by one.
        // If character is not in bag, append to returnString.
        for (i = 0; i < s.length; i++) {
            // Check that current character isn't whitespace.
            var c = s.charAt(i);
            if (bag.indexOf(c) === -1) returnString += c;
        }
        return returnString;
    },
    CheckInternationalPhone: function (strPhone) {
        if ((strPhone == null) || (strPhone === "") || strPhone.length > 15) {
            return false;
        }
        var bracket = 3;
        strPhone = User.Trim(strPhone);
        if (strPhone.indexOf("+") > 1) return false;
        if (strPhone.indexOf("-") !== -1) bracket = bracket + 1;
        if (strPhone.indexOf("(") !== -1 && strPhone.indexOf("(") > bracket) return false;
        var brchr = strPhone.indexOf("(");
        if (strPhone.indexOf("(") !== -1 && strPhone.charAt(brchr + 2) !== ")") return false;
        if (strPhone.indexOf("(") === -1 && strPhone.indexOf(")") !== -1) return false;
        s = User.StripCharsInBag(strPhone, validWorldPhoneChars);
        return (User.IsInteger(s) && s.length >= minDigitsInIPhoneNumber);
    },
    CheckInternationalPhoneNotReq: function (strPhone) {
        if (strPhone.length < 10 || strPhone.length > 15) {
            return false;
        }
        var bracket = 3;
        strPhone = User.Trim(strPhone);
        if (strPhone.indexOf("+") > 1) return false;
        if (strPhone.indexOf("-") !== -1) bracket = bracket + 1;
        if (strPhone.indexOf("(") !== -1 && strPhone.indexOf("(") > bracket) return false;
        var brchr = strPhone.indexOf("(");
        if (strPhone.indexOf("(") !== -1 && strPhone.charAt(brchr + 2) !== ")") return false;
        if (strPhone.indexOf("(") === -1 && strPhone.indexOf(")") !== -1) return false;
        s = User.StripCharsInBag(strPhone, validWorldPhoneChars);
        if (s.length > 0) {
            s.length >= minDigitsInIPhoneNumber
        } else { return true }
        return (User.IsInteger(s));
    },
    //// End Phone Mobile check

    UpdateUserClick: function () {
        Common.ShowSendingProgress();
        var firstName = $("#FirstNameUpdateProfile").val();
        var lastName = $("#LastNameUpdateProfile").val();
        var email = $("#EmailUpdateProfile").val();
        var mobile = $("#MobileUpdateProfile").val();
        var re = /^[ A-Za-z0-9_]*$/ // for usertag;
        User.HideUpdateUserError();

        var model = new Object();
        model.Id = $("#registrationIdUpdateProfile").val();
        model.UserId = $("#userIdUpdateProfile").val();
        model.FirstName = $("#FirstNameUpdateProfile").val();
        model.LastName = $("#LastNameUpdateProfile").val();
        model.Email = $("#EmailUpdateProfile").val();
        model.UserName = $("#UserNameUpdateProfile").val();
        model.Mobile = $("#MobileUpdateProfile").val();
        model.Description = $("#DescriptionUpdateProfile").val();
        model.Website = $("#WebsiteUpdateProfile").val();
        model.UserTag = $("#UserTagUpdateProfile").val();


        if (firstName == null || firstName === '') {
            User.ShowUpdateUserError("Please Enter Your First Name.");
        }
        else if (lastName == null || lastName === '') {
            User.ShowUpdateUserError("Please Enter Your Last Name.");
        }
        else if (email == null || email === '') {
            User.ShowUpdateUserError("Please Enter Your Email Address.");
        }
        else if (!User.IsValidEmailAddress(email)) {
            User.ShowUpdateUserError("Please Enter Your Valid Email Address.");
        }
            /*else if (!User.CheckInternationalPhoneNotReq(mobile)) {
                User.ShowUpdateUserError('Invalid Mobile Number.');
            }*/

            /*else if (!$("#MobileUpdateProfile").intlTelInput("isValidNumber")) {
                User.ShowUpdateUserError('Invalid Mobile Number.');
            }*/
        else if ($("#UserTagUpdateProfile").val() == '' || $("#UserTagUpdateProfile").val() == null) {
            User.ShowUpdateUserError('please enter usertag.');
        }

        else if (!re.test($("#UserTagUpdateProfile").val())) {
            User.ShowUpdateUserError('please enter valid usertag.');
        }
        else if ($("#UserTagUpdateProfile").val() != '' || $("#UserTagUpdateProfile").val() != null) {
            $.ajax({
                url: '/Home/UserTagExist',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ usertag: $("#UserTagUpdateProfile").val() }),
                async: false,
                success: function (response) {
                    if (response.IsSuccess) {
                        Common.ShowError("User tag already exist. Pls enter different then entered.");
                    } else {

                        //$("#frmUpdateUser").submit();

                        $.ajax({
                            url: '/Home/UpdateUserProfile',
                            type: 'POST',
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify(model),
                            success: function (response) {
                                if (response.IsSuccess) {
                                    window.location = "/Profile/" + response.username;
                                } else {
                                    User.ShowUpdateUserError(response.html);
                                }
                            },
                            error: function (response) {
                                //alert(response.html);
                            },
                            complete: function () {
                                Common.StopProgress();
                            }
                        });
                    }
                },
                error: function (response) {
                    alert(response.html);
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        }
        Common.StopProgress();
        Common.ScrollToElement($("#LayoutDiv1"));
    },
    Rating: function () {
        $('#rating-input').on('rating.change', function () {
            var $this = this;
            Common.ShowSendingProgress();
            $.ajax({
                url: "/Home/UserRating",
                type: "POST",
                dataType: "json",
                data: { user: $('.visitedUser').attr('id'), rate: $('#rating-input').val() },
                success: function (response) {
                    if (response.IsSuccess) {
                        //Common.ShowSuccess("Rating done successfully", true);
                        //$('#rating-input').val(response.rate);
                        $("#setRate").html(response.rate);
                        $('.btn-group').removeClass('open');
                    } else {
                        Common.ShowError(response.html, true);
                    }
                },
                complete: function () {
                    Common.StopProgress();
                }
            });
        });
    },
    HideProfileMessageError: function () {
        $('#messageErrorSpan').text("");
        $('#messageErrorDiv').hide();
    },
    ShowMessageError: function (errorMessage) {
        $('#messageErrorSpan').text(errorMessage);
        $('#messageErrorDiv').show();
    },
    BtnProfileMessage: function () {
        $("#userMessage").val('');
        User.HideProfileMessageError();
    },
    SendProfileMessage: function () {
        User.HideProfileMessageError();
        var userMessage = $("#userMessage").val();
        if (userMessage == null || userMessage === '') {
            User.ShowMessageError("Please Enter Message.");
        }
        else {
            Common.ShowSendingProgress();
            $.ajax({
                url: '/Home/UserMessage',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ message: userMessage, messageTo: $('.visitedUser').attr('id') }),
                success: function (response) {
                    if (response.IsSuccess) {
                        //Common.ShowSuccess("Message sent successfully", true);
                        $('#CloseMessageModal').trigger('click');
                        Common.StopProgress();
                        return;
                    } else {
                        User.ShowLoginError(response.html);
                    }
                }
            });
        }
    },

    HideReportUserError: function () {
        $('#reportUserErrorSpan').text("");
        $('#reportUserErrorDiv').hide();
    },
    ShowReportUserError: function (errorMessage) {
        $('#reportUserErrorSpan').text(errorMessage);
        $('#reportUserErrorDiv').show();
    },
    ReportUserClick: function (toUser) {
        Common.ShowSendingProgress();
        $.get('/Home/GetReportUserDialog', { toUser: toUser }, function (data) {
            $('#ReportUserContainer').html(data);
            $('#ReportUserModal').removeClass('hide');
            $('#ReportUserModal').modal('show');
            Common.StopProgress();
        });
    },
    ReportUser: function (toUser) {
        User.HideReportUserError();
        var reportUserComment = $("#reportUserComment").val();
        if (reportUserComment == null || reportUserComment === '') {
            User.ShowReportUserError("Please Enter Comment.");
        }
        else {
            Common.ShowSendingProgress();
            $.get('/Home/ReportUser', { toUser: toUser, comment: reportUserComment }, function (data) {
                if (data.IsSuccess) {
                    $('#CloseReportUserModal').trigger('click');
                    Common.StopProgress();
                }
            });
        }
    }
};

$(document).ready(function () {
    User.Rating();
});