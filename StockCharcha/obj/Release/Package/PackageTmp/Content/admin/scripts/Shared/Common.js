﻿var Common = {
    Paging: function (NumberOfPages, CurrentPage, ControllerName) {
        $('.paging').bootpag({
            total: NumberOfPages,
            page: CurrentPage,
            maxVisible: 10,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on('page', function (event, num) {
            $(".adminLoading").fadeIn("slow");
            var page = num;
            $.ajax({
                url: '/' + ControllerName + '/GetListByPage',
                data: { "searchText": $("#searchText").val(), "page": page },
                success: function (data) {
                    $("#searchList").html(data);
                },
                complete: function () {
                    $(".adminLoading").fadeOut("slow");
                }
            });
        });
    },
};
$(document).ready(function () {
});
$(window).load(function () {
    // Animate loader off screen
    $(".adminLoading").fadeOut("slow");
});

