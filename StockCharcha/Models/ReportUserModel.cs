﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models
{
    public class ReportUserModel
    {
        public long Id { get; set; }
        public string FromUser { get; set; }
        public string ToUser { get; set; }
        public string Comments { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
    }
}