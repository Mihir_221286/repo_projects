﻿using StockCharcha.Utils.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StockCharcha.Models
{
    public class SettingModel
    {
        public long Id { get; set; }
        public StreamPrefrence StreamPrefrence { get; set; }
        public int? ShareIdeaText { get; set; }
        public int? CommentTextLimit { get; set; }
        public int? ConsensusDays { get; set; }
        [Range(5, 120, ErrorMessage = "Seconds must be betwween 5 to 20.")]
        public int StockUpdate { get; set; }
        public int? ViewsOnPage { get; set; }
        public int? Ideas { get; set; }
        public int? Likes { get; set; }
        public int? MenuByDays { get; set; }
    }
}