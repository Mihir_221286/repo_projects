﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models
{
    public class SiteActivityModel
    {
        public long SiteActivityID { get; set; }
        public string ModuleName { get; set; }
        public string TraceID { get; set; }
        public string IPAddress { get; set; }
        public string Browser { get; set; }
        public string Description { get; set; }
        public string WebURL { get; set; }
        public Guid? ModifiedBY { get; set; }
        public string ModifiedName { get; set; }
        public DateTime? ModifiedON { get; set; }
        public string ModifiedONDate { get; set; }
    }
}