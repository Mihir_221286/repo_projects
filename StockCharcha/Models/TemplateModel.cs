﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StockCharcha.Models
{
    public class TemplateModel
    {
        public long TemplateID { get; set; }
        [Required]
        public string TemplateName { get; set; }
        [Required]
        public string TemplateSubject { get; set; }
        [Required]
        [UIHint("tinymce_full"), AllowHtml]
        public string TemplateDescription { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public Guid ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public string CompanyCode { get; set; }
    }
}