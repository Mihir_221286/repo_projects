﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StockCharcha.Models
{
    public class SymbolModel : IValidatableObject
    {
        public long Id { get; set; }
        //[Required]
        [Display(Name = "NSE Symbol")]
        public string NSESymbol { get; set; }
        //[Required]
        [Display(Name = "BSE Symbol")]
        public string BSESymbol { get; set; }
        [Display(Name = "Security Id")]
        public string SecurityId { get; set; }
        [Display(Name = "ISIN No.")]
        public string ISINNumber { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Display(Name = "Date Of Listing")]
        public string DateOfListing { get; set; }
        [Display(Name = "PaidUp Value")]
        public string PaidUpValue { get; set; }
        [Display(Name = "Face Value")]
        public string FaceValue { get; set; }
        public string Status { get; set; }
        public string Series { get; set; }
        [Display(Name = "Market Lot")]
        public string MarketLot { get; set; }
        [Display(Name = "Symbol Group")]
        public string SymbolGroup { get; set; }
        public string Industry { get; set; }
        public int? Hit { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(NSESymbol) && string.IsNullOrEmpty(BSESymbol))
                yield return new ValidationResult("Enter NSE Or BSE symbol.");
        }

    }
}