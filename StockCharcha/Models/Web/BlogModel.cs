﻿using StockCharcha.Utils.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models.Web
{
    public class BlogModel
    {
        public long Id { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }
        public bool Everyone { get; set; }
        public bool OnlyFollowers { get; set; }
        public bool EmailOnReply { get; set; }
        public string OriginalFileName { get; set; }
        public string OriginalFilePath { get; set; }
        public string NewFileName { get; set; }
        public TrendInsights TrendInSights { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool IsPublished { get; set; }
        public string EditorImage { get; set; }
    }
    public class ShareIdeaBlogModel
    {
        public string Symbol { get; set; }
        public string Description { get; set; }
        public bool Everyone { get; set; }
        public bool OnlyFollowers { get; set; }
        public TrendInsights? TrendInSights { get; set; }
        public bool EmailOnReply { get; set; }
        public string OriginalFileName { get; set; }
        public string NewFileName { get; set; }
        public bool IsLink { get; set; }
    }

    public class ShareIdeaBlogListModel
    {
        public long BlogId { get; set; }
        public string Image { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public string CreatedDateTime { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TrendInSights { get; set; }
        public string TrendInSightsColor { get; set; }
        public string AttachedFile { get; set; }
        public bool ReShareId { get; set; }
        public bool Like { get; set; }
        public int CommentsCount { get; set; }
        public int LikesCount { get; set; }
        public Guid? CreatedBy { get; set; }
        public bool DeleteIdeaOption { get; set; }
        public ReshareBlockModel ReshareBlock { get; set; }
        public bool IsLink { get; set; }
    }

    public class ReshareBlockModel
    {
        public long BlogId { get; set; }
        public long Image { get; set; }
        public string userImage { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
    }

    public class JsonModel
    {
        public string HTMLString { get; set; }
        public bool NoMoreData { get; set; }
    }

    public class CommentModel
    {
        public CommentModel()
        {
            CommentListUserWise = new List<CommentListUserWise>();
        }
        public long BlogId { get; set; }
        public long? ParentId { get; set; }
        public string UserName { get; set; }
        public int? CommentTextLimit { get; set; }
        public List<CommentListUserWise> CommentListUserWise { get; set; }
        public string AttachFileName { get; set; }
        public string OriginalFilePath { get; set; }
        public string CommentImage { get; set; }
    }

    public class ReshareModel
    {
        public ShareIdeaBlogListModel ShareIdeaModel { get; set; }
        public long BlogId { get; set; }
        public string Description { get; set; }
    }

    public class CommentListUserWise
    {
        public long CommentId { get; set; }
        public string CommentCreatedBy { get; set; }
        public string Image { get; set; }
        public DateTime? CommentCreatedOn { get; set; }
        public string Description { get; set; }
        public string AttachFileName { get; set; }
    }
}