﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models.Web
{
    public class UserListModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string UserImage { get; set; }
    }

    public class MessageListModel
    {
        public Guid LoggedInUserId { get; set; }
        public Guid MessageTo { get; set; }
        public string UserImageTo { get; set; }
        public string UserImageFrom { get; set; }
        public string Message { get; set; }
        public string CreatedDate { get; set; }
    }
    public class MessageModel
    {
        public MessageModel()
        {
            MessageListModel = new List<MessageListModel>();
        }
        public Guid SelectedUser { get; set; }
        public List<MessageListModel> MessageListModel { get; set; }
    }
}