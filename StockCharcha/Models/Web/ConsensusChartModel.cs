﻿using StockCharcha.Utils.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models.Web
{
    public class ConsensusChartModel
    {
        public int TotalCnt { get; set; }
        public int TrendInSight { get; set; }
        public string TrendInSightName { get; set; }
        public int Percentage { get; set; }
    }
}