﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models.Web
{
    public class EmailModel
    {
        public string subject { get; set; }
        public string body { get; set; }
        public string emailType { get; set; }
        public string emailfor { get; set; }
    }
}