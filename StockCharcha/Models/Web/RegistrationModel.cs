﻿using System;
using System.ComponentModel.DataAnnotations;
using StockCharcha.DataAccess;

namespace StockCharcha.Models.Web
{
    public class RegistrationModel
    {
        public long Id { get; set; }

        public Guid UserId { get; set; }

        [Display(Name = "Role")]
        public string RoleId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [Display(Name = "Confirm Password ")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is not valid")]
        public string Mobile { get; set; }

        [StringLength(500, ErrorMessage = "Description allowed max 500 charecters.")]
        public string Description { get; set; }
        public string Website { get; set; }
        public byte[] Photo { get; set; }
        public string userImage { get; set; }

        [Display(Name = "User Tag")]
        public string UserTag { get; set; }
        public long? CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Title { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual aspnet_Roles aspnet_Roles { get; set; }
    }
}