﻿using System;

namespace StockCharcha.Models.Web
{
    public class SymbolModel
    {
        public int SearchType { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string  SymbolLabel { get; set; }
        public string NseSymbol { get; set; }
        public string BseSymbol { get; set; }
        public string SecurityId { get; set; }
        public string IsinNumber { get; set; }
        public string CompanyName { get; set; }
        public string DateOfListing { get; set; }
        public int PaidUpValue { get; set; }
        public int FaceValue { get; set; }
        public string Status { get; set; }
        public string Series { get; set; }
        public int MarketLot { get; set; }
        public string SymbolGroup { get; set; }
        public string Industry { get; set; }
        public int Hit { get; set; }
        public string SearchedValue { get; set; }
        public long Id { get; set; }
    }
}