﻿using StockCharcha.Utils.Enums;
using System;
using System.Collections.Generic;

namespace StockCharcha.Models.Web
{
    public class StockModel
    {
        public StockModel()
        {
            StockMenuList = new List<StockMenu>();
            UserRecentlyViewedList = new List<UserRecentlyViewed>();
            WatchListModel = new List<WatchList>();
            ShareIdeaBlogModelList = new List<ShareIdeaBlogListModel>();
            ProfileListModel = new List<ProfileList>();
            symbolModelList = new List<SymbolModel>();
            ConsensusChartModel = new List<ConsensusChartModel>();
            UserListModel = new List<UserListModel>();
            HelpQuestionListModel = new List<HelpQuestionModel>();
            HelpAnswerListModel = new List<HelpAnswerModel>();
        }
        public StockDetail StockDetail { get; set; }
        public BseStockDetail BseStockDetail { get; set; }
        public NseStockDetail NseStockDetail { get; set; }
        public List<StockMenu> StockMenuList { get; set; }
        public List<SymbolModel> symbolModelList { get; set; }
        public RegistrationModel RegistrationModel { get; set; }
        public LoggedInUser LoggedInUser { get; set; }
        public List<UserRecentlyViewed> UserRecentlyViewedList { get; set; }
        public List<UserRecentlyViewed> RecentlyViewedUserList { get; set; }
        public List<WatchList> WatchListModel { get; set; }
        public List<ShareIdeaBlogListModel> ShareIdeaBlogModelList { get; set; }
        public Followed Followed { get; set; }
        public Followers Followers { get; set; }
        public UserRatingModel UserRatingModel { get; set; }
        public List<ProfileList> ProfileListModel { get; set; }
        public int ShareIdeaTextLimit { get; set; }
        public string MaxConsensusName { get; set; }
        public List<ConsensusChartModel> ConsensusChartModel { get; set; }
        public int WatchListCount { get; set; }
        public bool IsWatch { get; set; }
        public string StockExchange { get; set; }
        public double StockUpdate { get; set; }
        public int StreamPrefrence { get; set; }
        public int NewIdeaCount { get; set; }
        public DateTime LastFetchIdeaDate { get; set; }
        public bool IsFollow { get; set; }
        public bool IsStockMarketOpenClose { get; set; }
        public List<UserListModel> UserListModel { get; set; }
        public MessageModel MessageModel { get; set; }
        public ViewNSE_BSEModel ViewNSE_BSEModel { get; set; }
        public List<HelpQuestionModel> HelpQuestionListModel { get; set; }
        public List<HelpAnswerModel> HelpAnswerListModel { get; set; }
    }

    public class StockDetail
    {
        public string StockName { get; set; }
        public string StockRate { get; set; }
        public string StockRatePercentage { get; set; }
        public string StockType { get; set; }
        public string StockLastUpdated { get; set; }
        public string StockUpDown { get; set; }
        public string StockSymbol { get; set; }
        public string Exchange { get; set; }
    }
    public class NseStockDetail
    {
        public string StockName { get; set; }
        public string StockRate { get; set; }
        public string StockRatePercentage { get; set; }
        public string StockType { get; set; }
        public string StockLastUpdated { get; set; }
        public string StockUpDown { get; set; }
        public string StockSymbol { get; set; }
        public string Exchange { get; set; }
    }
    public class BseStockDetail
    {
        public string StockName { get; set; }
        public string StockRate { get; set; }
        public string StockRatePercentage { get; set; }
        public string StockType { get; set; }
        public string StockLastUpdated { get; set; }
        public string StockUpDown { get; set; }
        public string StockSymbol { get; set; }
        public string Exchange { get; set; }
    }
    public class StockMenu
    {
        public string StockcompanyName { get; set; }
        public string StockSymbol { get; set; }
        public string StockIsinNumber { get; set; }
        public string Exchange { get; set; }
        public int? Hit { get; set; }
        public int Total { get; set; }
    }
    public class LoggedInUser
    {
        public string FirstName { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public long RegistrationId { get; set; }
        public string userImage { get; set; }
    }
    public class UserRecentlyViewed
    {
        public string Symbol { get; set; }
        public string BseSymbol { get; set; }
        public Guid ViewedUser { get; set; }
        public string Username { get; set; }
        public string Exchange { get; set; }
    }

    public class WatchList
    {
        public string StockSymbol { get; set; }
        public string CompanyName { get; set; }
        public string StockUpDown { get; set; }
        public string StockRate { get; set; }
        public string StockRatePercentage { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Exchange { get; set; }
    }

    public class HeaderShareIdea
    {
        public int ShareIdeaTextLimit { get; set; }
    }
}