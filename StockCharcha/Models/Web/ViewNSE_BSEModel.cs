﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models.Web
{
    public class ViewNSE_BSEModel
    {
        public ViewBSEModel ViewBSEModel { get; set; }
        public ViewNSEModel ViewNSEModel { get; set; }
    }
    public class ViewNSEModel
    {
        public string Symbol { get; set; }
        public string Type { get; set; }
    }
    public class ViewBSEModel
    {
        public string Symbol { get; set; }
        public string Type { get; set; }
    }
}