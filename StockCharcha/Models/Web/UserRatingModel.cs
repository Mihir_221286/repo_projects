﻿namespace StockCharcha.Models.Web
{
    public class UserRatingModel
    {
        public decimal? Rate { get; set; }
    }
}