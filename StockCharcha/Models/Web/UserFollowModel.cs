﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models.Web
{
    public class UserFollowModel
    {
        public long Id { get; set; }
        public Guid? FollowTo { get; set; }
        public Guid? FollowBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }

    public class Followed
    {
        public int UserFollowed { get; set; }

    }
    public class Followers
    {
        public int UserFollowers { get; set; }
    }

    public class ProfileList  //FollowerList,FollowedList
    {
        public long Id { get; set; }
        public string RoleId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }
        public string UserTag { get; set; }
        public long? CreatedBy { get; set; }
        public byte[] photo { get; set; }
        public string userImage { get; set; }
        public string CreatedOn { get; set; }
    }
}