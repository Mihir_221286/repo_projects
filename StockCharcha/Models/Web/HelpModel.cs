﻿using StockCharcha.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models.Web
{
    public class HelpModel
    {
    }
    public class HelpQuestionModel
    {
        public long Id { get; set; }
        public Guid FromUser { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsResponded { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<HelpAnswer> HelpAnswers { get; set; }
    }
    public class HelpAnswerModel
    {
        public long Id { get; set; }
        public long HelpQuestionId { get; set; }
        public Guid FromUser { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsRead { get; set; }
        public bool IsActive { get; set; }
        public string Username { get; set; }
        public string UserImage { get; set; }
        public Guid LoggedInUserId { get; set; }
        public virtual HelpQuestion HelpQuestion { get; set; }
    }
}