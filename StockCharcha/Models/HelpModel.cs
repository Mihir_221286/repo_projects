﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models
{
    public class HelpModel
    {
        public HelpModel()
        {
            HelpAnswerModel = new List<HelpAnswerModel>();
        }
        public long Id { get; set; }
        public string Question { get; set; }
        public Guid FromUser { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsResponded { get; set; }
        public bool IsActive { get; set; }


        public List<HelpAnswerModel> HelpAnswerModel { get; set; }
        public string UserQuestion { get; set; }
    }

    public class HelpAnswerModel
    {
        public long Id { get; set; }
        public long HelpQuestionId { get; set; }
        public Guid FromUser { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsRead { get; set; }
        public bool IsActive { get; set; }
        public string Username { get; set; }
        public string UserImage { get; set; }
        public Guid LoggedInUserId { get; set; }
    }
}