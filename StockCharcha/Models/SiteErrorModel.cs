﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockCharcha.Models
{
    public class SiteErrorModel
    {
        public long ErrorID { get; set; }
        public string ErrorCode { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string Source { get; set; }
        public string IPAddress { get; set; }
        public string Browser { get; set; }
        public string Description { get; set; }
        public string WebURL { get; set; }
        public Guid ModifiedBY { get; set; }
       public string ModifiedName { get; set; }
        public DateTime? ModifiedON { get; set; }
        public string ModifiedONDate { get; set; }
    }
}