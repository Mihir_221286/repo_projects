﻿
namespace StockCharcha.Utils.Enums
{
    public enum HeaderSearchOptions
    {
        Stock = 0,
        User = 1,
        All = 2,
    }
}