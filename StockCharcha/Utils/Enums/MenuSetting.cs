﻿using System.ComponentModel;

namespace StockCharcha.Utils.Enums
{
    public enum MenuSetting
    {
        Hits = 1,
        Comments = 2,
    }
}