﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace StockCharcha.Utils.Enums
{
    public enum TrendInsights
    {
        [Description("Strong Buy")]
        StrongBuy = 0,
        [Description("Buy")]
        Buy = 1,
        [Description("Hold")]
        Hold = 2,
        [Description("Sell")]
        Sell = 3,
        [Description("Strong Sell")]
        StrongSell = 4
    }
}