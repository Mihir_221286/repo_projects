﻿using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System;
using System.Configuration;
using System.Reflection;
using System.ComponentModel;
using StockCharcha.Utils.Utilities;

namespace StockCharcha.Utils.Utilities
{
    public class GlobalUtil
    {
        #region "Public Method(s)"

        /// <summary>
        /// Generate Random Number
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetRandomNumber(int length)
        {
            try
            {
                Random random = new Random();
                return new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", length)
                                .Select(s => s[random.Next(s.Length)])
                                .ToArray());
            }
            catch (Exception ex)
            {
                ex.LogError(typeof(GlobalUtil));
            }
            return null;
        }

        /// <summary>
        /// Genrate array of element from startnumber to endumber
        /// </summary>
        /// <param name="startnumber"></param>
        /// <param name="endumber"></param>
        /// <returns></returns>
        public static int[] GenerateRangeArray(int startnumber, int endumber)
        {
            if (endumber <= startnumber)
            {
                throw new ArgumentOutOfRangeException("startnumber", @"endumber must be greater than startnumber.");
            }
            return Enumerable.Range(startnumber, endumber - startnumber).ToArray();
        }

        public static string GetIPAddress()
        {
            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GetWebBrowser()
        {
            string returnval = "";
            string browserName = "";
            //return HttpContext.Current.Request.Browser.Browser;
            try
            {
                string navigatorAgent = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
                string fullVersionName = "";
                int nameOffset, verOffset, ix;

                // In Firefox, the true version is after "Firefox" 
                if ((verOffset = navigatorAgent.IndexOf("Firefox")) != -1)
                {
                    browserName = "Firefox";
                    fullVersionName = navigatorAgent.Substring(verOffset + 8);
                }

                // In MSIE, the true version is after "MSIE" in userAgent
                else if ((verOffset = navigatorAgent.IndexOf("rv:")) != -1)
                {
                    browserName = "IE";
                    fullVersionName = navigatorAgent.Substring(verOffset + 3);
                }
                // In MSIE, the true version is after "MSIE" in userAgent
                else if ((verOffset = navigatorAgent.IndexOf("MSIE")) != -1)
                {
                    browserName = "IE";
                    fullVersionName = navigatorAgent.Substring(verOffset + 5);
                }
                // In Chrome, the true version is after "Chrome" 
                else if ((verOffset = navigatorAgent.IndexOf("Chrome")) != -1)
                {
                    browserName = "Chrome";
                    fullVersionName = navigatorAgent.Substring(verOffset + 7);
                }

                // In Opera, the true version is after "Opera" or after "Version"
                else if ((verOffset = navigatorAgent.IndexOf("Opera")) != -1)
                {
                    browserName = "Opera";
                    fullVersionName = navigatorAgent.Substring(verOffset + 6);
                    if ((verOffset = navigatorAgent.IndexOf("Version")) != -1)
                        fullVersionName = navigatorAgent.Substring(verOffset + 8);
                }

                // In Safari, the true version is after "Safari" or after "Version" 
                else if ((verOffset = navigatorAgent.IndexOf("Safari")) != -1)
                {
                    browserName = "Safari";
                    fullVersionName = navigatorAgent.Substring(verOffset + 7);
                    if ((verOffset = navigatorAgent.IndexOf("Version")) != -1)
                        fullVersionName = navigatorAgent.Substring(verOffset + 8);
                }

                // In most other browsers, "name/version" is at the end of userAgent 
                else if ((nameOffset = navigatorAgent.LastIndexOf(' ') + 1) <
                          (verOffset = navigatorAgent.LastIndexOf('/')))
                {
                    browserName = navigatorAgent.Substring(nameOffset, verOffset);
                    fullVersionName = navigatorAgent.Substring(verOffset + 1);

                }
                // trim the fullVersionName string at semicolon/space if present
                if ((ix = fullVersionName.ToString().IndexOf(";")) != -1)
                    fullVersionName = fullVersionName.ToString().Substring(0, ix);
                if ((ix = fullVersionName.ToString().IndexOf(" ")) != -1)
                    fullVersionName = fullVersionName.ToString().Substring(0, ix);

                if (fullVersionName.ToString().IndexOf(".") != -1)
                {
                    fullVersionName = fullVersionName.Substring(0, fullVersionName.ToString().IndexOf("."));
                }

                returnval = browserName + " " + fullVersionName;
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return returnval;

        }

        public static string GetWebURL()
        {
            return HttpContext.Current.Request.Url.AbsoluteUri;
            //return HttpContext.Current.Request.RawUrl;
            //return HttpContext.Current.Request.Form.ToString();
        }

        #endregion
    }
}
