﻿using System;

namespace StockCharcha.Utils.Utilities
{
    public static class DateUtil
    {
        /*public static string ToTimeAgo(this DateTime date)
        {

            TimeSpan timeSince = DateTime.Now.Subtract(date);
            if (timeSince.TotalMilliseconds < 1)
                return "not yet";
            if (timeSince.TotalMinutes < 1)
                return "just now";
            if (timeSince.TotalMinutes < 2)
                return "1 minute ago";
            if (timeSince.TotalMinutes < 60)
                return string.Format("{0} minute{1} ago", timeSince.Minutes, timeSince.Minutes > 1 ? "s" : string.Empty);
            if (timeSince.TotalMinutes < 120)
                return "1 hour ago";
            if (timeSince.TotalHours < 24)
                return string.Format("{0} hour{1} ago", timeSince.Hours, timeSince.Hours > 1 ? "s" : string.Empty);
            if (Math.Round(timeSince.TotalDays) == 1)
                return "yesterday";
            if (timeSince.TotalDays < 7)
                return string.Format("{0} day{1} ago", timeSince.Days, timeSince.Days > 1 ? "s" : string.Empty);
            if (timeSince.TotalDays < 14)
                return "last week";
            if (timeSince.TotalDays < 21)
                return "2 weeks ago";
            if (timeSince.TotalDays < 28)
                return "3 weeks ago";
            if (timeSince.TotalDays < 60)
                return "last month";
            if (timeSince.TotalDays < 365)
                return string.Format("{0} month{1} ago", Math.Round(timeSince.TotalDays / 30), Math.Round(timeSince.TotalDays / 30) > 1 ? "s" : string.Empty);
            if (timeSince.TotalDays < 730)
                return "last year";

            //last but not least...
            return string.Format("{0} year{1} ago", Math.Round(timeSince.TotalDays / 365), Math.Round(timeSince.TotalDays / 365) > 1 ? "s" : string.Empty);
        }*/
        public static string ToTimeAgo(this DateTime date)
        {

            TimeSpan timeSince = DateTime.Now.Subtract(date);
            if (timeSince.TotalMilliseconds < 1)
                return "not yet";
            if (timeSince.TotalMinutes < 1)
                return "just now";
            if (timeSince.TotalMinutes < 2)
                return "1 min ago";
            if (timeSince.TotalMinutes < 60)
                return string.Format("{0} min{1} ago", timeSince.Minutes, timeSince.Minutes > 1 ? "s" : string.Empty);
            if (timeSince.TotalMinutes < 120)
                return "1 hour ago";
            if (timeSince.TotalHours < 24)
                return string.Format("{0} hour{1} ago", timeSince.Hours, timeSince.Hours > 1 ? "s" : string.Empty);
            if (timeSince.TotalHours > 24)
                return date.ToString("MMM dd, yyyy");
            //last but not least...
            return date.ToString("MMM dd, yyyy");
        }
    }
}