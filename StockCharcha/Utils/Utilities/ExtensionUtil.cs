﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using log4net;

namespace StockCharcha.Utils.Utilities
{
    public static class ExtensionUtil
    {
        /// <summary>
        /// Log Exceptions extra data
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="log"></param>
        private static void LogErrorExtraData(Exception ex,ILog log)
        {
            log.Info("Extra error data : ");
            foreach (DictionaryEntry errorData in ex.Data)
            {
                log.Error(String.Format("{0} : {1}", errorData.Key, errorData.Value));
            }
        }

        /// <summary>
        /// Log Exceptions extra data
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="callingType"></param>
        public static void LogErrorExtraData(this Exception ex, Type callingType)
        {
            var log = LogManager.GetLogger(callingType);
            LogErrorExtraData(ex, log);
        }

        /// <summary>
        /// Log Exceptions extra data
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="callingType"></param>
        public static void LogErrorExtraData(Exception ex, object callingType)
        {
            var log = LogManager.GetLogger(callingType.GetType());
            LogErrorExtraData(ex, log);
        }


        /// <summary>
        /// Method Log error using Log4net
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="callingType"></param>
        public static void LogError(this Exception ex, object callingType)
        {
            var log = LogManager.GetLogger(callingType.GetType());
            if (log.IsErrorEnabled)
            {
                log.Error(ex.Message, ex);
                LogErrorExtraData(ex,log);
                //MailUtil.SendEmailToDevelopers(ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.ToString() : "N/A", ex.TargetSite != null ? ex.TargetSite.ToString() : "N/A");
                //throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// Method Log error using Log4net
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="callingType"></param>
        public static void LogError(this Exception ex, Type callingType)
        {
            var log = LogManager.GetLogger(callingType);
            if (log.IsErrorEnabled)
            {
                log.Error(ex.Message, ex);
                LogErrorExtraData(ex, log);
                //MailUtil.SendEmailToDevelopers(ex.Message, ex.StackTrace, ex.InnerException != null ? ex.InnerException.ToString() : "N/A", ex.TargetSite != null ? ex.TargetSite.ToString() : "N/A");
                //throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// Method log info into DB using Log4net
        /// </summary>
        /// <param name="message"></param>
        /// <param name="callingType"></param>
        public static void LogMessage(this string message, object callingType)
        {
            var log = LogManager.GetLogger(callingType.GetType());
            if (log.IsErrorEnabled)
            {
                log.Error(message);
            }
        }

        /// <summary>
        /// Method log info into DB using Log4net
        /// </summary>
        /// <param name="message"></param>
        /// <param name="callingType"></param>
        public static void LogInfo(this string message, object callingType)
        {
            var log = LogManager.GetLogger(callingType.GetType());
            if (log.IsInfoEnabled)
            {
                log.Info(message);
            }
        }

        /// <summary>
        /// Method log info into DB using Log4net
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public static void LogMessage(this string message, Type type)
        {
            var log = LogManager.GetLogger(type);
            if (log.IsErrorEnabled)
            {
                log.Error(message);
            }
        }

        /// <summary>
        /// set Log for web api method request
        /// </summary>
        /// <param name="message"></param>
        /// <param name="callingType"></param>
        public static void LogRequest(string message, object callingType)
        {
            try
            {
                var log = LogManager.GetLogger(callingType.GetType());
                log.Debug(message);
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, callingType);
            }
        }

        public static void LogRequest(this object callingType, string actionName, string controllerName, params KeyValuePair<string, string>[] logString)
        {
            try
            {
                var log = LogManager.GetLogger(callingType.GetType());
                log.Debug(GetLogMessageString(actionName, controllerName, logString));
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, callingType);
            }
        }

        /// <summary>
        /// Get Log Message from List with Formate tab
        /// </summary>
        /// <param name="actionName">Action Name</param>
        /// <param name="controllerName">Controller Name</param>
        /// <param name="logString">Key Value pair of model</param>
        /// <returns>string</returns>
        public static string GetLogMessageString(string actionName, string controllerName, params KeyValuePair<string, string>[] logString)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Action:" + actionName + "\t");
            sb.Append("Controller:" + controllerName + "\t");

            if (logString != null)
            {
                foreach (KeyValuePair<string, string> logStringPair in logString)
                {
                    sb.Append(Convert.ToString(logStringPair.Key) + ":" + Convert.ToString(logStringPair.Value) + "\t");
                }
            }

            return sb.ToString();
        }
    }
}
