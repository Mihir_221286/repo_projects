﻿using StockCharcha.DataAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace StockCharcha.Utils.Utilities
{
    public static class SiteActivityErrorsUtil
    {
        public static StockCharchaEntities db = new StockCharchaEntities();
        public static void SiteActivity(string moduleName, string traceID, string description)
        {
            Guid userId = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId) != Guid.Empty ? WebUtil.GetSessionValue<Guid>(CommonConstants.UserId) : Guid.Empty;
            SiteActivity siteActivity = new SiteActivity
            {
                ModuleName = moduleName,
                TraceID = traceID,
                IPAddress = GlobalUtil.GetIPAddress(),
                Browser = GlobalUtil.GetWebBrowser(),
                Description = description,
                WebURL = GlobalUtil.GetWebURL(),
                ModifiedBY = userId,
                ModifiedON = DateTime.Now,
            };
            db.SiteActivities.Add(siteActivity);
            db.SaveChanges();
        }
        public static void SiteError(Exception e)
        {
            string fileName = "";
            string lineNo = "";
            string description = "";

            if (e.InnerException != null)
            {
                Exception inex = e.InnerException;
                StackTrace trace = new StackTrace(inex, true);
                if (trace != null && trace.FrameCount > 0)
                {
                    fileName = trace.GetFrame(0).GetFileName();
                    lineNo = Convert.ToString(trace.GetFrame(0).GetFileLineNumber());
                }
            }
            description = "Type : " + e.GetType().ToString() + " Method Name : " + e.TargetSite + " FileName : " + fileName + " Line No : " + lineNo;
            Guid userId = WebUtil.GetSessionValue<Guid>(CommonConstants.UserId) != Guid.Empty ? WebUtil.GetSessionValue<Guid>(CommonConstants.UserId) : Guid.Empty;
            SiteError siteError = new SiteError
            {
                ErrorCode = string.Empty,
                ExceptionMessage = e.Message,
                ExceptionStackTrace = e.StackTrace,
                Source = e.Source,
                IPAddress = GlobalUtil.GetIPAddress(),
                Browser = GlobalUtil.GetWebBrowser(),
                Description = description,
                WebURL = GlobalUtil.GetWebURL(),
                ModifiedBY = userId,
                ModifiedON = DateTime.Now,
            };
            db.SiteErrors.Add(siteError);
            db.SaveChanges();
        }
    }

}