﻿using RestSharp;
using RestSharp.Authenticators;
using StockCharcha.DataAccess;
using System;
using System.Net;
using System.Net.Mail;

namespace StockCharcha.Utils.Utilities
{

    public static class MailUtil
    {
        private static StockCharchaEntities db = new StockCharchaEntities();
        static string isMailSent = "1";
        public static IRestResponse SendMail(string to, string cc, string bcc, string subject, string body, bool isAttached, string attachment, string emailType, string trackid, string emailfor)
        {
            EmailSetting emailSetting = db.EmailSettings.Find(1);
            string smtpServer = emailSetting.SmtpServer;
            int smtpPort = emailSetting.SmtpPort;
            bool isssl = emailSetting.IsSSL;
            string smtpUserName = emailSetting.FromEmail;
            string pwd = emailSetting.Password;
            string fromEmail = emailSetting.FromEmail;
            string fromName = emailSetting.FromName;

            MailMessage message = new MailMessage();
            Attachment att = null;
            SmtpClient cmt = new SmtpClient(smtpServer, smtpPort);

            try
            {
                cmt.Credentials = new NetworkCredential(smtpUserName, pwd);
                MailAddress from = new MailAddress(fromEmail, fromName);

                message.Subject = subject;
                message.From = from;

                message.To.Add(to.TrimEnd(','));
                message.IsBodyHtml = true;
                message.Body = body;

                if (!string.IsNullOrEmpty(cc))
                {
                    message.CC.Add(cc.TrimEnd(','));
                }
                if (!string.IsNullOrEmpty(bcc))
                {
                    message.Bcc.Add(bcc.TrimEnd(','));
                }

                if (isAttached)
                {
                    att = new Attachment(attachment);
                    message.Attachments.Add(att);
                }

                if (isssl)
                {
                    cmt.EnableSsl = true;
                }
                cmt.DeliveryMethod = SmtpDeliveryMethod.Network;

                cmt.Send(message);
            }
            catch (Exception e)
            {
                isMailSent = "0";
                SiteActivityErrorsUtil.SiteError(e);
            }
            finally
            {
                EmailMaster saveEmail = new EmailMaster
                {
                    EmailFrom = emailSetting.FromEmail,
                    EmailTo = to,
                    CC = cc,
                    BCC = bcc,
                    Subject = subject,
                    Body = body,
                    IsAttachment = isAttached,
                    AttachmentPath = attachment,
                    Status = isMailSent,
                    EmailType = emailType,
                    CreatedOn = DateTime.Now,
                    CreatedBy = Convert.ToString(WebUtil.GetSessionValue<Guid>(CommonConstants.UserIdWeb))
                };
                SaveSentEmail(saveEmail);
            }
            return null;
        }
        public static void SaveSentEmail(EmailMaster email)
        {
            db.EmailMasters.Add(email);
            db.SaveChanges();
        }

        /*public TemplateMaster GetTemplateMailDetails(string TemplateName)
        {
            try
            {
                return db.TemplateMasters.Where(x => x.TemplateName.ToLower().Trim() == TemplateName.ToLower().Trim()).FirstOrDefault();
            }
            catch (Exception e)
            {
                SiteActivityErrorsUtil.SiteError(e);
            }
            return null;
        }*/
    }
}
