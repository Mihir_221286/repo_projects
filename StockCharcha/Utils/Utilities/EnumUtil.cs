﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;


namespace StockCharcha.Utils.Utilities
{
    public static class EnumUtil
    {
        /// <summary>
        /// Get IEnumerable List of string
        /// </summary>
        /// <typeparam name="T"> T is Enum</typeparam>
        /// <returns></returns>
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        /// <summary>
        /// Get Enum Description Attribute
        /// </summary>
        /// <param name="en">Enum Type</param>
        /// <returns></returns>
        public static string GetEnumDescription(Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            return en.ToString();
        }

        /// <summary>
        /// Parse string to enum by specifying the enum type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception ex)
            {
                ex.LogError(typeof(GlobalUtil));
                return (T)Enum.Parse(typeof(T), value, true);
            }
        }

        /// <summary>
        /// Return the list of string from the enum tyep passed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<string> GetStringListByEnumType<T>()
        {
            return Enum.GetNames(typeof(T)).ToList();
        }

        public static IEnumerable<string>  GetStringListDescriptionOfEnumType<T>()
        {
            var descs = new List<string>();
            var names = Enum.GetNames(typeof(T));
            foreach (var name in names)
            {
                var field = typeof(T).GetField(name);
                var fds = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                foreach (DescriptionAttribute fd in fds)
                {
                    descs.Add(fd.Description);
                }
            }
            return descs;
        }


        public static T GetValueFromDescription<T>(string description)
        {
            Int16 cnt = 0;
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                    {
                        cnt++;
                        return (T)field.GetValue(null);
                    }
                }

            }
            if (cnt <= 0)
            {
                foreach (var field in type.GetFields())
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }



    }
}
