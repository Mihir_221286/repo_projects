﻿namespace StockCharcha.Utils
{
    public static class CommonConstants
    {
        public const string ModelStatus = "ModelStatus";
        public const string ModelInvalidStatus = "Invalid";
        public const string ModelDeleteStatus = "Delete";
        public const string ModelInsertStatus = "Insert";
        public const string ModelUpdateStatus = "Update";

        public const string MessageStatus = "MessageStatus";
        public const string AddStatus = "Added Successfully";
        public const string UpdateStatus = "Updated Successfully";
        public const string InvalidStatus = "Invalid Model";

        public const string UserId = "UserId";
        public const string UserName = "UserName";
        public const string UserEmail = "UserEmail";

        public const string UserIdWeb = "UserIdWeb";
        public const string UserNameWeb = "UserNameWeb";
        public const string UserEmailWeb = "UserEmailWeb";

        public const string LoginStatus = "LoginStatus";
        public const string LoginFailedStatus = "LoginFailed";
        public const string InvalidLoginStatus = "InvalidLoginStatus";

        public const string LoginSuccess = "Log In Successfully";
        public const string LoginFailed = "UserName or Password invalid.";
        public const string LoginInvalid = "You have no rights to login";
        public const string ConfirmDelete = "Do you really want to delete ?";

        public const string HfPeriod = "1d";
        public const string HfInterval = "900";

        #region "Search Type"

        public const int Stock = 1;
        public const int Userr = 2;
        public const int All = 3;

        #endregion

        #region "User Roles"

        public const string Admin = "Admin";
        public const string SuperAdmin = "SuperAdmin";
        public const string User = "User";

        #endregion

        public const string UserSearch = "UserSearch";

        public const int PageSize = 10;

        /*public const string StrongBuy = "label-StrongBuy";
        public const string Buy = "label-Buy";
        public const string Hold = "label-Hold";
        public const string Sell = "label-Sell";
        public const string StrongSell = "label-StrongSell";*/

        public const string StrongBuy = "storngBuyStrip";
        public const string Buy = "buyStrip";
        public const string Hold = "holdstrip";
        public const string Sell = "sellStrip";
        public const string StrongSell = "storngSellStrip";

        #region "Exchange [Stock Type]"

        public const string NSE = "NSE";
        public const string BSE = "BSE";
        public const string Nulll = "null";

        public const string USER = "USER";
        #endregion

        public const string Followers = "Followers";
        public const string Following = "Following";

        public const string Sensex = "SENSEX";


        #region "Image Size"
        public const int userImage32 = 32;
        public const int userImage50 = 50;
        public const int userImage100 = 100;
        public const int ShareIdeaAttachment = 100;
        #endregion

        public const int BlockSize = 10;

    }
}
