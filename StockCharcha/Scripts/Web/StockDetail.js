﻿var BlockNumber = 2;  //Infinate Scroll starts from second block
var pathname = window.location.pathname;
var newFileName;
var originalFileName;
var IsLink;
var StockDetail = {
    StockTicker: function (symbol, type) {
        dataParam = { symbol: symbol, type: type };
        $.get('/Home/StockTicker', dataParam,
        function (data) {
            $("#stockTicker").html(data);
            var className = data.indexOf('lowprice') > 0 ? "borderLeftRed" : data.indexOf('upprice') > 0 ? "borderLeftGreen" : "";
            $("#stockTicker").parent().parent().removeClass().addClass('watchList' + ' ' + className)
        });
    },
    stockDetailData: function (symbol) {  //, type
        Common.ShowSendingProgress();
        StockDetail.SymbolDetailCall(symbol);  //, type
    },
    SymbolDetailCall: function (symbol) {  //, type
        Common.ShowSendingProgress();
        var stockDetail = $("#stockDetail");
        $.ajax({
            url: '/Home/StockDetail',
            data: JSON.stringify({ symbol: symbol }), //, type: type 
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            success: function (result) {
                if (result.IsSuccess) {
                    //stockDetail.html('');
                    stockDetail.html(result.html);
                    var className = result.html.indexOf('lowprice') > 0 ? "borderLeftRed" : result.html.indexOf('upprice') > 0 ? "borderLeftGreen" : "";
                    $("#stockTicker").parent().parent().removeClass().addClass('watchList' + ' ' + className)
                    //StockDetail.WatchListFilter(symbol,"null");
                    //$('.modal-backdrop').remove();
                }
                else {
                    stockDetail.html('');
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    RemoveContent: function (period, interval, symbol) {
        document.getElementById("hfInterval").value = interval;
        document.getElementById("hfPeriod").value = period;
        $.ajax({
            url: '/Home/StockGraph',
            async: false,
            type: 'POST',
            contentType: 'application/json',
            data: "{'period':'" + period + "','interval':'" + parseInt(interval) + "','symbol':'" + symbol + "','exchange':'" + $("#hfExchange").val() + "'}",
            dataType: 'json',//this is important
            success: function (data) {
                if (data.html.length > 0) {
                    StockDetail.drawVisualization(data.html);
                    $("." + period).addClass('active')
                    $("#timelineId li a").not("." + period).removeClass('active');
                    $("#chart_divStock").removeClass('container').removeAttr('style').attr('style', 'padding-left: 0 !important;height:184px;');
                } else {
                    $("#chart_divStock").html('No Chart Available').attr('style', 'text-align: center;font-weight: 800;');
                    //alert("No data found.");
                }
            },
            error: function (e) {
                //alert(e.message);
                //called when there is an error
                //console.log(e.message);
            }
        });
    },
    drawVisualization: function (serverData) {
        var chardata = [];
        var month = "00";
        var day = "00";
        var hour = "00";
        var minute = "00";

        for (var i = 0; i < serverData.length; i++) {

            month = serverData[i][1];
            day = parseInt(serverData[i][2]);
            hour = serverData[i][3];
            minute = serverData[i][4];

            if (serverData[i][1].toString().length == 1) {
                month = '0' + serverData[i][1];
            }
            if (serverData[i][2].toString().length == 1) {
                day = '0' + parseInt(serverData[i][2]);
            }
            if (serverData[i][3].toString().length == 1) {
                hour = '0' + serverData[i][3];
            }
            if (serverData[i][4].toString().length == 1) {
                minute = '0' + serverData[i][4];
            }
            //for (var i = 0; i < serverData.length; i++) {
            //chardata.push([new Date(serverData[i][0], serverData[i][1], serverData[i][2], serverData[i][3], serverData[i][4]), parseFloat(serverData[i][5])]);
            chardata.push([new Date(serverData[i][0], month, day, hour, minute), parseFloat(serverData[i][5])]);
        }
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'Date');
        data.addColumn('number', $("#StockSymbolName").val());
        data.addRows(chardata);

        new google.visualization.AnnotationChart(document.getElementById('chart_divStock')).draw(data, { width: 286, is3D: true, dateFormat: 'MMM dd, yyyy' });
    },
    WatchListAdd: function (item, type) {
        Common.ShowSendingProgress();
        var itemArray = type.split(",");
        var symbol = item;
        $.ajax({
            url: "/Home/WatchListAdd",
            type: "POST",
            dataType: "json",
            data: { item: symbol, type: itemArray[1] },
            success: function (response) {
                if (response.IsSuccess) {
                    StockDetail.WatchListFilter(symbol, itemArray[0]);
                    StockDetail.IsWatch(symbol, itemArray[1]);
                    //Common.ShowSuccess("Added to watchlist", true);
                } else {
                    Common.StopProgress();
                    alert(response.html);
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    WatchListAutocomplete: function () {
        $("#watchList").autocomplete({
            source: function (request, response) {
                // Get value of explore text (trim, replace multi space to single space, convert to lower string)  //.replace(/\s+/g, '-')
                var searchtext = request.term.trim().replace(/\s{2,}/g, ' ')
                                                                        .replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
                                                                        .toLowerCase();

                var NSE_BSE = "null";
                /*if (pathname.indexOf('NSE') > 0 || pathname.indexOf('BSE') > 0) {
                    if (pathname.indexOf('NSE') > 0) { NSE_BSE = "NSE"; }
                    else { NSE_BSE = "BSE"; }
                }*/

                if (searchtext.length >= 2) {
                    $.ajax({
                        type: "get",
                        url: "/Home/WatchList?searchText=" + searchtext + "&type=" + NSE_BSE,
                        contentType: "application/json",
                        beforeSend: function () {
                            //$(".glyphicon-search-refresh").addClass("glyphicon-refresh");
                        },
                        complete: function () {
                            //$(".glyphicon-search-refresh").removeClass("glyphicon-refresh");
                        },
                        success: function (data) {
                            response(data);
                        }
                    }).done(function (result) {
                        if (result == null || result === "") {
                            // If result null, then auto clear auto complete list
                            $('#ui-id-1').html('');
                        }
                    });
                }
            },
            messages: {
                noResults: "",
                results: function () { }
            },
            focus: function (event, ui) {
                // Set value in text box
                //$("#watchList").val(ui.item.CompanyName);
                return false;
            },
            select: function (event, ui) {
                var nseSymbol = ui.item.NseSymbol;
                var bseSymbol = ui.item.BseSymbol;
                if (nseSymbol !== "" && nseSymbol !== "null") {
                    StockDetail.WatchListAdd(nseSymbol, "NSE");
                }
                else if (bseSymbol !== "" && bseSymbol !== "null") {
                    StockDetail.WatchListAdd(bseSymbol, "BSE");
                }
                Common.StopProgress();
                return false;
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            var link = "";
            if (item.NseSymbol !== null && item.NseSymbol !== "") {
                link = item.NseSymbol + "   " + item.CompanyName;
            } else {
                link = item.BseSymbol + "   " + item.CompanyName;
            }

            return $("<li class='ui-menu-item-explore' style='width:270px;font-size: 12px;' tabindex='-1'>")
                .append("<a href ='javascript:void(0);'>" + link + "</a>")
                .append("</li>")
                .appendTo(ul);
        };

    },
    WatchListFilter: function (symbol, type) {
        //Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/WatchListFilter",
            type: "POST",
            dataType: "json",
            data: { symbol: symbol, type: type },
            success: function (response) {
                if (response.IsSuccess) {
                    $("#watchListFilter").html('');
                    $("#watchListFilter").html(response.html);
                    $("#watchList").val('');
                    $("#profileWatchFilter").html(response.html1);// refresh for profile watchlist;
                    $("#settingRightBody").html(response.html1); //setting watchlist refresh
                    //Common.StopProgress();
                }
            },
            complete: function () {
                //Common.StopProgress();
            }
        });
    },
    RecentlyViewedFilter: function () {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/RecentlyViewedListFilter",
            type: "POST",
            dataType: "json",
            //data: { item: item },
            success: function (response) {
                if (response.IsSuccess) {
                    $("#recentlyViewed").html('');
                    $("#recentlyViewed").html(response.html);
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    watchListBlockAdd: function () {
        $('#watchInput').fadeToggle("slow");
        //$("#LoginModal").modal('show');
    },
    watchListBlockclose: function () {
        $('#watchInput').fadeToggle("slow");
    },
    CommentsReply: function () {
        $('#commentsBlock').fadeToggle("slow");
    },
    CommentsCancel: function () {
        $('#commentsBlock').fadeToggle("slow");
    },
    CreateBlog: function () {
        Common.ShowSendingProgress();
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    StockDetail.ShareIdeaReset();
                    $("#ckEditor").val('#' + $("#symboll").val() + ' ');
                    $('#createBlock').fadeToggle("slow");
                } else {
                    //Common.ShowError("Please login first.", true);
                    //Common.ScrollToElement($(".notifications"));
                    $("#btnSignIn").trigger('click');
                }
            },
            complete: function () {
                Common.StopProgress();
            }

        });
    },
    UploadAttachment: function () {
        $('#txtUploadFile').on('change', function (e) {
            Common.ShowSendingProgress();
            var files = e.target.files;
            if (files.length > 0) {
                if (window.FormData !== undefined) {
                    var data = new FormData();
                    for (var x = 0; x < files.length; x++) {
                        data.append("file" + x, files[x]);
                    }

                    $.ajax({
                        type: "POST",
                        url: '/Home/UploadAttachedFile',
                        contentType: false,
                        processData: false,
                        data: data,
                        success: function (result) {
                            if (result.IsSuccess) {
                                newFileName = result.html.NewFileName;
                                originalFileName = result.html.OriginalFilePath;
                                $('#editorImage').html(result.html.EditorImage)
                            } else {
                                $('#txtUploadFile').replaceWith($("#txtUploadFile").clone(true));
                                Common.ShowError("Please login first.", true);
                                Common.ScrollToElement($(".notifications"));
                            }
                        },
                        complete: function () {
                            Common.StopProgress();
                        },
                        error: function (xhr, status, p3, p4) {
                            var err = "Error " + " " + status + " " + p3 + " " + p4;
                            if (xhr.responseText && xhr.responseText[0] == "{")
                                err = JSON.parse(xhr.responseText).Message;
                            console.log(err);
                        }
                    });
                } else {
                    alert("This browser doesn't support HTML5 file uploads!");
                }
            }
        });
    },
    RemoveAttachment: function () {
        //Common.ShowSendingProgress();
        $('#txtUploadFile').replaceWith($("#txtUploadFile").clone(true));
        $('#editorImage').html('');
        newFileName = "";
        originalFileName = "";
    },
    //Find URL from entered text.
    Strip: function (html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return tmp.innerText.replace(urlRegex, function (url) {
            return '\n' + url;
        })
    },
    RenderHTML: function (text) {
        var rawText = StockDetail.Strip(text);
        var urlRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/ig
        //  /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

        return rawText.replace(urlRegex, function (url) {
            if ((url.indexOf(".jpg") > 0) || (url.indexOf(".png") > 0) || (url.indexOf(".gif") > 0)) {
                return '<img src="' + url + '">' + '<br/>';
            } else {
                if ((url.indexOf("http") >= 0)) {
                    IsLink = true;
                    return '<a href="' + url + '" target="_blank">' + url + '</a>';//+ '<br/>'
                } else {
                    url = url.toLowerCase().replace("www.", "http://www.");
                    IsLink = true;
                    return '<a href="' + url + '" target="_blank">' + url + '</a>';//+ '<br/>'
                }

            }
        })
    },
    //

    ShareIdea: function () {
        Common.ShowSendingProgress();
        var symbol = $("#symboll").val();
        var description = $("#ckEditor").val();
        if (description === "") {
            Common.ShowError("Share idea first.", true);
            Common.StopProgress();
            return false;
        } else {
            description = StockDetail.RenderHTML($("#ckEditor").val());
        }

        /*var type = "";
        if (pathname.indexOf('NSE') > 0) {
            type = "NSE";
        }
        else if (pathname.indexOf('BSE') > 0) {
            type = "BSE";
        }*/
        var model = new Object();
        model.Symbol = symbol;
        model.Description = description;//CKEDITOR.instances.ckeditor.getData();
        model.Everyone = $('#chkEveryone').is(":checked");
        model.OnlyFollowers = $('#chkFollower').is(":checked");
        model.TrendInSights = $("#drpTrend").children().closest(".active").val();
        model.EmailOnReply = $('#chkEmailMe').is(":checked");
        model.OriginalFileName = originalFileName;
        model.NewFileName = newFileName;
        model.IsLink = IsLink;

        $.ajax({
            url: "/Home/ShareIdea",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify(model),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    StockDetail.ShareIdeaReset();
                    //Common.ShowSuccess("Posted successfully.", true);
                    newFileName = "";
                    originalFileName = "";
                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                    $('#createBlock').fadeToggle("slow");
                    StockDetail.RefreshStockRatingChart(symbol); //, type
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    ShareIdeaCancel: function () {
        $('#createBlock').fadeToggle("slow");
        StockDetail.ShareIdeaReset();
        StockDetail.RemoveAttachment();
    },
    ShareIdeaReset: function () {
        //CKEDITOR.instances.ckeditor.setData('');
        $("#ckEditor").val('');
        $('#editorImage').html('');
        //$("#chkEveryone").prop("checked", false);
        $("#chkFollower").prop("checked", false);
        //$("#chkEmailMe").prop("checked", false);
        $("#drpTrend button").removeClass('active');
        $('#txtUploadFile').replaceWith($("#txtUploadFile").clone(true));
    },
    HideBlogCommentError: function () {
        $('#commentsErrorSpan').text("");
        $('#commentsErrorDiv').hide();
    },
    ShowBlogCommentError: function (errorMessage) {
        $('#commentsErrorSpan').text(errorMessage);
        $('#commentsErrorDiv').show();
    },
    BtnBlogComment: function () {
        $("#blogComment").val('');
        StockDetail.HideBlogCommentError();
    },
    SendBlogComment: function () {
        StockDetail.HideBlogCommentError();
        var blogId = $("#blogId").val();
        var blogComment = $("#blogComment").val();
        if (blogComment == null || blogComment === '') {
            StockDetail.ShowBlogCommentError("Please Enter Comment.");
        }
        else {
            //Common.ShowSendingProgress();
            $.ajax({
                url: '/Home/BlogComment',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ comment: blogComment, blogId: blogId }),
                success: function (response) {
                    if (response.IsSuccess) {
                        //Common.ShowSuccess("Comment added successfully", true);
                        $('#CloseBlogCommentModal').trigger('click');
                        Common.StopProgress();
                        return;
                    } else {
                        //Common.StopProgress();
                        StockDetail.ShowBlogCommentError(response.html);

                    }
                }
            });
        }
    },
    GetTopIdeas: function (symbol) {
        Common.ShowSendingProgress();
        option = "Like";
        $.ajax({
            url: "/Home/GetTopIdeas",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ symbol: symbol, option: $("#streamPrefrence").val() }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#topIdea").addClass("active");
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetAllIdeas: function (symbol) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetAllIdeas",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ symbol: symbol }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").addClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").removeClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetChartIdeas: function (symbol) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetChartIdeas",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ symbol: symbol }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").addClass("active");
                    $("#linksIdea").removeClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetLinksIdeas: function (symbol) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetLinksIdeas",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ symbol: symbol }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").addClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },

    ConsensusRemoveContent: function (symbol) {
        $.ajax({
            url: '/Home/ConsensusChart',
            async: false,
            type: 'POST',
            contentType: 'application/json',
            data: "{'symbol':'" + symbol + "'}",
            dataType: 'json',//this is important
            success: function (data) {
                if (data.html.length > 0) {
                    StockDetail.ConsensusDrawVisualization(data.html);
                    return false;
                } else {
                    //alert("No data found.");
                }
            },
            error: function (e) {
                //alert(e.message);
                //called when there is an error
                //console.log(e.message);
            }
        });
    },
    ConsensusDrawVisualization: function (serverData) {
        var trendInsight = "";
        var percentage = "";
        var chardata = [];
        chardata.push(['TrendInSightName', 'TotalCnt']);
        for (var i = 0; i < serverData.length; i++) {

            chardata.push([serverData[i].TrendInSightName, serverData[i].TotalCnt]);
        }
        var data = google.visualization.arrayToDataTable(chardata
          //  [
          //['TrendInSightName', 'TotalCnt'],
          //[serverData[0].TrendInSightName, serverData[0].Percentage],
          //[serverData[1].TrendInSightName, serverData[1].Percentage],
          //[serverData[2].TrendInSightName, serverData[2].Percentage],
          //[serverData[3].TrendInSightName, serverData[3].Percentage],
          //[serverData[4].TrendInSightName, serverData[4].Percentage]
          //  ]
        );

        var options = {
            //title: 'Consensus',
            legend: 'none',
            slices: {
                0: { color: '#219A00' },
                1: { color: '#35EC00' },
                2: { color: '#F7E03E' },
                3: { color: '#FD9D15' },
                4: { color: '#B51300' }
            },
            pieSliceTextStyle: {
                color: 'black'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
        return false;
    },
    IsWatch: function (symbol, type, isUnwatch) {
        //Common.ShowSendingProgress();
        if (isUnwatch == '' || isUnwatch == null) {
            isUnwatch = false;
        }
        $.ajax({
            url: "/Home/UnWatch",
            type: "POST",
            dataType: "json",
            data: { symbol: symbol, type: type, isUnwatch: isUnwatch },
            success: function (response) {
                if (response.IsSuccess) {
                    if (isUnwatch) {
                        //Common.ShowSuccess(response.msg, true);
                    }
                    $("#watchListButton").html(response.html)
                    StockDetail.WatchListFilter(symbol, "null");
                } else {
                    //Common.StopProgress();
                    Common.ShowError(response.msg, true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                //Common.StopProgress();
            }
        });
    },
    IsWatchCheck: function (symbol, type) {
        Common.ShowSendingProgress();
        $.get('/Home/IsWatchCheck', { symbol: symbol, type: type },
        function (data) {
            $("#watchListButton").html(data);
        });
    },
    ShareIdeaCount: function (symbol, activeTab, lastDate) {
        $.get('/Home/FetchIdeaCount',
            { symbol: symbol, activeTab: activeTab, lastDate: lastDate },
                    function (data) {
                        $("#ideaCount").html(data);
                    });
    },
    FetchIdeas: function () {
        var selectedClass = $(".tab li button.active").attr('id');
        $("#newIdeaCount").text('');
        $("#newIdeaCount").text(0);
        $("#lastFetchIdeaDate").val($("#curDate").text());
        $(".tab li button.active").trigger("click");
    },
    ShareIdeaTextAreaAutoComplete: function () {
        var symbol = "";
        var companyName = "";
        var searchText = "";
        $('#ckEditor').textcomplete([{
            match: /\B@(\w{2,})$/,
            search: function (term, callback) {
                term = term.toLowerCase();
                $.getJSON("/Home/UserList", { searchText: term })
                  .done(function (resp) {
                      callback($.map(resp, function (word) {
                          return word.toLowerCase().indexOf(term) === 0 ? word : null;
                      }));
                  })
                  .fail(function () {
                      callback([]);
                  });
            },
            index: 1,
            replace: function (mention) {
                return '@' + mention + ' ';
            }
        }, {
            match: /\B#(\w{2,})$/, //    /^\B#(\w{2,})+[a-zA-Z0-9_ ]*$/
            search: function (term, callback) {
                term = term.toLowerCase();
                $.getJSON("/Home/SymbolsList", { searchText: term })
                  .done(function (resp) {
                      callback($.map(resp, function (word) {
                          symbol = word.Key;
                          companyName = word.Value;
                          searchText = symbol.toLowerCase().search(term) === 0 ? symbol + " " + companyName : companyName.toLowerCase().search(term) === 0 ? symbol + " " + companyName : null;
                          return searchText;
                      }));
                  })
                  .fail(function () {
                      callback([]);
                  });
            },
            index: 1,
            replace: function (resp) {
                return '#' + resp.split(' ')[0] + ' ';
            }
        }
        ], {
            appendTo: 'body'
        })
    },
    RefreshStockRatingChart: function (symbol) {  //, type
        $.get('/Home/RefershStockRatingChart', { symbol: symbol },  //, type: type 
        function (data) {
            $("#stockRatingChart").html('');
            $("#stockRatingChart").html(data);
        });
    },
    ViewNse_Bse: function (el, symbol, type) {
        Common.ShowSendingProgress();
        var $this = $(el);
        dataParam = { symbol: symbol, type: type };
        $.get('/Home/StockTicker', dataParam,
        function (data) {
            $("#stockTicker").html(data);
            var className = data.indexOf('lowprice') > 0 ? "borderLeftRed" : data.indexOf('upprice') > 0 ? "borderLeftGreen" : "";
            $("#stockTicker").parent().parent().removeClass().addClass('watchList' + ' ' + className)
            $("#hfExchange").val(type); //Used for graph so dont delete.

            //Graph Bind
            StockDetail.RemoveContent($("#hfPeriod").val(), $("#hfInterval").val(), symbol);
            Common.StopProgress();
        });
    }
};
$(document).ready(function () {
    setInterval(function () {
        if ($("#isStockMarketOpenClose").val() === "True") {
            if (pathname.indexOf('symbol') > 0) {
                if (!$(".inrbg").hasClass("modal-open")) {
                    StockDetail.StockTicker($("#hfViewSymbol").val(), $("#hfViewExchange").val());
                    //StockDetail.ShareIdeaCount($("#hfDefaultSymbol").val(), $(".tab li button.active").attr('id'), $("#lastFetchIdeaDate").val());
                }
            }
        }
        if (pathname.indexOf('symbol') > 0) {
            StockDetail.ShareIdeaCount($("#hfDefaultSymbol").val(), $(".tab li button.active").attr('id'), $("#lastFetchIdeaDate").val());
        }
    }, $("#stockUpdateRefresh").val());     //$("#stockUpdateRefresh").val()

    setTimeout(function () {
        $("#defalutGraph").trigger('click');
    }, 1);

    StockDetail.WatchListAutocomplete();

    if (pathname.indexOf('symbol') > 0 || pathname.indexOf('profile') > 0) {
        StockDetail.ShareIdeaTextAreaAutoComplete();
    }
    StockDetail.UploadAttachment();
});