﻿var timer;
//var stocktype = '';
var treandinsight = '';
var Dashboard = {
    BindBubbles: function () {
        treandinsight = $(".stockRightFilter").find('input[name=radio-group]:checked').val();
        //if (stocktype == "ALL") {
        //    stocktype = '';
        //}
        if (treandinsight == undefined || treandinsight == '') {
            treandinsight = '-1';
        }
        $.ajax({
            url: "/Home/DashboardBubbles",
            type: "POST",
            dataType: "json",
            data: { treandInSight: treandinsight},
            beforeSend: function () {
                //$('#bubbles').html('').addClass('loading');
                Common.ShowSendingProgress();
            },
            complete: function () {
                //$('#bubbles').removeClass('loading');
                Common.StopProgress();
            },
            success: function (response) {
                if (response.IsSuccess) {
                    $('#bubbles').html(response.html);
                    Dashboard.Generate_bubbles();
                } else {

                }
            }
        });

    },
    Generate_bubbles: function () {
        $('#bubbles').freetile({
            selector: '.bubble',
            animate: true,
            elementDelay: 30
        });
    },
    TrendingPopup: function (el, e) {
        var $this = $(el);
        clearTimeout(timer);
        timer = setTimeout(function () {
            var thisId = $this[0].id;
            var list = $this.attr('id').split(",")
            $this.qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: {
                    text: function (event, api) {
                        api.elements.content.html(' Loading, Please wait...');
                        return $.ajax({
                            url: "/Home/MenuHoverGet",
                            //type: "POST",
                            data: { symbol: list[0], type: list[1] }
                        })
                             .then(function (content) {
                                 api.elements.content.html(content.html);
                                 clearTimeout(timer);
                             }, function (xhr, status, error) {
                                 api.set('content.text', status + ':  ' + error);
                             });
                    },
                    //title: { text: '...' }
                },
                position: {
                    at: 'bottom left',
                    my: 'top left',
                    //viewport: $(window),
                    effect: true,
                    //adjust: {
                    //    resize: true
                    //},
                    target: $this
                },

                hide: {
                    delay: 500,
                    fixed: true
                },
                style: {
                    tip: {
                        corner: true
                    },
                    classes: 'qtip-light qtip-shadow',
                    //width: 600
                }
            }).qtip('show');
            $(".curve").remove();
        }, 1000);









    },
    TrendingPopupComplete: function () {
        clearTimeout(timer);
    }
};
$(document).ready(function () {
    stocktype = '';
    treandinsight = '-1';
    Dashboard.BindBubbles();
    $('.stocktype').on('click', function (event) {
        stocktype = $(this).text();
        treandinsight = $("#stockRightFilter").find('input[name=radio-group]:checked').val();
        $(this).parent().parent().find('a').removeClass('active')
        $(this).addClass('active');
        Dashboard.BindBubbles();
        return false;
    });
});