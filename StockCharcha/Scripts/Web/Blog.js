﻿var BlockNumber = 2;  //Infinate Scroll starts from second block
var newFileName;
var originalFileName;
var pathname = window.location.pathname;
var Blog = {
    CommentsReply: function () {
        $('#commentsBlock').fadeToggle("slow");
    },
    CommentsCancel: function () {
        $('#createBlock').fadeToggle("slow");
        Blog.ShareIdeaReset();
        Blog.RemoveAttachment();
    },
    CreateBlog: function () {
        Common.ShowSendingProgress();
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    Blog.ShareIdeaReset();
                    $("#ckEditor").val('@' + $("#hfUserTag").val() + ' ');
                    $('#createBlock').fadeToggle("slow");
                } else {
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }

        });
    },
    UserFollow: function (ele) {
        Common.ShowSendingProgress();
        var element = ele;
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    $.ajax({
                        url: "/Home/UserFollow",
                        type: "POST",
                        dataType: "json",
                        data: { followBy: $('.userFollow').attr('id') },
                        success: function (response) {
                            if (response.IsSuccess) {
                                $(element).html($(element).text().trim() == 'Follow' ? '<i class="fa fa-user-plus"></i>Following' : '<i class="fa fa-user-plus"></i>Follow');
                                if ($(element).text().trim() == 'Following') {
                                    $(element).parent().addClass("userfollowing ");
                                } else {
                                    $(element).parent().removeClass("userfollowing ");
                                }
                                //Common.ShowSuccess(response.html, true);
                            } else {
                                //Common.StopProgress();
                                Common.ShowError(response.html, true);
                                Common.ScrollToElement($(".notifications"));
                            }
                        },
                        complete: function () {
                            Common.StopProgress();
                        }
                    });
                } else {
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            }

        });
    },

    UserFollowList: function () {
        Common.ShowSendingProgress();
        $.ajax({
            type: "POST",
            url: '/Home/LoginCheck',
            success: function (result) {
                if (result.IsSuccess) {
                    $.ajax({
                        url: "/Home/UserFollow",
                        type: "POST",
                        dataType: "json",
                        data: { followBy: $('.userFollow').attr('id') },
                        success: function (response) {
                            if (response.IsSuccess) {
                                //Common.ShowSuccess(response.html, true);
                                Blog.UserFollowedListFilter();
                            } else {
                                //Common.StopProgress();
                                Common.ShowError(response.html, true);
                                Common.ScrollToElement($(".notifications"));
                            }
                        },
                        complete: function () {
                            Common.StopProgress();
                        }
                    });
                } else {
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            }

        });
    },

    UserFollowedListFilter: function () {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/UserFollowedFilter",
            type: "POST",
            dataType: "json",
            //data: {  },
            success: function (response) {
                if (response.IsSuccess) {
                    //Common.ShowSuccess(response.msg, true);
                    $("#userFollowedFilter").html(response.html)
                } else {
                    //Common.StopProgress();
                    Common.ShowError(response.msg, true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    ProfileWatchListFilter: function (el) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/ProfileWatchListFilter",
            type: "POST",
            dataType: "json",
            data: { symbol: $(el).attr('id') },
            success: function (response) {
                if (response.IsSuccess) {
                    //Common.ShowSuccess(response.msg, true);
                    $("#profileWatchFilter").html(response.html)
                } else {
                    //Common.StopProgress();
                    Common.ShowError(response.msg, true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    UploadAttachment: function () {
        $('#txtUploadFile').on('change', function (e) {
            var files = e.target.files;
            if (files.length > 0) {
                if (window.FormData !== undefined) {
                    var data = new FormData();
                    for (var x = 0; x < files.length; x++) {
                        data.append("file" + x, files[x]);
                    }

                    $.ajax({
                        type: "POST",
                        url: '/Home/UploadAttachedFile',
                        contentType: false,
                        processData: false,
                        data: data,
                        success: function (result) {
                            if (result.IsSuccess) {
                                newFileName = result.html.NewFileName;
                                originalFileName = result.html.OriginalFilePath;
                                $('#editorImage').html(result.html.EditorImage)
                            } else {
                                $('#txtUploadFile').replaceWith($("#txtUploadFile").clone(true));
                                Common.ShowError("Please login first.", true);
                                Common.ScrollToElement($(".notifications"));
                            }
                        },
                        error: function (xhr, status, p3, p4) {
                            var err = "Error " + " " + status + " " + p3 + " " + p4;
                            if (xhr.responseText && xhr.responseText[0] == "{")
                                err = JSON.parse(xhr.responseText).Message;
                            console.log(err);
                        }
                    });
                } else {
                    alert("This browser doesn't support HTML5 file uploads!");
                }
            }
        });
    },
    RemoveAttachment: function () {
        //Common.ShowSendingProgress();
        $('#txtUploadFile').replaceWith($("#txtUploadFile").clone(true));
        $('#editorImage').html('');
        newFileName = "";
        originalFileName = "";
    },
    //Find URL from entered text.
    Strip: function (html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return tmp.innerText.replace(urlRegex, function (url) {
            return '\n' + url;
        })
    },
    RenderHTML: function (text) {
        var rawText = Blog.Strip(text);
        var urlRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/ig
        //  /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

        return rawText.replace(urlRegex, function (url) {
            if ((url.indexOf(".jpg") > 0) || (url.indexOf(".png") > 0) || (url.indexOf(".gif") > 0)) {
                return '<img src="' + url + '">' + '<br/>';
            } else {
                return '<a href="' + url + '" target="_blank">' + url + '</a>';//+ '<br/>'
            }
        })
    },
    //

    ShareIdea: function () {
        Common.ShowSendingProgress();
        var description = Blog.RenderHTML($("#ckEditor").val());
        var model = new Object();
        model.Symbol = "";
        model.Description = description//CKEDITOR.instances.ckeditor.getData();
        model.Everyone = $('#chkEveryone').is(":checked");
        model.OnlyFollowers = $('#chkFollower').is(":checked");
        model.TrendInSights = $("#drpTrend").children().closest(".active").val();
        model.EmailOnReply = $('#chkEmailMe').is(":checked");
        model.OriginalFileName = originalFileName;
        model.NewFileName = newFileName;
        if (description === "") {
            Common.ShowError("Share idea first.", true);
            Common.StopProgress();
            return false;
        }
        $.ajax({
            url: "/Home/ShareIdeaProfile",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify(model),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    Blog.ShareIdeaReset();
                    newFileName = "";
                    originalFileName = "";
                    //Common.ShowSuccess("Posted successfully.", true);
                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                    $('#createBlock').fadeToggle("slow");
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    ShareIdeaReset: function () {
        $("#ckEditor").val('');
        $('#editorImage').html('');
        //$("#chkEveryone").prop("checked", false);
        $("#chkFollower").prop("checked", false);
        //$("#chkEmailMe").prop("checked", false);
        $("#drpTrend button").removeClass('active');
        $('#txtUploadFile').replaceWith($("#txtUploadFile").clone(true));
    },
    GetTopIdeas: function (username) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetTopIdeasProfile",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ username: username, option: $("#streamPrefrenceProfile").val() }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#topIdea").addClass("active");
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetAllIdeas: function (username) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetAllIdeasProfile",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ username: username }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").addClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").removeClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetChartIdeas: function (username) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetChartIdeasProfile",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ username: username }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").addClass("active");
                    $("#linksIdea").removeClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    GetLinksIdeas: function (username) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/GetLinksIdeasProfile",
            type: "POST",
            //dataType: "json",
            data: JSON.stringify({ username: username }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsSuccess) {
                    BlockNumber = 2;
                    $("#allIdea").removeClass("active");
                    $("#chartIdea").removeClass("active");
                    $("#linksIdea").addClass("active");
                    $("#topIdea").removeClass("active");

                    $("#shareIdeaList").html('');
                    $("#shareIdeaList").html(response.html);
                } else {
                    //Common.StopProgress();
                    Common.ShowError("Please login first.", true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },
    ShareIdeaCount: function (username, activeTab, lastDate) {
        var dataParam = { username: username, activeTab: activeTab, lastDate: lastDate };
        if (activeTab == "topIdea") {
            dataParam = { username: username, activeTab: activeTab, lastDate: lastDate, option: $("#streamPrefrenceProfile").val() };
        }
        $.get('/Home/FetchIdeaProfileCount', dataParam,
                    function (data) {
                        $("#ideaCount").html(data);
                    });
    },
    FetchIdeas: function () {
        var selectedClass = $(".tab li button.active").attr('id');
        $("#newIdeaCount").text('');
        $("#newIdeaCount").text(0);
        $("#lastFetchIdeaDate").val($("#curDate").text());
        $(".tab li button.active").trigger("click");
    }
};
$(document).ready(function () {
    Blog.UploadAttachment();
    setInterval(function () {
        if (pathname.indexOf('profile') > 0) {
            Blog.ShareIdeaCount($("#hfUsername").val(), $(".tab li button.active").attr('id'), $("#lastFetchIdeaDate").val());
        }
    }, $("#stockUpdateRefresh").val());
});