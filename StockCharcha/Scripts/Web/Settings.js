﻿var Settings = {
    UpdateUserProfile: function (el) {
        Common.ShowSendingProgress();
        var $this = $(el.parentNode);
        $.get('/Home/UpdateUserProfileGet',
        function (data) {
            $("#settingRightBody").html('');
            $("#settingRightBody").html(data);
            $this.addClass('active');
            $(".setupList li").not($this).removeClass('active');
            Common.StopProgress();
        });
    },
    ChangePassword: function (el) {
        Common.ShowSendingProgress();
        var $this = $(el.parentNode);
        $.get('/Home/ChangePasswordGet',
        function (data) {
            $("#settingRightBody").html('');
            $("#settingRightBody").html(data);
            $this.addClass('active');
            $(".setupList li").not($this).removeClass('active');
            Common.StopProgress();
        });
    },
    UserMessages: function (el) {
        Common.ShowSendingProgress();
        var $this = $(el.parentNode);
        $.get('/Home/UserListGet',
        function (data) {
            $("#settingRightBody").html('');
            $("#settingRightBody").html(data);
            $this.addClass('active');
            $(".setupList li").not($this).removeClass('active');
            Common.StopProgress();
        });
    },
    MessagesListByUser: function (el, userId) {
        Common.ShowSendingProgress();
        var $this = $(el.parentNode);
        $.get('/Home/MessageListByUser', { selecteduserId: userId },
        function (data) {
            $("#userByMessageList").html('');
            $("#userByMessageList").html(data);
            $this.parent().addClass('active');
            $("#userListing li").not($this.parent()).removeClass('active');
            Common.ScrollToElement($("#messageReply"));
            Common.StopProgress();
        });
    },
    HideMessageError: function () {
        $('#messageErrorSpan').text("");
        $('#messageErrorDiv').hide();
    },
    ShowMessageError: function (errorMessage) {
        $('#messageErrorSpan').text(errorMessage);
        $('#messageErrorDiv').show();
    },
    ReplyMessage: function (el, TouserId) {
        Common.ShowSendingProgress();
        Settings.HideMessageError();
        var messageReply = $("#messageReply").val();
        if (messageReply == null || messageReply === '') {
            Settings.ShowMessageError("Please Enter Reply.");
            Common.StopProgress();
        }
        else {
            $.get('/Home/ReplyMessage', { toUserId: TouserId, message: messageReply },
                    function (data) {
                        $("#userByMessageList").html('');
                        $("#userByMessageList").html(data);
                        Common.StopProgress();
                    });
        }

    },
    WatchList: function (el) {
        Common.ShowSendingProgress();
        var $this = $(el.parentNode);
        $.get('/Home/WatchListGet',
        function (data) {
            $("#settingRightBody").html('');
            $("#settingRightBody").html(data);
            $this.addClass('active');
            $(".setupList li").not($this).removeClass('active');
            Common.StopProgress();
        });
    },
    ProfileWatchListFilter: function (el) {
        Common.ShowSendingProgress();
        $.ajax({
            url: "/Home/ProfileWatchListFilter",
            type: "POST",
            dataType: "json",
            data: { symbol: $(el).attr('id') },
            success: function (response) {
                if (response.IsSuccess) {
                    //Common.ShowSuccess(response.msg, true);
                    $("#settingRightBody").html(response.html)
                } else {
                    //Common.StopProgress();
                    Common.ShowError(response.msg, true);
                    Common.ScrollToElement($(".notifications"));
                }
            },
            complete: function () {
                Common.StopProgress();
            }
        });
    },

    AddQuestion: function (el) {
        $.get('/Home/AddQuestion',
                    function (data) {
                        $("#answerListByQuestion").html('');
                        $("#answerListByQuestion").html(data);
                        $("#questionListing li").removeClass('active');
                        $(el).parent().addClass('active');
                        Common.StopProgress();
                    });
    },
    SaveAddQuestion: function (el) {
        Common.ShowSendingProgress();
        Settings.HideMessageError();
        var question = $("#question").val();
        if (question == null || question === '') {
            Settings.ShowMessageError("Please Enter Question.");
            Common.StopProgress();
        }
        else {
            $.get('/Home/SaveQuestion', { question: question },
                    function (data) {
                        $("#questionList").html('');
                        $("#questionList").html(data);
                        Common.StopProgress();
                    });
        }

    },
    AnswerListByQuestion: function (el, questionId) {
        Common.ShowSendingProgress();

        $.get('/Home/AnswerListByQuestion', { questionId: questionId },
                function (data) {
                    $("#answerListByQuestion").html('');
                    $("#answerListByQuestion").html(data);
                    $("#questionListing li").removeClass('active');
                    $(el).parent().addClass('active');
                    Common.StopProgress();
                });
    },
    SaveAnswer: function (el, questionId) {
        Common.ShowSendingProgress();
        Settings.HideMessageError();
        var answer = $("#answer").val();
        if (answer == null || answer === '') {
            Settings.ShowMessageError("Please Enter Reply.");
            Common.StopProgress();
        }
        else {
            $(el).parent().addClass('active');
            $.get('/Home/SaveAnswer', { questionId: questionId, answer: $("#answer").val() },
                    function (data) {
                        $("#answerListByQuestion").html('');
                        $("#answerListByQuestion").html(data);

                        Common.StopProgress();
                    });
        }
    }
};
$(document).ready(function () {

});