//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StockCharcha.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class SiteError
    {
        public long ErrorID { get; set; }
        public string ErrorCode { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string Source { get; set; }
        public string IPAddress { get; set; }
        public string Browser { get; set; }
        public string Description { get; set; }
        public string WebURL { get; set; }
        public Nullable<System.Guid> ModifiedBY { get; set; }
        public Nullable<System.DateTime> ModifiedON { get; set; }
    }
}
