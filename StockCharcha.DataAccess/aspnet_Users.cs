//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StockCharcha.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class aspnet_Users
    {
        public aspnet_Users()
        {
            this.aspnet_PersonalizationPerUser = new HashSet<aspnet_PersonalizationPerUser>();
            this.Blogs = new HashSet<Blog>();
            this.Blogs1 = new HashSet<Blog>();
            this.BlogComments = new HashSet<BlogComment>();
            this.BlogComments1 = new HashSet<BlogComment>();
            this.BlogCommentsLikes = new HashSet<BlogCommentsLike>();
            this.BlogCommentsLikes1 = new HashSet<BlogCommentsLike>();
            this.RecentlyVieweds = new HashSet<RecentlyViewed>();
            this.RegistrationMasters = new HashSet<RegistrationMaster>();
            this.UserRatings = new HashSet<UserRating>();
            this.UserWatchLists = new HashSet<UserWatchList>();
            this.aspnet_Roles = new HashSet<aspnet_Roles>();
        }
    
        public System.Guid ApplicationId { get; set; }
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public string LoweredUserName { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public System.DateTime LastActivityDate { get; set; }
    
        public virtual aspnet_Applications aspnet_Applications { get; set; }
        public virtual aspnet_Membership aspnet_Membership { get; set; }
        public virtual ICollection<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }
        public virtual aspnet_Profile aspnet_Profile { get; set; }
        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Blog> Blogs1 { get; set; }
        public virtual ICollection<BlogComment> BlogComments { get; set; }
        public virtual ICollection<BlogComment> BlogComments1 { get; set; }
        public virtual ICollection<BlogCommentsLike> BlogCommentsLikes { get; set; }
        public virtual ICollection<BlogCommentsLike> BlogCommentsLikes1 { get; set; }
        public virtual ICollection<RecentlyViewed> RecentlyVieweds { get; set; }
        public virtual ICollection<RegistrationMaster> RegistrationMasters { get; set; }
        public virtual ICollection<UserRating> UserRatings { get; set; }
        public virtual ICollection<UserWatchList> UserWatchLists { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles { get; set; }
    }
}
