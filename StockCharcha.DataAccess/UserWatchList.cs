//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StockCharcha.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserWatchList
    {
        public long Id { get; set; }
        public Nullable<System.Guid> UserId { get; set; }
        public string Symbol { get; set; }
        public string Exchange { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
    }
}
