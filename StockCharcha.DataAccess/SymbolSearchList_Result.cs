//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StockCharcha.DataAccess
{
    using System;
    
    public partial class SymbolSearchList_Result
    {
        public long Id { get; set; }
        public string NSESymbol { get; set; }
        public string BSESymbol { get; set; }
        public string SecurityId { get; set; }
        public string ISINNumber { get; set; }
        public string CompanyName { get; set; }
    }
}
